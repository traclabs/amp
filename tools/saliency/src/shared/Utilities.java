////////////////////////////////////////////////////////////////////////////////
// Copyright (c)  2015 General Dynamics Inc.
////////////////////////////////////////////////////////////////////////////////

package shared;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.codec.binary.Base64;

/**
 * Contains utility methods used by Client and Server.
 */
public final class Utilities
{
  /**
   * Never instantiate this utility class.
   */
  private Utilities(){}
  
  
  /**
   * Reads a File into memory and encodes the byte data in Base64
   * @param filepath String path to the file.
   * @return a String containing the Base64 data or null if the file could not
   *  be read.
   */
  public static String encodeImageFileToBase64(final String filepath) 
  {
    return encodeImageFileToBase64(new File(filepath));
  }
  
  
  /**
   * Reads a File into memory and encodes the byte data in Base64
   * @param file the File to read
   * @return a String containing the Base64 data or null if the file could not
   *  be read.
   */
  public static String encodeImageFileToBase64(final File file)
  {
    try
    {
      final FileInputStream imageInFile = new FileInputStream(file);
      byte imageData[] = new byte[(int) file.length()];
      imageInFile.read(imageData);
      imageInFile.close();
    
      // Converting Image byte array into Base64 String
      final String imageDataString = 
        Base64.encodeBase64URLSafeString(imageData);
      
      return imageDataString;
    }
    catch (final IOException ioe)
    {
      System.err.println(ioe);
      return null;
    }
    
  }
  
  
  /**
   * Decodes a base64 String back to its byte format, then stores the result
   * in a File.
   * @param base64 the base64 data (as a String).
   * @param destinationPath the full path of the file to create.
   * @return true if the file was written without any errors occuring.
   */
  public static boolean decodeBase64ToImageFile(
      final String base64,
      final String destinationPath)
  {
    final byte[] decodedData = Base64.decodeBase64(base64);
    
    try
    {
      final FileOutputStream imageOutFile = new FileOutputStream(
          destinationPath);
      imageOutFile.write(decodedData);
      imageOutFile.close();
      return true;
    }
    catch (IOException e)
    {        
      e.printStackTrace();
      return false;
    }
  }
  
}
