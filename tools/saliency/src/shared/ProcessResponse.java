////////////////////////////////////////////////////////////////////////////////
// Copyright (c)  2015 General Dynamics Inc.
////////////////////////////////////////////////////////////////////////////////


package shared;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class that describes the data returned from a requestProcess POST.
 */
@XmlRootElement
public class ProcessResponse
{
  /*
   * Required default constructor for Moxy.
   */
  public ProcessResponse() {}
  
  /**
   * @param id A String identifier (should match ProcessRequest.requestId)
   * @param resultFlag Whether the operation was successful or not.
   * @param results An array of Strings containing the resulting images 
   *  encoded in base64.
   */
  public ProcessResponse(
      final String id, 
      final Boolean resultFlag,
      final String[] results)
  {
    requestId = id;
    success = resultFlag;
    resultImages = results;
  }
  
  
  public String requestId;
  
  public Boolean success;
  
  public String[] resultImages;
}
