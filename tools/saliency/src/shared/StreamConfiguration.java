////////////////////////////////////////////////////////////////////////////////
// Copyright (c)  2015 General Dynamics Inc.
////////////////////////////////////////////////////////////////////////////////


package shared;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class StreamConfiguration
{
  public StreamConfiguration() 
  {
    channels = new ArrayList<Channel>(0);
    levels = 3;
  }
  
  
  public List<Channel> channels;
  public int levels;
}
