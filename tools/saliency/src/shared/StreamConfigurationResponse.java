////////////////////////////////////////////////////////////////////////////////
// Copyright (c)  2015 General Dynamics Inc.
////////////////////////////////////////////////////////////////////////////////


package shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class StreamConfigurationResponse
{
  public StreamConfigurationResponse() {}
  
  public StreamConfigurationResponse(final boolean existsInput, 
                                     final boolean successInput)
  {
    existingStream = existsInput;
    success = successInput;
  }
  
  public boolean existingStream;
  public boolean success;
}
