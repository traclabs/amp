////////////////////////////////////////////////////////////////////////////////
// Copyright (c)  2015 General Dynamics Inc.
////////////////////////////////////////////////////////////////////////////////

package saliency;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import matlabcontrol.MatlabConnectionException;
import matlabcontrol.MatlabInvocationException;
import matlabcontrol.MatlabProxy;
import matlabcontrol.MatlabProxyFactory;
import matlabcontrol.MatlabProxyFactoryOptions;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.lang3.tuple.Pair;

import shared.Channel;
import shared.ProcessRequest;
import shared.ProcessResponse;
import shared.StreamConfiguration;
import shared.StreamConfigurationResponse;
import shared.Utilities;

/**
 * This class "controls" the connection to a running Matlab process.
 * While technically instantiatable (via new), 
 * this class should be treated as a Singleton as only a single instance will
 * exist within the Servlet container.  (This class is configured as a 
 * ServletContext Listener in web.xml).
 * 
 */
public class MatlabController implements ServletContextListener, Runnable
{
  /**
   * @return the single instance of MatlabController
   */
  public static MatlabController getSingleton()
  {
    return instance_;
  }
  
  /**
   * The maximum number of input images supported.
   */
  public static final int MAX_INPUTS = 32;
  
  /**
   * The directory that the input data files should be written to. 
   */
  static String INPUT_FILE_DIR;
    
  
  /**
   * The directory where the results of the Saliency algorithm are stored.
   */
  static String RESULT_DIR;
      
  
  /**
   * A meaningless number that happens to be the "starting number" of the 
   * Matlab Saliency program.
   */
  public static final int MEANINGLESS_STARTING_NUMBER = 1;

  
  /**
   * Given a 0-based number ("0" being the "first" input) returns the full 
   * path that this input should be written to.
   * 
   * @param inputNumber A 0-based number 
   * @return String containing the filename this input should be written to.
   */
  public static String mapInputNumberToFilename(final int inputNumber)
  {
    final String imageName = String.format("%03d.jpg", 
        MEANINGLESS_STARTING_NUMBER + inputNumber);
    final String imagePath = INPUT_FILE_DIR + imageName;
    
    return imagePath;
  }
  
  
  /**
   * Given a 0-based number ("0" being the "first" result) returns the full
   * path to the results file.
   * @param resultNumber A 0-based number.
   * @return String containing the filename of the results file.
   */
  public static String mapResultNumberToFilename(final int resultNumber)
  {
    final String imageName = String.format("%03d.jpg", 1 + resultNumber);
    final String imagePath = RESULT_DIR + imageName;
    
    return imagePath;
  }
  
  
  /**
   * Add a request
   * @param request
   * @param response
   * @return (boolean) true if the request was added, false if the processor
   *         cannot accept the request
   */
  public boolean addRequest(final ProcessRequest request,
                            final AsyncResponse response)
  {
    if (proxy_ != null)
    {     
      synchronized (requests_)
      {
        requests_.add(Pair.of(request, response));
      }
      
      synchronized (requestGate_)
      {
        requestGate_.notify();
      }
      
      return true;
    }
    else 
    {
      System.err.println("Proxy is not available");
      return false;
    }
  }
  

  @Override
  public void contextInitialized(ServletContextEvent contextEvent)
  {
    System.out.println("MatlabController: Context Initialized");
    instance_ = this;
    
    // Use the web.xml's GBVS_DIR context-parameter to initialize 
    // INPUT_FILE_DIR and RESULTS_FILE_DIR
    String gbvsDir = 
      contextEvent.getServletContext().getInitParameter("GBVS_DIR");
    if (gbvsDir == null)
    {
      System.err.println("web.xml is missing <context-param> for GBVS_DIR, "
        + "assuming a default location of C:\\SBIR\\GBVS\\gbvs");
      
      // default C:\SBIR\GBVS\gbvs\
      gbvsDir = "C:" + File.separator + "SBIR" + File.separator + "GBVS" 
        + File.separator + "gbvs" + File.separator;
    }
    
    INPUT_FILE_DIR = gbvsDir + 
      (gbvsDir.endsWith(File.separator) ? "" : File.separator) +
      "samplepics" + File.separator + "seq" + File.separator;
            
    RESULT_DIR = gbvsDir + 
      (gbvsDir.endsWith(File.separator) ? "" : File.separator) +
      "results" + File.separator;                
    
    System.out.format("Input data folder: %s\n", INPUT_FILE_DIR);
    System.out.format("Results folder: %s\n", RESULT_DIR);
    
    // Create a proxy, which we will use to control MATLAB
    try 
    {
      final MatlabProxyFactoryOptions options = 
        new MatlabProxyFactoryOptions.Builder().setUsePreviouslyControlledSession(true).build();
      final MatlabProxyFactory factory = new MatlabProxyFactory(options);
      MatlabProxy proxy = factory.getProxy();
      proxy.disconnect();
      proxy = factory.getProxy();
      
      proxy_ = proxy;
      proxy_.eval("disp('Connection Established')");
      proxy_.eval("cd " + gbvsDir);
      
      // proxy_.eval("streamMap = containers.Map");
      
      processingThread_ = new Thread(this, "Matlab Processing Thread");
      processingThread_.start();
      System.out.println("Proxy connection to Matlab established !");
    }
    catch (final MatlabConnectionException | MatlabInvocationException e)
    {
      System.err.println(e);
      proxy_ = null;
      processingThread_ = null;
    } 
      
  }
  
  
  @Override
  public void contextDestroyed(final ServletContextEvent contextEvent)
  {
    System.out.println("MatlabController: Context Destroyed");

    // TODO: Move this if block into run()
    // Theoretically this isn't terribly safe because the other thread could be
    // using the Proxy "right now".  All use of the proxy object should be 
    // thread confined.
    if (proxy_ != null)
    {
      try
      {
        proxy_.eval("disp('Shutting down connection')");
      }
      catch (final MatlabInvocationException e)
      {
        // do nothing, we're shutting down here anyway
      }
      proxy_.disconnect();
 
      proxy_ = null;
    }
    // --- END TODO ---
    
    if (processingThread_ != null)
    {
      // Signal the thread to stop
      stopThread_ = true;
      synchronized (requestGate_)
      {
        requestGate_.notify();
      }
      
      processingThread_ = null;
    }
  }


  
  @Override
  public void run()
  {
    while (true)
    {      
      try
      {
        ProcessRequest request = null;
        AsyncResponse response = null;
        
        synchronized (requests_)
        {
          if (!requests_.isEmpty())
          {
            final Pair<ProcessRequest, AsyncResponse> pair = requests_.poll();
            request = pair.getLeft();
            response = pair.getRight();
          }
        }
        
        if (request != null)
        {
          final long startTime = System.currentTimeMillis();
                    
          processRequest(request, response);
          
          final long endTime = System.currentTimeMillis();
          final long delta = endTime - startTime;
          System.out.format("Processing completed in: %d MS\n" , delta);
        }
        
        // This will wait until requestGate_.notify() is called        
        synchronized (requestGate_)
        {
          while (requests_.isEmpty())
          {
            if (stopThread_)
            {
              return;
            }
            try
            {
              requestGate_.wait();
            }
            catch (final InterruptedException e)
            {
              // ignore interrupts.
            }
          }
        }
      }
      catch (final Throwable t)
      {
        System.err.println(t);
      }      
    }
    
  }
  
  
  /**
   * Stores the images contained in the request to files in the input data
   * directory.
   * @param request ProcessRequest that contains the images that will be stored.
   * @throws ClientErrorException Throws in an error occurs.
   */
  private static void storeImagesFromRequest(final ProcessRequest request)
    throws ClientErrorException
  {
    // Get images from request
    for (int i=0; i < request.base64Images.length;i++)
    {
      final String base64String = request.base64Images[i];
      if (base64String == null)
      {
        final String errorMessage = String.format(
          "Missing or invalid data in image %d", i);
        System.err.println(errorMessage);
   
        final ResponseBuilder response = 
          Response.status(Response.Status.BAD_REQUEST).entity(errorMessage);
        throw new ClientErrorException(response.build());   
      }
      
      if (!Utilities.decodeBase64ToImageFile(
            base64String, 
            MatlabController.mapInputNumberToFilename(i)))
      {
        final String errorMessage = String.format(
            "Could not write to file %s", 
            MatlabController.mapInputNumberToFilename(i));
        System.err.println(errorMessage);
        
        final ResponseBuilder response = 
          Response.status(Response.Status.BAD_REQUEST).entity(errorMessage);
        throw new ClientErrorException(response.build());
      }      
    }
      
    System.out.println("All images received successfully"); 
  }
  
  
  /**
   * Determines the number of input images we have. 
   * @param request the current request being processed
   * @return (int) the number of images.
   */
  private static int getInputImagesCount(final ProcessRequest request)
  {
    final int count;
    if (request.getRequestContainsImages())
    {
      count = request.base64Images.length;
    }
    else
    {
      // Look in the input folder to see how many images we have
      final File directory = new File(INPUT_FILE_DIR);
      count = directory.list().length;
      // TODO: Change this code to inspect the filenames, files should
      // be numerically in order (no missing files).  Otherwise the
      // matlab scripts will break.
    }
    
    return count;
  }
  
  
  /**
   * Processes a request.
   * @param request the ProcessRequest to handle.
   * @param response the AsyncResponse object to pass results to.
   */
  private void processRequest(final ProcessRequest request, 
                              final AsyncResponse response)
  {
    
    // If the requests comes with images, store those now
    if (request.getRequestContainsImages())
    {
      try
      {
        storeImagesFromRequest(request);
      }
      catch (final ClientErrorException e)
      {
        response.resume(e);
        return;
      }
    }
    
    try
    {
      
      final int inputImagesCount = getInputImagesCount(request);
      final int startIndex = MEANINGLESS_STARTING_NUMBER;
      final int endIndex = MEANINGLESS_STARTING_NUMBER + 
                           inputImagesCount - 1; 
      
      // Run the demo with default parameters.
      final String streamId = request.getStreamId();
      
      if (streamId == null)
      {
        // Assign startIndex and endIndex variables within Matlab
        proxy_.eval("startIndex = " + Integer.toString(startIndex));
        proxy_.eval("endIndex = " + Integer.toString(endIndex));
        
        proxy_.eval("DKMcolor_motion_orientation_demo");
      
        // Close out all Matlab Figures.  This prevents us from building up a
        // large number of Matlab Figures on the screen.
        proxy_.eval("close all");
      }
      else
      {
        final String streamConfig = "stream_" + streamId + "_config";
        final String streamMotInfo = "stream_" + streamId + "_motinfo";
        
        // Execute saliency with this stream's configuration and 
        // previous motinfo.  Store the result back into streamMotInfo so
        // we can use it next time (Matlab seems to pass by value)
        proxy_.eval(String.format(
          "%s = execute_saliency(%s, %s, %d, %d)", 
          streamMotInfo, streamConfig, streamMotInfo, startIndex, endIndex));
      }
      
      if (request.getRespondWithJson())
      {
        final List<String> base64Images = 
          new ArrayList<String>(inputImagesCount);
        // Read result files
        // We are assuming that the number of outputs == number of input images
        for (int i=0; i<inputImagesCount; i+=1)
        {
          final String filename = mapResultNumberToFilename(i);
          final String base64 = Utilities.encodeImageFileToBase64(filename);
          if (base64 != null)
          {
            base64Images.add(base64);
          }
          else
          {
            System.err.format(
              "Missing result image %s, "
              + "results are incomplete\n", filename);
          }
          // TODO: error handle base64 == null
        }
        
        final ProcessResponse processResponse = 
          new ProcessResponse(
            request.requestId, 
            true, 
            base64Images.toArray(new String[base64Images.size()]));

        response.resume(processResponse);
      }
      else 
      {
        final File resultsFile = new File(RESULT_DIR + "result.jpg");
        if (resultsFile.exists())
        {
          // send the file directly back
          final String mt = new MimetypesFileTypeMap().getContentType(resultsFile);   
          final ResponseBuilder r = Response.ok(resultsFile, mt);
          r.header("Content-Disposition", "attachment; filename=\"result.jpg\"");      
          response.resume(r.build());                   
        }
        else 
        {
          final ResponseBuilder r = 
            Response.status(500).entity("result.jpg does not exist");
          response.resume(r.build());
        } // _end_ if (resultsFile.exists())
               
      } // _end_ else
      
    }// _end_ try
    catch (final MatlabInvocationException e)
    {
      // Some problem occurred in Matlab.
      e.printStackTrace();
      
      final ResponseBuilder r =
        Response.status(500).entity(e);
      response.resume(r.build());
    }
    
  }

  /**
   * Private boolean flag (must be volatile to work across threads), used to
   * notify the MatlabController Thread that it should shutdown.
   */
  private volatile boolean stopThread_ = false;
  
  /**
   * An Object used to notify (wake up) the MatlabController Thread.
   */
  private final Object requestGate_ = new Object();
  
  /**
   * A Queue that tracks the ProcessRequest and the corresponding
   * AsyncResponse.
   */
  private final Queue<Pair<ProcessRequest, AsyncResponse>> requests_ =
    new LinkedList<Pair<ProcessRequest, AsyncResponse>>();
  
 
  /**
   * The connection to Matlab.
   */
  private MatlabProxy proxy_;
 
  /**
   * The Thread used to execute Process Requests.
   */
  private Thread processingThread_;
  
  /**
   * The singleton-like instance of the MatlabController.
   */
  private static MatlabController instance_;


  //TODO: This is not thread safe !
  public Response configureStream(final String streamId, 
                                  final StreamConfiguration config)  
  {
    try
    {      
      final String streamConfigObjectName = "stream_" + streamId + "_config";
      final String streamMotInfo = "stream_" + streamId + "_motinfo";
      
      final boolean existingStream = 
        streamConfigurations_.containsKey(streamId); 
      if (!existingStream)
      {
        // Create a new parameters object, initialized with makeGBVSParams
        proxy_.eval(streamConfigObjectName + " = makeGBVSParams");
        proxy_.eval(streamMotInfo + " = []");
      }
      else
      {
        // TODO: Clean up the existing stream
        proxy_.eval(streamMotInfo + " = []");
      }
        
      streamConfigurations_.put(streamId, config);
      
      // Read off attributes from StreamConfiguration, execute Matlab commands
      // to apply the settings to the Matlab streamConfigObjectName object
      proxy_.eval(streamConfigObjectName + ".levels = " + config.levels);
      
      // Build up a String containing the abbreviation letter for each channel
      // we want to include.
      final StringBuilder abreviatedChannels = new StringBuilder();      
      for (final Channel c : config.channels)
      {
        abreviatedChannels.append(c.type.getLetter());
        
        // Set the weight as well.
        // The weight string looks like "colorWeight", "intensityWeight", etc 
        proxy_.eval(streamConfigObjectName + "."
          + c.type.toString().toLowerCase() + "Weight = " + c.weight);       
      }
      
      proxy_.eval(streamConfigObjectName + ".channels = '" + abreviatedChannels + "'");
      
      final StreamConfigurationResponse configResponse = 
          new StreamConfigurationResponse(existingStream, true);
      
      final ResponseBuilder responseBuilder = 
          Response.status(Response.Status.OK).entity(configResponse);
      return responseBuilder.build();
      
      // Async version
      // response.resume(configResponse);
    }
    catch (MatlabInvocationException e)
    {
      e.printStackTrace();
      
      final ResponseBuilder responseBuilder = 
        Response.status(Response.Status.BAD_REQUEST).entity(e);
      return responseBuilder.build();
    }
    
    
    
    
  }
  
  
  private Map<String, StreamConfiguration> streamConfigurations_ = 
      new HashMap<String, StreamConfiguration>();
  
}
