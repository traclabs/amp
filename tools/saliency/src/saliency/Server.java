////////////////////////////////////////////////////////////////////////////////
// Copyright (c)  2015 General Dynamics Inc.
////////////////////////////////////////////////////////////////////////////////


package saliency;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.tomcat.util.http.fileupload.FileUtils;

import shared.ProcessRequest;

@Path("/matlab")
public class Server
{
	
  @DELETE
  @Path("/result")
  public Response deleteResult() 
  {
    final File inputDirectory = new File(MatlabController.RESULT_DIR);
    try
    {
      FileUtils.cleanDirectory(inputDirectory);
      return Response.ok("Result files deleted").build();
    }
    catch (final IOException e)
    {
      e.printStackTrace();
      return Response.status(500).entity(e).build();
    }  
  }
  
  
  @GET
  @Path("/result")
  @Produces("image/*")
  public void asyncGetResult( @Suspended final AsyncResponse asyncResponse )
  {
    final ProcessRequest request = new ProcessRequest("0");
    request.setRespondWithJson(false);
    request.setRequestContainsImages(false);
    
    if (!MatlabController.getSingleton().addRequest(request, asyncResponse))
    {
      asyncResponse.resume(
          Response.status(Response.Status.SERVICE_UNAVAILABLE)
          .entity("Request was rejected, Matlab processing unavailable.")
          .build());
    }
    
	}
  
  
  /*
  @GET
  @Path("/resultImage")
  @Produces("image/*")
  public Response getResultImage( ) // @PathParam("image") String image) 
  {    
    File f = new File("C:\\SBIR\\GBVS\\gbvs\\results\\result.jpg");
    if (!f.exists()) 
    {
      throw new WebApplicationException(404);
    }
    
    final String mt = new MimetypesFileTypeMap().getContentType(f);   
    final ResponseBuilder response = Response.ok(f, mt);
    
    response.header("Content-Disposition", "attachment; filename=\"result.jpg\"");
    return response.build();
  }
  */
  
  
  @DELETE
  @Path("/input")
  public Response deleteInput() 
  {
    final File inputDirectory = new File(MatlabController.INPUT_FILE_DIR);
    try
    {
      FileUtils.cleanDirectory(inputDirectory);
      return Response.ok("Input files deleted").build();
    }
    catch (final IOException e)
    {
      e.printStackTrace();
      return Response.status(500).entity(e).build();
    }    
  }
  
  
  @PUT
  @Path("/input/{id}")
  @Consumes("image/jpeg")
  @Produces(MediaType.TEXT_PLAIN)
  public Response putInput( @PathParam("id") String imgId, File imgFile ) 
  {
    System.out.println("Received imgId: " + imgId);
    System.out.println("Received imgFile: " + imgFile.getAbsolutePath() );
    
    try 
    {
      final int inputNumber = Integer.parseInt(imgId);
      if (inputNumber < 0 || inputNumber > MatlabController.MAX_INPUTS)
      {        
        final String errorMessage = String.format(
            "imgId '%s' exceeds bounds 0 to %d", 
            imgId, MatlabController.MAX_INPUTS);
        System.out.println(errorMessage);
        
        final ResponseBuilder response = 
            Response.status(Response.Status.BAD_REQUEST).entity(errorMessage);
        
        System.out.println("Response: " + response);
        return response.build();
      }
      
      // Jersey stores the image on disk for us (File imgFile)
      // So we just need to move the image
      final String destinationFilename = 
        MatlabController.mapInputNumberToFilename(inputNumber);
      final File destination = new File(destinationFilename);
      
      System.out.format("Moving file from: %s to %s\n", 
        imgFile.getAbsolutePath(), 
        destination.getAbsolutePath());
      
      boolean success;
      try 
      {
        Files.move(imgFile.toPath(), destination.toPath(), 
            StandardCopyOption.REPLACE_EXISTING);
        success = true;
      }
      catch (final Exception e)
      {
        success = false;
        System.err.println(e);
      }
      
      // old way
      //final boolean success = imgFile.renameTo(destination);
      
      System.out.format(success ? "Success\n" : "Failed\n");

      final ResponseBuilder response = success ?  
        Response.ok("Accepted image as " + destinationFilename) :
        Response.status(500).entity("Failed to move file to " + destinationFilename);
      return response.build();
    }
    catch (final NumberFormatException nfe)
    {
      final String errorMessage = String.format(
          "imgId '%s' can not be interpretted as a integer", 
          imgId);
      System.out.println(errorMessage);
      
      final ResponseBuilder response = 
          Response.status(Response.Status.BAD_REQUEST).entity(errorMessage);
      
      System.out.println("Response: " + response);
      
      return response.build();
    }     
  }
  
 
  
  @POST
  @Path("/requestProcessing")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public void requestProcessing(
      final ProcessRequest request,
      @Suspended final AsyncResponse asyncResponse)
  {
    System.out.format("requestProcessing called with requestId: %s\n", 
                      request.requestId);
    
    // Error check the input object
    if (request.base64Images == null || request.base64Images.length == 0)
    {
      final String errorMessage = String.format(
        "Incorrect number of images (%d) found in request", 
        request.base64Images == null ? 0 : request.base64Images.length);

      System.out.println(errorMessage);
      
      final ResponseBuilder response = 
          Response.status(Response.Status.BAD_REQUEST).entity(errorMessage);
      
      throw new ClientErrorException(response.build());
    }

    
    // Enqueue the request.
    if (!MatlabController.getSingleton().addRequest(request, asyncResponse))
    {
      // Should only occur if the MatlabController failed to connect to 
      // Matlab.
      asyncResponse.resume(
          Response.status(Response.Status.SERVICE_UNAVAILABLE)
          .entity("Request was rejected, Matlab processing unavailable.")
          .build());
    }
        
  }
    
}