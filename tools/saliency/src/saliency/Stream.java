////////////////////////////////////////////////////////////////////////////////
// Copyright (c)  2015 General Dynamics Inc.
////////////////////////////////////////////////////////////////////////////////

package saliency;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import shared.ProcessRequest;
import shared.StreamConfiguration;

/**
 * Experimental REST API in which a "Stream" is created on the server.
 * The server keeps track of necessary data about a Stream allowing the API
 * user to continuously POST individual (or multiple) images and get back 
 * the most current results (results incorporate previous POSTed images).  
 * 
 */
@Path("/stream")
public class Stream
{
  
  
  @POST
  @Path("/{id}/configure")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public Response configureStream(
      @PathParam("id") String streamId,
      final StreamConfiguration config)
  {
    return MatlabController.getSingleton().configureStream(streamId, config);    
  }
  
  
  @POST
  @Path("/{id}/images")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public void postImages(
      @PathParam("id") String streamId,
      final ProcessRequest request,
      @Suspended final AsyncResponse asyncResponse)
  {
    
    System.out.format("requestProcessing called on steam: %s\n", 
      streamId);
    
    request.setStreamId(streamId);
    
    // Error check the input object
    if (request.base64Images == null || request.base64Images.length == 0)
    {
      final String errorMessage = String.format(
        "Incorrect number of images (%d) found in request", 
        request.base64Images == null ? 0 : request.base64Images.length);

      System.out.println(errorMessage);
      
      final ResponseBuilder response = 
          Response.status(Response.Status.BAD_REQUEST).entity(errorMessage);
      
      throw new ClientErrorException(response.build());
    }
    
    
    // Enqueue the request.
    if (!MatlabController.getSingleton().addRequest(request, asyncResponse))
    {
      // Should only occur if the MatlabController failed to connect to 
      // Matlab.
      asyncResponse.resume(
          Response.status(Response.Status.SERVICE_UNAVAILABLE)
          .entity("Request was rejected, Matlab processing unavailable.")
          .build());
    }
  }
  
  
  
  
  
}
