////////////////////////////////////////////////////////////////////////////////
// Copyright (c)  2015 General Dynamics Inc.
////////////////////////////////////////////////////////////////////////////////


package shared;

import javax.xml.bind.annotation.XmlRootElement;


/**
 * Class describes the JSON data that will be sent to the server when
 * the client issues certain commands.
 */
@XmlRootElement
public class ProcessRequest
{
  /**
   * Default constructor needed for Moxy to handle JSON encoding/decoding.
   */
  public ProcessRequest() {}
  
  
  // TODO: Remove this one when we sunset the PUT interface.
  /**
   * Constructor.  Use when images are already available on the server
   * @param request String - the requestId
   */
  public ProcessRequest(final String request)
  {
    requestId = request;
    base64Images = null;
  }
  
  
  /**
   * Constructor.
   * @param request String - the requestId
   * @param imageValues String[] an array of base64 encoded images
   */
  public ProcessRequest(final String request, final String[] imageValues)
  {
    requestId = request;
    base64Images = imageValues;
  }
  
  /**
   * Alternative constructor for use when only a single base64 image is
   * being sent
   * @param request String - the requestId
   * @param imageValue String - base64 encoded image data
   */
  public ProcessRequest(final String request, final String imageValue)
  {
    requestId = request;
    base64Images = new String[1];
    base64Images[0] = imageValue;
  }
  
  public String requestId;
  public String[] base64Images;
  
  
  public void setRespondWithJson(final boolean value) 
  {
    respondWithJson_ = value; 
  }
  
  public boolean getRespondWithJson()
  {
    return respondWithJson_;
  }
  
  public void setRequestContainsImages(final boolean value) 
  {
    requestContainsImages_ = value; 
  }
  
  public boolean getRequestContainsImages()
  {
    return requestContainsImages_;
  } 
  
  public String getStreamId()
  {
    return streamId_;
  }
  
  public void setStreamId(final String id)
  {
    streamId_ = id;
  }
  
  private boolean respondWithJson_ = true;
  
  private boolean requestContainsImages_ = true;
  
  private String streamId_ = null;
}