////////////////////////////////////////////////////////////////////////////////
// Copyright (c)  2015 General Dynamics Inc.
////////////////////////////////////////////////////////////////////////////////

package shared;

public class Channel
{
  public enum CHANNEL_TYPE
  {
    Color("C"),
    Intensity("I"),
    Orientation("O"),
    Contrast("R"),
    Flicker("F"),
    Motion("M"),
    DKLColor("D");

    
    public String getLetter()
    {
      return letter_;
    }
    
    private CHANNEL_TYPE(final String letter)
    {
      letter_ = letter;
    }
    
    private final String letter_;
  }
  
  
  
  
  public Channel()
  {
    weight = 1.0f;
  }
  
  public Channel(final CHANNEL_TYPE channelType, 
                 final float channelWeight)
  {
    type = channelType;
    weight = channelWeight;
  }
  

  public CHANNEL_TYPE type;
  public float weight; // 0.0 to 1.0

}
