////////////////////////////////////////////////////////////////////////////////
// Copyright (c)  2015 General Dynamics Inc.
////////////////////////////////////////////////////////////////////////////////


package client;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;


/**
 * Tests runs DELETE against <server>/rest/matlab/input
 * 
 * Accepts command line arguments (in order):
 * Address
 * 
 * Example usage:
 * TestDeleteInput 127.0.0.1:8080
 * 
 * Address is an optional argument that will default to 127.0.0.1:8080
 */
public class TestDeleteInput
{
  public static void main(String[] args) {

    final String ipOrHost = args.length > 0 ? args[0] : "127.0.0.1:8080"; 
    
    final ClientConfig config = new ClientConfig();

    final Client client = ClientBuilder.newClient(config);

    final WebTarget target = client.target(getBaseURI(ipOrHost));
    
    final WebTarget inputWT = target.path("input");
    
    System.out.println("DELETING");
    final Builder request = inputWT.request();
    final Response response =
      request.delete();
    System.out.println(response);    
  }

 
  /**
   * Composes a URI from the ip address or hostname being used.
   * @param ipOrName The ip address (127.0.0.1) or host
   * @param port The port the server is running on
   * @return URI 
   */
  private static URI getBaseURI(final String ipOrHost) 
  {
    final String uriString = String.format("http://%s/saliency/rest/matlab",
      ipOrHost);
   
    return UriBuilder.fromUri(uriString).build();
  }
}
