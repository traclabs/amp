////////////////////////////////////////////////////////////////////////////////
// Copyright (c)  2015 General Dynamics Inc.
////////////////////////////////////////////////////////////////////////////////


package client;

import java.io.File;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

/**
 * Tests uses PUT /rest/matlab/input/{imgId} to PUT images 0, 1, 2, and 3.
 * then uses GET /rest/matlab/result to retrieve the result from the server.
 * 
 * The filenames are hardcoded to c:\SBIR\inputdata\085.jpg - 088.jpg 
 * See TestRequesatProcessing_directory_upload.java for an example of reading
 * a directory.
 * 
 */
public class TestPUTGET {

  public static void main(String[] args) {

    final ClientConfig config = new ClientConfig();

    final Client client = ClientBuilder.newClient(config);

    final WebTarget target = client.target(getBaseURI());
    
    final WebTarget inputWT = target.path("rest").path("matlab").path("input");
    
    System.out.println("Posting File 085.jpg");
    final Response r0 = postImage(inputWT, "0", "C:\\SBIR\\inputset1\\085.jpg");        
    System.out.println("response: " + r0);
    
    System.out.println("Posting File 086.jpg");
    final Response r1 = postImage(inputWT, "1", "C:\\SBIR\\inputset1\\086.jpg");        
    System.out.println("response: " + r1);
    
    System.out.println("Posting File 087.jpg");
    final Response r2 = postImage(inputWT, "2", "C:\\SBIR\\inputset1\\087.jpg");        
    System.out.println("response: " + r2);
    
    System.out.println("Posting File 088.jpg");
    final Response r3 = postImage(inputWT, "3", "C:\\SBIR\\inputset1\\088.jpg");        
    System.out.println("response: " + r3);
    
    
    System.out.println("Requesting resulting image");
    final WebTarget resultWT = target.path("rest").path("matlab").path("result");
    
    final Builder request = resultWT.request();
    final File response = request.get(File.class);
    System.out.format("Got back a File, see: %s\n", response.getAbsolutePath());
    
    try
    {
      final File destination = new File("c:\\SBIR\\results.jpg");
      Files.move(response.toPath(), destination.toPath(), 
        StandardCopyOption.REPLACE_EXISTING);
      System.out.format("Moving file to %s\n", destination.getAbsolutePath());
    }
    catch (final Exception e)
    {
      System.err.println(e);
    }
    
  }

  
  
  private static Response postImage(
      final WebTarget target,
      final String imgId,
      final String filename)
  {
    final File imgFile = new File(filename);
        
    final Builder request =
      target.path(imgId).request();
    final Response response = 
      request.put(Entity.entity(imgFile, "image/jpeg"));
    return response;
  }

  private static URI getBaseURI() 
  {
    return UriBuilder.fromUri("http://127.0.0.1:8080/saliency").build();
  }
} 

