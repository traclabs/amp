////////////////////////////////////////////////////////////////////////////////
// Copyright (c)  2015 General Dynamics Inc.
////////////////////////////////////////////////////////////////////////////////

package client;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import shared.StreamConfiguration;
import shared.StreamConfigurationResponse;
import shared.Channel;




import org.glassfish.jersey.client.ClientConfig;


/**
 * Tests POSTING a Channel configuration to 
 * http://ADDRESS/saliency/rest/stream/{streamId}/configure
 * 
 * Accepts command line arguments (in order):
 *  Address StreamId
 *  
 * Example:
 * 190.1.1.2:8080 50
 * 
 * Both arguments are optional. If not specified we default to:
 * 127.0.0.1:8080 1 
 * 
 */
public class TestStreamConfigure
{
  public static void main(String[] args) 
  {
    final String ipOrHost = args.length > 0 ? args[0] : "127.0.0.1:8080"; 
    final String streamId = args.length > 1 ? args[1] : "1";
    
    final long startTime = System.currentTimeMillis();  
    
    // Using Jersey Client API to compose a POST against
    // http://ipOrHost/saliency/rest/stream/{streamId}/configure
    final ClientConfig clientConfig = new ClientConfig();
    final Client client = ClientBuilder.newClient(clientConfig);
    final WebTarget target = client.target(getBaseURI(ipOrHost));    
    final WebTarget configWT = target.path(streamId).path("configure");
    
    final StreamConfiguration streamConfig = new StreamConfiguration();
    streamConfig.channels.add(new Channel(Channel.CHANNEL_TYPE.DKLColor, 1.0f));
    streamConfig.channels.add(new Channel(Channel.CHANNEL_TYPE.Intensity, 1.0f));
    streamConfig.channels.add(new Channel(Channel.CHANNEL_TYPE.Motion, 1.0f));
    streamConfig.channels.add(new Channel(Channel.CHANNEL_TYPE.Orientation, 1.0f));
    streamConfig.levels = 3;
    
    // POST the streamConfiguration object (automatically converted to JSON).
    // Expect back a StreamConfigurationResponse (as JSON data).
    final StreamConfigurationResponse response = 
      configWT.request(MediaType.APPLICATION_JSON_TYPE)
      .post(Entity.entity(streamConfig, MediaType.APPLICATION_JSON_TYPE), 
        StreamConfigurationResponse.class);
    
    System.out.println("Response received: ");
    System.out.format("existingStream: %s\n", response.existingStream);
    System.out.format("success: %b\n", response.success);
    
    final long endTime = System.currentTimeMillis();
    System.out.format("Total time: %d milliseconds\n", 
      endTime - startTime);
  }
  
  
  /**
   * Composes a URI from the ip address or hostname being used.
   * @param ipOrName The ip address (127.0.0.1) or host
   * @param port The port the server is running on
   * @return URI 
   */
  private static URI getBaseURI(final String ipOrHost) 
  {
    final String uriString = String.format("http://%s/saliency/rest/stream",
                                           ipOrHost);
    
    return UriBuilder.fromUri(uriString).build();
  }
}
