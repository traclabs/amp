////////////////////////////////////////////////////////////////////////////////
// Copyright (c)  2015 General Dynamics Inc.
////////////////////////////////////////////////////////////////////////////////

package client;

import java.io.File;
import java.io.FileFilter;
import java.net.URI;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

import shared.Channel;
import shared.ProcessRequest;
import shared.ProcessResponse;
import shared.StreamConfiguration;
import shared.StreamConfigurationResponse;
import shared.Utilities;

/**
 * Tests POSTING multiple images to 
 * http://ADDRESS/saliency/rest/stream/{streamId}/images
 * 
 * This test will wait for the first POST to return before running the next one.
 * (POSTs will resolve in order).
 * As each POST is resolved any images that come back from the server are saved
 * to the outputdirectory.
 * 
 * Accepts command line arguments (in order):
 * InputDirectory OutputDirectory Address StreamId 
 *  
 * Example:
 * c:\inputset1 c:\resultset1 190.1.1.2:8080 50 
 * 
 * Address and StreamId are optional arguments which will default to:
 * 127.0.0.1:8080 and 1 if not specified.
 * 
 */
public class TestStreamingPosts
{
  public static void main(String[] args) 
  {
    final long startTime = System.currentTimeMillis();  
    
    // Assumes that first argument is a directory
    if (args.length < 2)
    {
      System.err.println("Must specify at least two arguments.\n"
          + "\tUsage: inputDirectory outputDirectory Address SteamId\n"
          + "\t\tAddress is optional and will default to 127.0.0.1:8080\n"
          + "\t\tStreamId is optional and will default to 1");
      return;
    }
    
    final String inputString = args[0];
    final File inputFile = new File(inputString);
    if (!inputFile.exists())
    {
      System.err.format("Input directory \"%s\" does not exist.\n", 
                        inputFile.getAbsolutePath());
      return;
    }
    
    // Get the list of jpg/jpeg files (filter out anything else).
    final File[] listOfFiles = inputFile.listFiles(new FileFilter() 
    {
      @Override
      public boolean accept(final File pathname)
      {
        return pathname.getName().endsWith(".jpg") ||
               pathname.getName().endsWith(".jpeg");
      }
    });
    
    // Sort the list, the intention is that image files should end with 01, 02,
    // 03, etc.
    Arrays.sort(listOfFiles);
    
    String outputDir = args[1];
    if (!outputDir.endsWith(File.separator))
    {
      outputDir += File.separator;
    }
    
    // Create directory if it does not exist
    final File resultsDirFile = new File(outputDir);
    if (!resultsDirFile.exists())
    {
      if (!resultsDirFile.mkdirs())
      {
        System.err.format("Unable to create output directory: %s\n",
            resultsDirFile.getAbsolutePath());
        return;
      }
    }
    
    final String ipOrHost = args.length > 2 ? args[2] : "127.0.0.1:8080"; 
    final String streamId = args.length > 3 ? args[3] : "1";
    
    // Using Jersey Client API to compose a POST against
    // http://ipOrHost/saliency/rest/stream/{streamId}/configure
    final ClientConfig clientConfig = new ClientConfig();
    final Client client = ClientBuilder.newClient(clientConfig);
    final WebTarget target = client.target(getBaseURI(ipOrHost));    
    final WebTarget imagesWT = target.path(streamId).path("images");
    final WebTarget configWT = target.path(streamId).path("configure");
    
    // Comment if we want to not re-make the stream
    {
    final StreamConfiguration streamConfig = new StreamConfiguration();
    streamConfig.channels.add(new Channel(Channel.CHANNEL_TYPE.DKLColor, 1.0f));
    streamConfig.channels.add(new Channel(Channel.CHANNEL_TYPE.Intensity, 1.0f));
    streamConfig.channels.add(new Channel(Channel.CHANNEL_TYPE.Motion, 1.0f));
    streamConfig.channels.add(new Channel(Channel.CHANNEL_TYPE.Orientation, 1.0f));
    streamConfig.levels = 3;
    
    // POST the streamConfiguration object (automatically converted to JSON).
    // Expect back a StreamConfigurationResponse (as JSON data).
    
    final StreamConfigurationResponse response = 
      configWT.request(MediaType.APPLICATION_JSON_TYPE)
      .post(Entity.entity(streamConfig, MediaType.APPLICATION_JSON_TYPE), 
        StreamConfigurationResponse.class);
    
    System.out.println("Response received: ");
    System.out.format("existingStream: %s\n", response.existingStream);
    System.out.format("success: %b\n", response.success);
    }
    
    // Read all images as base64 Strings.
    final List<String> imageData = new LinkedList<String>();    
    for (final File f : listOfFiles)
    {
      final String base64Image = Utilities.encodeImageFileToBase64(f); 
      if (base64Image != null)
      {
        imageData.add(base64Image);
      }
    }
    
    // Iterate through images, post one image at a time.
    int imagesReceived = 0;
    for (int i = 0; i < imageData.size(); i++)
    {
      System.out.format("Making POST with 1 image\n");
      final long startPostTime = System.currentTimeMillis();
      
      // Jersey will use automatically detect and use Moxy (jersey-media-moxy)
      // to serialize & deserialize ProcessRequest as JSON.
      final ProcessRequest processRequest = 
        new ProcessRequest(
         Integer.toString(i), 
         imageData.get(i));
      
      final ProcessResponse response = imagesWT.
        request(MediaType.APPLICATION_JSON_TYPE).
        post(Entity.entity(processRequest, MediaType.APPLICATION_JSON_TYPE), 
          ProcessResponse.class);
     
      final long endPostTime = System.currentTimeMillis();
      System.out.format("POST took: %d milliseconds\n", 
          endPostTime - startPostTime);
      
      // Create result file
      for (int j = 0; j < response.resultImages.length; j+=1)
      {
        final String imageName = String.format("%s%03d.jpg", 
          outputDir, imagesReceived + 1);
        Utilities.decodeBase64ToImageFile(response.resultImages[j], imageName);
        imagesReceived += 1;
      }
      
    }
    
    
    final long endTime = System.currentTimeMillis();
    System.out.format("Total time: %d milliseconds\n", 
      endTime - startTime);    
  }
  
  
  /**
   * Composes a URI from the ip address or hostname being used.
   * @param ipOrName The ip address (127.0.0.1) or host
   * @param port The port the server is running on
   * @return URI 
   */
  private static URI getBaseURI(final String ipOrHost) 
  {
    final String uriString = String.format("http://%s/saliency/rest/stream",
                                           ipOrHost);
    
    return UriBuilder.fromUri(uriString).build();
  }
}
