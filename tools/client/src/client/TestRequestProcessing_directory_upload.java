////////////////////////////////////////////////////////////////////////////////
// Copyright (c)  2015 General Dynamics Inc.
////////////////////////////////////////////////////////////////////////////////


package client;

import java.io.File;
import java.io.FileFilter;
import java.net.URI;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

import shared.ProcessRequest;
import shared.ProcessResponse;
import shared.Utilities;

/**
 * Tests POSTing multiple images to
 * http://ADDRESS/saliency/rest/matlab/requestProcessing
 * 
 * This will read all jpg files in the input directory, encode them using base64 
 * and send them in JSON format as a single POST operation.  The Client blocks
 * until the server responds with the resulting images which will be decoded and
 * stored as consecutive files in the OutputDirectory
 *  
 * Accepts command line arguments (in order):
 * InputDirectory OutputDirectory Address
 * 
 * Example usage:
 * TestRequestProcessing_directory_upload C:\SBIR\inputset1 c:\SBIR\results 127.0.0.1:8080
 * 
 * Address is an optional argument that will default to 127.0.0.1:8080
 * 
 */
public class TestRequestProcessing_directory_upload
{
  public static void main(String[] args) 
  {
    final long startTime = System.currentTimeMillis();
    
    // Assumes that first argument is a directory
    if (args.length < 2)
    {
      System.err.println("Must specify at least two arguments.\n"
          + "\tUsage: inputDirectory outputDirectory Address\n"
          + "\t\tAddress is optional and will default to 127.0.0.1:8080\n");          
      return;
    }
    
    final String inputString = args[0];    
    final File inputFile = new File(inputString);
    if (!inputFile.exists())
    {
      System.err.format("Input directory \"%s\" does not exist.\n", 
                        inputFile.getAbsolutePath());
      return;
    }
    
    // Get the list of jpg/jpeg files (filter out anything else).
    final File[] listOfFiles = inputFile.listFiles(new FileFilter() 
    {
      @Override
      public boolean accept(final File pathname)
      {
        return pathname.getName().endsWith(".jpg") ||
               pathname.getName().endsWith(".jpeg");
      }
    });
    
    // Sort the list, the intention is that image files should end with 01, 02,
    // 03, etc.
    Arrays.sort(listOfFiles);
    
    String outputDir = args[1];
    if (!outputDir.endsWith(File.separator))
    {
      outputDir += File.separator;
    }
    // Create directory if it does not exist
    final File resultsDirFile = new File(outputDir);
    if (!resultsDirFile.exists())
    {
      if (!resultsDirFile.mkdirs())
      {
        System.err.format("Unable to create output directory: %s\n",
          resultsDirFile.getAbsolutePath());
        return;
      }
    }
    
    final String ipOrHost = args.length > 2 ? args[2] : "127.0.0.1:8080"; 
    
    
    // Using Jersey Client API to compose a POST against
    // URL http://ipOrHost/saliency/rest/matlab/requestProcessing
    final ClientConfig config = new ClientConfig();
    final Client client = ClientBuilder.newClient(config);
    final WebTarget target = client.target(getBaseURI(ipOrHost));    
    final WebTarget requestProcessingWt = target.path("requestProcessing");
    
    // Read all images as base64 Strings.
    final List<String> imageData = new LinkedList<String>();    
    for (final File f : listOfFiles)
    {
      final String base64Image = Utilities.encodeImageFileToBase64(f); 
      if (base64Image != null)
      {
        imageData.add(base64Image);
      }
    }
    
    // Jersey will use automatically detect and use Moxy (jersey-media-moxy)
    // to serialize & deserialize ProcessRequest as JSON.
    final ProcessRequest processRequest = 
      new ProcessRequest(
        "1000", 
       imageData.toArray(new String[imageData.size()]));
    
    // Make a request that asks for JSON back.  
    // POST operation, using data in processRequest
    System.out.format("Making POST with %d images\n", imageData.size());
    
    final long startPostTime = System.currentTimeMillis();
    
    final ProcessResponse response = requestProcessingWt.
      request(MediaType.APPLICATION_JSON_TYPE).
      post(Entity.entity(processRequest, MediaType.APPLICATION_JSON_TYPE), 
        ProcessResponse.class);
    
    final long endPostTime = System.currentTimeMillis();
    System.out.format("POST took: %d milliseconds\n", 
                      endPostTime - startPostTime);
    
    System.out.println("Response received: ");
    System.out.format("Response request Id: %s\n", response.requestId);
    System.out.format("Response success: %b\n", response.success);
    System.out.format("Response contains: %d images\n", 
                      response.resultImages.length);
    
    System.out.format("Writing resulting images to %s\n", outputDir);
        
    // Create result files "001.jpg" to "00X.jpg"
    for (int i=0; i < response.resultImages.length; i+=1)
    {
      final String imageName = String.format("%s%03d.jpg", 
        outputDir, i + 1);
      Utilities.decodeBase64ToImageFile(response.resultImages[i], imageName);
    }
    
    final long endTime = System.currentTimeMillis();
    System.out.format("Total time: %d milliseconds\n", 
        endTime - startTime);
  }
  
  
  /**
   * Composes a URI from the ip address or hostname being used.
   * @param ipOrName The ip address (127.0.0.1) or host
   * @param port The port the server is running on
   * @return URI 
   */
  private static URI getBaseURI(final String ipOrHost) 
  {
    final String uriString = String.format("http://%s/saliency/rest/matlab",
      ipOrHost);
   
    return UriBuilder.fromUri(uriString).build();
  }
  
}



