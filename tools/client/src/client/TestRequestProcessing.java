////////////////////////////////////////////////////////////////////////////////
// Copyright (c)  2015 General Dynamics Inc.
////////////////////////////////////////////////////////////////////////////////


package client;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

import shared.ProcessRequest;
import shared.ProcessResponse;
import shared.Utilities;

/**
 * This program makes a POST command to the server with
 *  hard coded input and output files.
 *  
 *  See TestRequestProcessing_directory_upload for an example that supports
 *  specifying inputs & outputs  
 */
public class TestRequestProcessing
{
  public static void main(String[] args) 
  {
    final ClientConfig config = new ClientConfig();
  
    final Client client = ClientBuilder.newClient(config);
  
    final WebTarget target = client.target(getBaseURI());
    
    final WebTarget requestProcessingWt = target.path("rest").path("matlab")
        .path("requestProcessing");
    
    final String base64Image0 = Utilities.encodeImageFileToBase64(
      "C:\\SBIR\\inputset1\\085.jpg");
    
    final String base64Image1 = Utilities.encodeImageFileToBase64(
      "C:\\SBIR\\inputset1\\086.jpg");
        
    final String base64Image2 = Utilities.encodeImageFileToBase64(
      "C:\\SBIR\\inputset1\\087.jpg");
        
    final String base64Image3 = Utilities.encodeImageFileToBase64(
      "C:\\SBIR\\inputset1\\088.jpg");
    
    final String[] imageData = {base64Image0, base64Image1, 
                                base64Image2, base64Image3};
    
    ProcessRequest processRequest = 
      new ProcessRequest(
        "1000", 
        imageData);
    
    // Make a request that asks for JSON back.  
    // POST operation, using data in processRequest
    
    System.out.println("Making POST");
    final ProcessResponse response = requestProcessingWt.
      request(MediaType.APPLICATION_JSON_TYPE).
      post(Entity.entity(processRequest, MediaType.APPLICATION_JSON_TYPE), 
          ProcessResponse.class);
    
    System.out.println("Response received: ");
    System.out.format("Response request Id: %s\n", response.requestId);
    System.out.format("Response success: %b\n", response.success);
    System.out.format("Response contains: %d images\n", 
                      response.resultImages.length);
    
    // Create result files "001.jpg" to "00X.jpg"
    for (int i=0; i < response.resultImages.length; i+=1)
    {
      final String imageName = String.format("c:\\SBIR\\results\\%03d.jpg", 
        i + 1);
      Utilities.decodeBase64ToImageFile(response.resultImages[i], imageName);
    }
    
  }

    
  private static URI getBaseURI() 
  {
    return UriBuilder.fromUri("http://127.0.0.1:8080/saliency").build();
  }
  
}



