import java.util.LinkedList;

/**
 * File created by TRACLabs
 * User: oliverl3
 * Date: 8/16/13
 */
public class Target {


    private String name;
    private Vehicle.Payload intendedPayload;
    private Vehicle.TerrainMedium medium;
    private PositionObj pos;

    private boolean done;


    public Target(){
        
        
    }


    public String getName() {
        return name;
    }

    public Vehicle.Payload getIntendedPayload() {
        return intendedPayload;
    }

    public Vehicle.TerrainMedium getMedium() {
        return medium;
    }

    public PositionObj getPos() {
        return pos;
    }

    public boolean isDone() {
        return done;
    }
    
}
