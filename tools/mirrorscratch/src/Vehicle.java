import java.util.LinkedList;

/**
 * File created by TRACLabs
 * User: oliverl3
 * Date: 8/16/13
 */
public class Vehicle {

    public enum VehicleClass{
        firescout,globalhawk,talisman, reaper, spartanscout
    }

    public enum Payload{
        weapons, inspectcam, SRsensors, LRsensors,
        //kill, inspect, monitor, surveil
        airbase, seabase
    }

    public enum TerrainMedium {
        air, water, land
    }

    
    private String name;
    private VehicleClass vehicleClass;
    private Payload payload[];
    private TerrainMedium medium;
    private PositionObj pos;
    private double velocityActual, velocityNominal;
    private int status;
    private double vDamage;
    private boolean intersect;
    private Target target;
    private LinkedList<PositionObj> path;
    private int UUV_stuck_count;
    private boolean UUV_stuck;
    private boolean grounded;
    
    
    public Vehicle(){
        
    }




    public String getName() {
        return name;
    }

    public VehicleClass getVehicleClass() {
        return vehicleClass;
    }

    public Payload[] getPayload() {
        return payload;
    }

    public TerrainMedium getMedium() {
        return medium;
    }
    
    public PositionObj getPos(){
        
        return pos;
    }


    public double getVelocityActual() {
        return velocityActual;
    }

    public double getVelocityNominal() {
        return velocityNominal;
    }

    public int getStatus() {
        return status;
    }

    public double getvDamage() {
        return vDamage;
    }

    public boolean isIntersect() {
        return intersect;
    }

    public Target getTarget() {
        return target;
    }

    public LinkedList<PositionObj> getPath() {
        return path;
    }

    public int getUUV_stuck_count() {
        return UUV_stuck_count;
    }

    public boolean isUUV_stuck() {
        return UUV_stuck;
    }

    public boolean isGrounded() {
        return grounded;
    }



}
