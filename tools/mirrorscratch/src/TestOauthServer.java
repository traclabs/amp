import com.google.gson.Gson;
import com.sun.net.httpserver.*;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;


import javax.net.ssl.*;
import javax.swing.*;
import java.io.*;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URLDecoder;
import java.security.*;
import java.util.*;

/**
 * File created by TRACLabs
 * User: oliverl3
 * Date: 8/1/13
 */
public class TestOauthServer {

    
    final String client_id="884948243261-vj3hgmick1lq7tsvqo1v5v4efj7r5a6a.apps.googleusercontent.com";
    final String client_secret="TRAFHqlySWiIg7akxk8G7wyW";

    final String redirect_uri_URLENCODED = "https%3A%2F%2Fgoogleglass.traclabs.com%3A8080%2Foauth2callback";
    final String redirect_uri = "https://googleglass.traclabs.com:8080/oauth2callback";

    private ProperMirrorClientHttpHandler propermirrorclientHandler;
    
    
    static String accessToken;
    
    
    public TestOauthServer(){

        HttpsServer secureServer;

        try {
            //server = HttpServer.create(new InetSocketAddress(8080), 0);
            secureServer=HttpsServer.create(new InetSocketAddress(8080), 0);
        }
        catch (IOException e) {
            System.err.println("exception with creating server");
            System.exit(-1);
            return;
        }



        try{
            System.out.println("Initializing context ...");
            KeyStore ks = KeyStore.getInstance("JKS");
            char[] password = "password".toCharArray();
            ks.load(new FileInputStream(".keystore"), password);
            KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
            kmf.init(ks, password);
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(kmf.getKeyManagers(), null, null);

            //  a HTTPS server must have a configurator for the SSL connections.
            secureServer.setHttpsConfigurator (new HttpsConfigurator(sslContext){
                //  override configure to change default configuration.
                public void configure (HttpsParameters params){
                    try{
                        //  get SSL context for this configurator
                        SSLContext c = getSSLContext();

                        //  get the default settings for this SSL context
                        SSLParameters sslparams = c.getDefaultSSLParameters();

                        //  set parameters for the HTTPS connection.
                        params.setNeedClientAuth(true);
                        params.setSSLParameters(sslparams);
                        System.out.println("SSL context created ...");
                    }
                    catch(Exception e2){
                        System.err.println("Invalid parameter ...");
                        e2.printStackTrace();
                    }
                }
            });
        }
        catch(Exception e1){
            e1.printStackTrace();
        }
        
        
        System.out.println("Setting up secure server complete....\n");

        secureServer.createContext("/",new RootHttpHandler());
        secureServer.createContext("/login", new LoginHttpHandler());
        secureServer.createContext("/oauth2callback", new OauthRedirectHttpHandler());
        propermirrorclientHandler=new ProperMirrorClientHttpHandler();
        secureServer.createContext("/propermirrorclient", propermirrorclientHandler);

        secureServer.setExecutor(null); // creates a default executor
        secureServer.start();

    }
    
    public class RootHttpHandler implements HttpHandler{
        
        public RootHttpHandler(){
        }
        
        @Override
        public void handle(HttpExchange exchange) throws IOException{

            if(exchange.getRequestMethod().equals("GET")){
                System.out.println("a GET request in root handler received");

                OutputStream os = exchange.getResponseBody();

                String response="<html><body>" +
                        "<p>GET request received and handled at root</p>" +
                        "</body></html>";

                exchange.sendResponseHeaders(200, response.length());
                os.write(response.getBytes());

                os.close();
                
            }
            else if(exchange.getRequestMethod().equals("POST")){
                System.out.println("a POST request in root handler received");

                OutputStream os = exchange.getResponseBody();

                String response="<html><body>" +
                        "<p>POST request received and handled at root</p>" +
                        "</body></html>";

                exchange.sendResponseHeaders(200, response.length());
                os.write(response.getBytes());

                os.close();
            }
            else{
                System.out.println("a request of some random method in root handler received");

                OutputStream os = exchange.getResponseBody();

                String response="<html><body>" +
                        "<p>A request neither GET nor POST received and handled at root</p>" +
                        "</body></html>";

                exchange.sendResponseHeaders(200, response.length());
                os.write(response.getBytes());

                os.close();
            }
        }
    }
    
    
    public class LoginHttpHandler implements HttpHandler {

        public LoginHttpHandler() {
        }

        @Override
        public void handle(HttpExchange exchange) throws IOException {
            
            
            if(exchange.getRequestMethod().equals("GET")){

                OutputStream os = exchange.getResponseBody();
                
                String response="<html><body>" +
                        "<a href='https://accounts.google.com/o/oauth2/auth?state=%2Fprofile&amp;" +
                        "redirect_uri=" + redirect_uri_URLENCODED + "&amp;" +
                        "response_type=code&amp;" +
                        "client_id=884948243261-vj3hgmick1lq7tsvqo1v5v4efj7r5a6a.apps.googleusercontent.com&amp;" +
                        "approval_prompt=force&amp;" +
                        "scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fglass.timeline+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile'>Login</a>" +
                        "</body></html>";
                
                exchange.sendResponseHeaders(200, response.length());
                os.write(response.getBytes());

                os.close();
            }
        }
    }

    public class OauthRedirectHttpHandler implements HttpHandler{

        public OauthRedirectHttpHandler(){
        }

        @Override
        public void handle(HttpExchange exchange) throws IOException {

            //extract authorization code from GET params
            Map<String,Object> redirectedGetparams=TestOauthServer.parseGetParameters(exchange);
            String authenticationCode=(String)redirectedGetparams.get("code");

            System.out.println("Authentication code is: "+ authenticationCode);


            //send a post request to google that contains the secret and the auth code

            HttpClient httpclient = new DefaultHttpClient();

            String postToForTokenURL="https://accounts.google.com/o/oauth2/token";
            HttpPost postReqestForToken=new HttpPost(postToForTokenURL);


            List<NameValuePair> postForTokenParams = new ArrayList<NameValuePair>();

            postForTokenParams.add(new BasicNameValuePair("code", authenticationCode));
            postForTokenParams.add(new BasicNameValuePair("client_id", client_id));
            postForTokenParams.add(new BasicNameValuePair("client_secret", client_secret));
            postForTokenParams.add(new BasicNameValuePair("redirect_uri", redirect_uri));//you have to give the redirect uri, but I don't think it will actually redirect you to there after this post request
            postForTokenParams.add(new BasicNameValuePair("grant_type", "authorization_code"));

            postReqestForToken.setEntity(new UrlEncodedFormEntity(postForTokenParams, "UTF-8"));

            HttpResponse postResponse=httpclient.execute(postReqestForToken);//this response should contain the token in a JSON object

            String jsonstring = getResponseBodyAsString(postResponse);

            Gson gson=new Gson();
            TokenContainingGoogleResponse tokenResponse=gson.fromJson(jsonstring, TokenContainingGoogleResponse.class);

            accessToken=tokenResponse.access_token;

            System.out.println("Access token: "+accessToken);

            updateToken();
            
            
            // display the code and token
            OutputStream os = exchange.getResponseBody();
            String displayresponse="<html><body>" +
                    "<p>Authentication code is: "+authenticationCode+"</p>"+
                    "<p>Access token is: "+accessToken+"</p>"+
                    //"<p>Status code of response to timeline post request: "+timelineResponse.getStatusLine().getStatusCode()+timelineResponse.getStatusLine().getReasonPhrase()+"</p>"+
                    "<a href=\"https://googleglass.traclabs.com:8080/propermirrorclient\">alright cool story</a>"+
                    "</body></html>";
            exchange.sendResponseHeaders(200, displayresponse.length());
            os.write(displayresponse.getBytes());
            os.close();
        }
    }

    public void updateToken(){
        propermirrorclientHandler.updateToken();
    }



    public static String getResponseBodyAsString(HttpResponse postResponse){
        
        try{
            InputStream content=postResponse.getEntity().getContent();
            InputStreamReader is = new InputStreamReader(content);
            BufferedReader br = new BufferedReader(is);

            StringBuilder sb=new StringBuilder();
            String read = br.readLine();
            while(read != null) {
                //System.out.println(read);
                sb.append(read);
                read =br.readLine();
            }

            return sb.toString();
            
        }
        catch(IOException e){
            
            System.err.println("IO exception while trying to get the response body as a string");
            
            return null;
        }
    }

    public static Map<String, Object> parseGetParameters(HttpExchange exchange) throws UnsupportedEncodingException {

        Map<String, Object> parameters = new HashMap<String, Object>();
        URI requestedUri = exchange.getRequestURI();
        String query = requestedUri.getRawQuery();
        parseQuery(query, parameters);

        //exchange.setAttribute("parameters", parameters);// I have no idea what setAttribute does

        return parameters;
    }

    public static Map<String, Object> parsePostParameters(HttpExchange exchange) throws IOException {
        InputStreamReader bodyReader = new InputStreamReader(exchange.getRequestBody(),"utf-8");
        BufferedReader bufferedBodyReader = new BufferedReader(bodyReader);
        StringBuilder queryBuilder = new StringBuilder();
        while (bufferedBodyReader.ready()){
            String bodyLine = bufferedBodyReader.readLine();
            queryBuilder.append(bodyLine);
        }

        Map<String, Object> parameters = new HashMap<String, Object>();

        System.out.println("querry: "+queryBuilder.toString());

        parseQuery(queryBuilder.toString(), parameters);

        return parameters;
    }


    @SuppressWarnings("unchecked")
    private static void parseQuery(String query, Map<String, Object> parameters) throws UnsupportedEncodingException {

        if (query != null) {
            String pairs[] = query.split("[&]");

            for (String pair : pairs) {
                String param[] = pair.split("[=]");

                String key = null;
                String value = null;
                if (param.length > 0) {
                    key = URLDecoder.decode(param[0],
                            System.getProperty("file.encoding"));
                }

                if (param.length > 1) {
                    value = URLDecoder.decode(param[1],
                            System.getProperty("file.encoding"));
                }

                if (parameters.containsKey(key)) {
                    Object obj = parameters.get(key);
                    if(obj instanceof List<?>) {
                        List<String> values = (List<String>)obj;
                        values.add(value);
                    } else if(obj instanceof String) {
                        List<String> values = new ArrayList<String>();
                        values.add((String)obj);
                        values.add(value);
                        parameters.put(key, values);
                    }
                } else {
                    parameters.put(key, value);
                }
            }
        }
    }
    
    
    public static void main(String[] args){
        System.out.println("................TestOauthServer executing!.................");

        new TestOauthServer();
        
    }
    
}
