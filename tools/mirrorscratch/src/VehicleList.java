import java.util.List;

/**
 * File created by TRACLabs
 * User: oliverl3
 * Date: 8/16/13
 */
public class VehicleList {
    
    private List<Vehicle> vehicleList;
    
    public VehicleList(){
    }

    public List<Vehicle> getVehicleList() {
        return vehicleList;
    }
}
