import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.mirror.Mirror;
import com.google.api.services.mirror.model.*;
import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

/**
 * File created by TRACLabs
 * User: oliverl3
 * Date: 8/6/13
 */
public class ProperMirrorClientHttpHandler implements HttpHandler{
    
    static String accessToken;


    final String dividerdiv = "<div id=\"dividerdiv\">-------------------------------------------------------------------------------------------------</div>";

    final String htmlforms=
            "<div id=\"htmlforms\">" +
                "<form action=\"https://googleglass.traclabs.com:8080/propermirrorclient?action=actOnReschuThenDisplayResponse\" method=\"post\">" +
                    "<div>Reschu URL: <input type=\"text\" name=\"reschuurl\"></div> " +
                    "<div>Reschu post parameters: <input type=\"text\" name=\"reschpostparams\"></div> " +
                    "<input type=\"submit\" value=\"Do this to reschu then display reschu's response on glass\">" +
                "</form>" +
                
                "<form action=\"https://googleglass.traclabs.com:8080/propermirrorclient?action=displayInteractiveHelloMessage\" method=\"post\">"+
                    "<input type=\"submit\" value=\"Display interactive 'Hello' Message on glass\">" +
                "</form>" +

                "<form action=\"https://googleglass.traclabs.com:8080/propermirrorclient?action=repeatedlySendVehiclesToTargetsInfoToGlass\" method=\"post\">"+
                    "<input type=\"submit\" value=\"Begin sending vehicle info to glass\">" +
                "</form>" +
            "</div>";



    final Document basicDocTemplate;

    public ProperMirrorClientHttpHandler() {
        
        String basicDoc="<html><body>" +
                dividerdiv+
                htmlforms+
                "</body></html>";
        basicDocTemplate = Jsoup.parse(basicDoc);
    }


    @Override
    public void handle(HttpExchange exchange) throws IOException {
    //this handle method uses the access token to access google api in various ways

        if(accessToken==null){//this is basically catching an error
            OutputStream os = exchange.getResponseBody();
            String response="<html><body>" +
                    "<p>Access token has not yet been set and is currently equal to null</p>"+
                    "</body></html>";
            exchange.sendResponseHeaders(400, response.length());
            os.write(response.getBytes());
            os.close();
            return;
        }

        System.out.println("handling....");

        if(exchange.getRequestMethod().equals("POST")){

            System.out.println("in POST");
            Map<String,Object> actionParam=TestOauthServer.parseGetParameters(exchange);
            
            if(actionParam.containsKey("action")){

                System.out.println("url params contains 'action'");
                
                if(actionParam.get("action").equals("actOnReschuThenDisplayResponse")){//this post comes from the html control console on someone's browswer
                    doActOnReschuThenDisplayResponse(exchange);
                }
                else if(actionParam.get("action").equals("displayInteractiveHelloMessage")){ //this post comes from the html control console on someone's browswer
                    doDisplayInteractiveHelloMessage(exchange);
                }
                else if(actionParam.get("action").equals("notificationFromGlass")){// this is a post comes from the actual glass
                    System.out.println("parsing notification");
                    doParseNotificationFromGlass(exchange);
                }
                else if(actionParam.get("action").equals("repeatedlySendVehiclesToTargetsInfoToGlass")){ //table of vehicles to targets 
                    System.out.println("send vehicles and targets info to glass");
                    doVehiclesToTargetsInfoToGlass(exchange);
                }
                else{
                    OutputStream os = exchange.getResponseBody();
                    String response="<html><body>" +
                            "<p>in-URL querry parameter \"action\" not set to any recognized parameter</p>"+
                            "</body></html>";
                    exchange.sendResponseHeaders(400, response.length());
                    os.write(response.getBytes());
                    os.close();
                }
            }
            else{//bad query parameters
                OutputStream os = exchange.getResponseBody();
                String response="<html><body>" +
                        "<p>in-URL query parameter \"action\"missing</p>"+
                        "</body></html>";
                exchange.sendResponseHeaders(400, response.length());
                os.write(response.getBytes());
                os.close();
            }
        }
        else if(exchange.getRequestMethod().equals("GET")){

            OutputStream os = exchange.getResponseBody();
            exchange.sendResponseHeaders(200, basicDocTemplate.body().html().length());
            os.write(basicDocTemplate.body().html().getBytes());
            os.close();

            System.out.println("did a GET in ProperMirrorClientHandler\n");
        }
        else{
            OutputStream os = exchange.getResponseBody();
            String response="<html><body>" +
                    "<p>method not recognized</p>"+
                    "</body></html>";
            exchange.sendResponseHeaders(400, response.length());
            os.write(response.getBytes());
            os.close();
        }
    }

    private void doVehiclesToTargetsInfoToGlass(HttpExchange exchange) throws IOException {
        Thread displayInfoToGlassThread=new Thread(new Runnable() {
            @Override
            public void run() {
                doVehiclesAndTargetsInfoToGlass();
            }
        });
        displayInfoToGlassThread.start();

        basicDocTemplate.getElementById("dividerdiv").before("<p>Sending reschu's information to glass regularly</p>");
        OutputStream os = exchange.getResponseBody();
        exchange.sendResponseHeaders(200, basicDocTemplate.body().html().length());
        os.write(basicDocTemplate.body().html().getBytes());
        os.close();
    }

    private void doParseNotificationFromGlass(HttpExchange exchange) throws IOException {
        Notification notification=parseNotification(exchange.getRequestBody());

        if(notification.getItemId().equals("doinstufftoreschu")){
            System.out.println("doing stuff to reschu, notification successfully received");

            OutputStream os = exchange.getResponseBody();
            String response="<html><body>" +
                    "<p>notification successfully received and parsed</p>"+
                    "</body></html>";
            exchange.sendResponseHeaders(200, response.length());
            os.write(response.getBytes());
            os.close();

        }
        else{

            System.out.println("notification object NOT received or formatted incorrectly");
            
            OutputStream os = exchange.getResponseBody();
            String response="<html><body>" +
                    "<p>notification object NOT received or formatted incorrectly</p>"+
                    "</body></html>";
            exchange.sendResponseHeaders(400, response.length());
            os.write(response.getBytes());
            os.close();
        }
    }

    private void doDisplayInteractiveHelloMessage(HttpExchange exchange) throws IOException {
        //this message displays the hello message properly but the menu items created do not work for some reason
        
        Mirror service = getMirrorServiceObj();

        Subscription subscription = new Subscription();
        ArrayList<String> listOfOperations= new ArrayList<String>();
        listOfOperations.add("UPDATE");
        listOfOperations.add("INSERT");
        listOfOperations.add("DELETE");
        subscription.setCollection("timeline").setOperation(listOfOperations);
        subscription.setCallbackUrl("https://googleglass.traclabs.com:8080/propermirrorclient?action=notificationFromGlass");
        try {
            service.subscriptions().insert(subscription).execute();
        } catch (IOException e) {
            System.err.println("An error occurred with inserting the subscription: " + e);
        }


        TimelineItem timelineItem = new TimelineItem();
        String helloMessage="Hello Glass, From mirrorscratch";
        timelineItem.setText(helloMessage);


        MenuItem menuItem=new MenuItem();
        menuItem.setAction("custom");
        menuItem.setId("doinstufftoreschu");
        MenuValue menuValue=new MenuValue();
        menuValue.setDisplayName("do stuff to reschu");
        menuValue.setIconUrl("http://cdn.theatlantic.com/static/mt/assets/science/dronerender.jpg");
        List<MenuValue> menuValueList=new ArrayList<MenuValue>();
        menuItem.setValues(menuValueList);
        menuItem.getValues().add(menuValue);
        ArrayList<MenuItem> menuItemList=new ArrayList<MenuItem>();
        menuItemList.add(menuItem);

        timelineItem.setMenuItems(menuItemList);
        timelineItem.setNotification(new NotificationConfig().setLevel("DEFAULT"));

        //TimelineItem timelineItemFromResponse= 
        service.timeline().insert(timelineItem).execute();


        basicDocTemplate.getElementById("dividerdiv").before("<p>'Hello' message posted to glass</p>");

        OutputStream os = exchange.getResponseBody();
        exchange.sendResponseHeaders(200, basicDocTemplate.body().html().length());
        os.write(basicDocTemplate.body().html().getBytes());
        os.close();

        System.out.println("completed display interactive Hello message\n");
    }

    private void doVehiclesAndTargetsInfoToGlass() {

        String id;
        Mirror service=getMirrorServiceObj();

        try{
            List<Vehicle> vehicleList = getVehiclesListFromReschu();
            String vehiclesAndTargets = getVehiclesAndTargetsInHtml(vehicleList);

            TimelineItem timelineItem = new TimelineItem();
            timelineItem.setHtml(vehiclesAndTargets);
            
            TimelineItem timelineItemFromResponse= service.timeline().insert(timelineItem).execute();
            
            id=timelineItemFromResponse.getId();
            
            System.out.println("finished inserting to glass");
        }
        catch(IOException e){
            System.err.println("IO exception thrown while trying to INSERT vehicles and target info to glass, reshcu might not be running");
            System.exit(-1);
            return;
        }

        while(true){
            try{
                List<Vehicle> vehicleList = getVehiclesListFromReschu();
                String vehiclesAndTargets = getVehiclesAndTargetsInHtml(vehicleList);

                TimelineItem timelineItem =service.timeline().get(id).execute();

                timelineItem.setHtml(vehiclesAndTargets);
                service.timeline().update(id, timelineItem).execute();
            }
            catch(IOException e){
                System.err.println("IO exception thrown while trying to UPDATE vehicles and target info on glass");
                e.printStackTrace();
                System.exit(-1);
                return;
            }
            
            try{
                Thread.sleep(2000);
            }
            catch(InterruptedException e){
                System.err.println("InterruptedException thrown while trying to sleep");
            }
        }
    }

    private List<Vehicle> getVehiclesListFromReschu() {

        HttpClient httpclient = new DefaultHttpClient();

        HttpResponse responseFromReschu;

        try{
            HttpGet getReschu=new HttpGet("http://googleglass.traclabs.com:8000/Reschu/vehicles");
            responseFromReschu=httpclient.execute(getReschu);
        }
        catch(Exception e){
            System.out.println("reschu is probably not running");
            System.exit(-1);
            return null;
        }

        String vehiclesListJsonString=TestOauthServer.getResponseBodyAsString(responseFromReschu);
        System.out.println(vehiclesListJsonString);


        //parse the json to extract the vehicles and the data

        Gson gson=new Gson();

        VehicleList vehicleListObj=gson.fromJson(vehiclesListJsonString, VehicleList.class);

        return vehicleListObj.getVehicleList();
    }

    private String getVehiclesAndTargetsInHtml(List<Vehicle> vehicleList) {

        Document vehiclesAndTargetsTable=Jsoup.parse("<html><body>\n" +
                    "<table id=\"VTtable\">\n"+
                    "</table>\n" +
                "</body></html>");
        
        for(int i=0;i<vehicleList.size();i++){

            String vehicleName=vehicleList.get(i).getName();
            Target target=vehicleList.get(i).getTarget();
            
            String targetName;
            if(target!=null){
                targetName=target.getName();
            }
            else{
                targetName="no target";
            }
            
            
            vehiclesAndTargetsTable.getElementById("VTtable").append(
                    "<tr>\n" +
                            "<td>" + vehicleName + ": </td>\n" +
                            "<td>" + targetName + "</td>\n" +
                    "</tr>");
            
        }
        
        System.out.println("construction of html doc complete");
        return vehiclesAndTargetsTable.html();
    }



    private void doActOnReschuThenDisplayResponse(HttpExchange exchange) throws IOException {//the form from the page has just executed a POST, requesting that some request gets sent to reschu and the info sent to glass
        Map<String,Object> paramsForReschu=TestOauthServer.parsePostParameters(exchange);
        String reschuURL=(String)paramsForReschu.get("reschuurl");
        String reschuPostParams=(String)paramsForReschu.get("reschpostparams");

        HttpResponse responseFromReschu;
        
        try{
            HttpClient httpclient = new DefaultHttpClient();

            if(reschuPostParams==null){
                System.out.println("post params are null, doing it as a get");
                HttpGet getReschu=new HttpGet(reschuURL);
                responseFromReschu=httpclient.execute(getReschu);
            }
            else{
                System.out.println("post params are NOT null, doing post");
                HttpPost postReschu=new HttpPost(reschuURL);
                postReschu.setEntity(new StringEntity(reschuPostParams));
                responseFromReschu=httpclient.execute(postReschu);
            }
        }
        catch(IOException e){
            System.out.println("IO Exception, reschu server is probably not running");
            OutputStream os = exchange.getResponseBody();
            String displayresponse="<html><body>" +
                    "<p>IO Exception, reschu server is probably not running</p>"+
                    "</body></html>";
            exchange.sendResponseHeaders(400, displayresponse.length());
            os.write(displayresponse.getBytes());
            os.close();
            
            return;
        }
        System.out.println("got a response");

        String body=TestOauthServer.getResponseBodyAsString(responseFromReschu);

        Mirror service=getMirrorServiceObj();

        TimelineItem timelineItem = new TimelineItem();
        timelineItem.setText(body);
        TimelineItem timelineItemFromResponse= service.timeline().insert(timelineItem).execute();


        if(timelineItemFromResponse!=null){

            basicDocTemplate.getElementById("dividerdiv").before("<p>Post reschu info to glass complete</p>");
            basicDocTemplate.getElementById("dividerdiv").before("<p>"+body +"</p>");

            // display the code and token
            OutputStream os = exchange.getResponseBody();
            exchange.sendResponseHeaders(200, basicDocTemplate.body().html().length());
            os.write(basicDocTemplate.body().html().getBytes());
            os.close();

        }
        else{
            OutputStream os = exchange.getResponseBody();
            String displayresponse="<html><body>" +
                    "<p>Something went wrong, timeline item from response is null</p>"+
                    "</body></html>";
            exchange.sendResponseHeaders(200, displayresponse.length());
            os.write(displayresponse.getBytes());
            os.close();
        }
    }
    
    

    private Mirror getMirrorServiceObj() {
        HttpTransport transport=new NetHttpTransport();
        JsonFactory factory=new JacksonFactory();
        GoogleCredential credential=new GoogleCredential().setAccessToken(accessToken);

        return new Mirror.Builder(transport,factory,credential).setApplicationName("traclab's googleglass").build();
    }

    
    public void updateToken(){
        accessToken=TestOauthServer.accessToken;
        basicDocTemplate.getElementById("htmlforms").after("<p>Access token is: "+accessToken+"</p>");
    }

    private Notification parseNotification(InputStream requestBody) {
        try {
            JsonFactory jsonFactory = new JacksonFactory();
            return jsonFactory.fromInputStream(requestBody, Notification.class);
        }
        catch (IOException e) {
            System.out.println("An error occurred: " + e);
            return null;
        }
    }
    
}