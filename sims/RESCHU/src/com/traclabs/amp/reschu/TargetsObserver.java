package com.traclabs.amp.reschu;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.LinkedList;

import reschu.game.model.Game;
import reschu.game.model.Target;

import com.sun.net.httpserver.HttpServer;

/**
 * File created by TRACLabs
 * User: oliverl3
 * Date: 6/20/13
 */
public class TargetsObserver implements PropertyChangeListener {

    private HttpServer server;
    private Game game;
    
    
    LinkedList<com.sun.net.httpserver.HttpContext> targetContexts=new LinkedList<com.sun.net.httpserver.HttpContext>();

    LinkedList<com.sun.net.httpserver.HttpContext> vehiclesContext=new LinkedList<com.sun.net.httpserver.HttpContext>();


    public TargetsObserver(HttpServer s, Game g){

        server=s;
        game=g;


        game.reschuMap.addTargetsObserver(this);

        addContexts();
        
        
    }

    private void addContexts() {
        ArrayList<Target> currentTargets = game.reschuMap.getTargetList();
        for(int i=0;i<currentTargets.size();i++){
        	Target tempTarget = currentTargets.get(i);
            
            TargetHttpHandler handler=new TargetHttpHandler( game, tempTarget, server, "/Reschu/map/targets/"+tempTarget.getName());
            
            com.sun.net.httpserver.HttpContext context=server.createContext("/Reschu/map/targets/" + tempTarget.getName(), handler);
            
           targetContexts.add(context);
            
            
        }


        for(int i=0;i<game.getVehicleList().size();i++){

            TargetHttpHandler handler=new TargetHttpHandler(game, game.getVehicleList().getVehicle(i).getTarget(), server,"/Reschu/vehicles/"+i+"/target");

            com.sun.net.httpserver.HttpContext contextVeh=server.createContext("/Reschu/vehicles/"+ i +"/target", handler);

            vehiclesContext.add(contextVeh);
        }
    }
    
    private void stripContexts(){

        for(int i=0;i<targetContexts.size();i++){
            server.removeContext(targetContexts.get(i));
        }
        targetContexts.clear();



        for(int i=0;i<game.getVehicleList().size();i++){
            server.removeContext(vehiclesContext.get(i));
        }
        vehiclesContext.clear();
        
    }


    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {

        stripContexts();

        addContexts();
    }
}
