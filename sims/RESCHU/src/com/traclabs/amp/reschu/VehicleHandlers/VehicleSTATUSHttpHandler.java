package com.traclabs.amp.reschu.VehicleHandlers;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.traclabs.amp.reschu.ReschuHttpHandler;
import reschu.game.model.Vehicle;

import java.io.IOException;
import java.io.OutputStream;

/**
 * File created by TRACLabs
 * User: oliverl3
 * Date: 6/13/13
 */
public class VehicleSTATUSHttpHandler implements HttpHandler {


    private Vehicle vehicle;

    public VehicleSTATUSHttpHandler(Vehicle v) {
        vehicle = v;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        
        //the following in reschu.constants.MyGame.java
        /*	final static public int STATUS_VEHICLE_STASIS = 0;
            final static public int STATUS_VEHICLE_MOVING = 1;
            final static public int STATUS_VEHICLE_PENDING = 2;
            final static public int STATUS_VEHICLE_PAYLOAD = 3;*/

        if (ReschuHttpHandler.checkInvalidPath(exchange)) return;

        OutputStream os = exchange.getResponseBody();

        Gson gson=new Gson();


        String JsonResponse=gson.toJson(vehicle.getStatus());

        exchange.sendResponseHeaders(200, JsonResponse.length());
        os.write(JsonResponse.getBytes());

        os.close();
        
    }
}
