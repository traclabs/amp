package com.traclabs.amp.reschu.VehicleHandlers;

import com.google.gson.*;
import reschu.game.model.VehicleList;

import java.lang.reflect.Type;
import java.util.LinkedList;

public class VehicleListSerializer implements JsonSerializer<VehicleList> {

    @Override
    public JsonElement serialize(VehicleList src, Type typeOfSrc, JsonSerializationContext context) {

        JsonObject vehiclesJson=new JsonObject();
        Gson gson=new Gson();

        LinkedList<JsonElement> vehicleListJson=new LinkedList<JsonElement>();
        
        GsonBuilder vehicleBuilder=new GsonBuilder();
        vehicleBuilder.registerTypeAdapter(src.getVehicle(0).getClass(), new VehicleSerializer());
        Gson vehicleGson=vehicleBuilder.create();
        
        for(int i=0;i<src.size();i++){
            
            vehicleListJson.add(i,vehicleGson.toJsonTree(src.getVehicle(i)));
            
        }


        vehiclesJson.add("vehicleList",gson.toJsonTree(vehicleListJson));
        
        return vehiclesJson;
    }
}
