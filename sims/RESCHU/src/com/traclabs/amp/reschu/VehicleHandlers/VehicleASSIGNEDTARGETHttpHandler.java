package com.traclabs.amp.reschu.VehicleHandlers;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.traclabs.amp.reschu.ReschuHttpHandler;
import reschu.game.model.Vehicle;

import java.io.IOException;
import java.io.OutputStream;

/**
 * File created by TRACLabs
 * User: oliverl3
 * Date: 6/10/13
 */
public class VehicleASSIGNEDTARGETHttpHandler implements HttpHandler {

    Vehicle vehicle;
    
    public VehicleASSIGNEDTARGETHttpHandler(Vehicle v) {
        
        vehicle=v;
        
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {

        if (ReschuHttpHandler.checkInvalidPath(exchange)) return;

        OutputStream os = exchange.getResponseBody();

        Gson gson=new Gson();
        String targetName = "NONE";
        if(vehicle.getTarget()!=null){
        	targetName = vehicle.getTarget().getName();
        }
        String JsonResponse=gson.toJson(targetName);

        exchange.sendResponseHeaders(200, JsonResponse.length());
        os.write(JsonResponse.getBytes());

        os.close();
        
    }
    
    
}
