package com.traclabs.amp.reschu.VehicleHandlers;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.Map;

import reschu.constants.MyGame;
import reschu.constants.MySize;
import reschu.game.model.Game;
import reschu.game.model.Vehicle;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import com.traclabs.amp.reschu.PositionObject;
import com.traclabs.amp.reschu.ReschuHttpHandler;

/**
 * File created by TRACLabs
 * User: oliverl3
 * Date: 6/11/13
 */
public class VehiclePATHHttpHandler implements HttpHandler {
    
    Game game;
    Vehicle vehicle;
    HttpServer server;
    
    public VehiclePATHHttpHandler(Game g, Vehicle v, HttpServer s) {
        game=g;
        vehicle=v;
        server=s;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {

        //if (ReschuHttpHandler.checkInvalidPath(exchange)) return;
        
        if(exchange.getRequestMethod().equals("GET")){
            // /Reschu/vehicles/1/path/...
            String requestStr=exchange.getRequestURI().getPath();
            String[] requestPath=requestStr.split("/");
            
            //System.out.println("request path: "+requestStr);

            String contextStr=exchange.getHttpContext().getPath();
            String[] contextPath=contextStr.split("/");

            //System.out.println("context path: "+contextStr);

            OutputStream os = exchange.getResponseBody();

            if(requestPath.length==contextPath.length){
                //user wants whole path
                GsonBuilder gsonBuilder=new GsonBuilder();

                gsonBuilder.registerTypeAdapter(LinkedList.class, new PathSerializer());

                Gson gson=gsonBuilder.create();

                String JsonResponse=gson.toJson(vehicle.getPath());
                exchange.sendResponseHeaders(200, JsonResponse.length());
                os.write(JsonResponse.getBytes());
            }
            else if(requestPath[5].equals("numberofpathpoints")){
                //user wants to know how many path points are in the path
                Gson gson=new Gson();

                String JsonResponse=gson.toJson(vehicle.getPathSize());
                exchange.sendResponseHeaders(200, JsonResponse.length());
                os.write(JsonResponse.getBytes());
            }
            else if(requestPath.length>contextPath.length && Integer.parseInt(requestPath[5])<vehicle.getPathSize() ){
                //user wants to know a specific pathpoint
                Gson gson=new Gson();

                String JsonResponse=gson.toJson(new PositionObject(vehicle.getPath().get((Integer.parseInt(requestPath[5])))));
                exchange.sendResponseHeaders(200, JsonResponse.length());
                os.write(JsonResponse.getBytes());
            }
            else{
                String notRecogResponse="<h1>requestPath not recognized</h1>";
                exchange.sendResponseHeaders(400, notRecogResponse.length());
                os.write(notRecogResponse.getBytes());
            }
            os.close();
        }
        else if(exchange.getRequestMethod().equals("POST")){
            
            Map<String, Object> parameters = ReschuHttpHandler.parsePostParameters(exchange);
            OutputStream os = exchange.getResponseBody();
            
            System.out.println(parameters.get("x"));
            System.out.println(parameters.get("y"));


            if(vehicle.isGrounded()){
                String vehicleIsGroundedResponse="<h1>vehicle is grounded, cannot be assigned waypoints</h1>";
                exchange.sendResponseHeaders(400, vehicleIsGroundedResponse.length());
                os.write(vehicleIsGroundedResponse.getBytes());
                os.close();
                return;

            }

            serverDoPostSetWP(exchange, parameters, os);
            
            /*if (parameters.get("command").equals("setwp")) {
                serverDoPostSetWP(exchange, parameters, os);
            }
            else{
                Gson gson=new Gson();
                String commNotSup=gson.toJson("command not supported");
                exchange.sendResponseHeaders(400, commNotSup.length());
                os.write(commNotSup.getBytes());
            }*/
            os.close();
        }
        else if(exchange.getRequestMethod().equals("DELETE")){

            String requestStr=exchange.getRequestURI().getPath();
            String[] requestPath=requestStr.split("/");

            String contextStr=exchange.getHttpContext().getPath();
            String[] contextPath=contextStr.split("/");

            OutputStream os = exchange.getResponseBody();

            if(requestPath.length>contextPath.length && Integer.parseInt(requestPath[5])<vehicle.getPathSize() ){

                serverDoDeleteDelWP(exchange, Integer.parseInt(requestPath[5]), os);
                
                String response="";
                exchange.sendResponseHeaders(200, response.length());
                os.write(response.getBytes());
            }
            else{
                String notRecogResponse="<h1>requestPath not recognized, or no such resource to be deleted</h1>";
                exchange.sendResponseHeaders(400, notRecogResponse.length());
                os.write(notRecogResponse.getBytes());
            }
            os.close();
            
        }
        else{
            OutputStream os = exchange.getResponseBody();
            String methNotSup="method not supported";
            exchange.sendResponseHeaders(400, methNotSup.length());
            os.write(methNotSup.getBytes());
            os.close();
        }
        
    }



    private boolean serverDoPostSetWP(HttpExchange exchange, Map<String, Object> parameters, OutputStream os) throws IOException {
        //this method may as well return void, but the way it is now, false means failure, true means everything good
        int x_coord;
        int y_coord;
        Integer insertIndex = null;

        try {x_coord = Integer.parseInt((String) parameters.get("x"));}
        catch (NumberFormatException e) {
            String errorResponse = "x misformated or not given";
            exchange.sendResponseHeaders(400, errorResponse.length());
            os.write(errorResponse.getBytes());
            return false;
        }
        try {y_coord = Integer.parseInt((String) parameters.get("y"));}
        catch (NumberFormatException e) {
            String errorResponse = "y misformated or not given";
            exchange.sendResponseHeaders(400, errorResponse.length());
            os.write(errorResponse.getBytes());
            return false;
        }
        try {
            String iistr = (String) parameters.get("insertIndex");
            if (iistr != null) {
                insertIndex = Integer.parseInt(iistr);
            }
        } catch (NumberFormatException e) {
            String errorResponse = "insertIndex misformated";
            exchange.sendResponseHeaders(400, errorResponse.length());
            os.write(errorResponse.getBytes());
            return false;
        }


        int[] arguments;
        if (insertIndex == null) {
            arguments = new int[2];

            arguments[0] = x_coord;
            arguments[1] = y_coord;
        } else {
            arguments = new int[3];

            arguments[0] = x_coord;
            arguments[1] = y_coord;
            arguments[2] = insertIndex;
        }

        Gson gson=new Gson();

        String JsonResponse = gson.toJson(setWaypoint(arguments));
        
        exchange.sendResponseHeaders(200, JsonResponse.length());
        os.write(JsonResponse.getBytes());
        return true;
    }


    private String setWaypoint(int[] args) {

        if (!(args.length == 2 || args.length == 3)) {
            return "setwp takes 2 or 3 arguments: x y (insertIndex)";
        }

        int x = args[0];
        int y = args[1];

        if (vehicle.getStatus() == MyGame.STATUS_VEHICLE_PENDING) {
            // veh.setStatus(MyGame.STATUS_VEHICLE_MOVING); //this doesn't work, so i'll just make it fail safely for now
            //return "Cannot set waypoint of a vehicle that is pending to engage";
        	
        	System.out.println("Setting waypoint for vehicle that has pending engagement, switching to VehicleMoving...");
        	vehicle.setStatus(MyGame.STATUS_VEHICLE_MOVING);
        }

        if (x < MySize.width && y < MySize.height && x > 0 && y > 0) {

            if (args.length == 2) {
                
                if(vehicle.getPath().isEmpty()){
                    vehicle.addWaypoint(x, y, 0);
                }
                else{
                    vehicle.addWaypoint(x, y);
                }
            } else {// that is- if(args.length==3)
                int insertionIndex = args[2];

                if (insertionIndex <= vehicle.getPathSize()) {
                    vehicle.addWaypoint(x, y, insertionIndex);
                } else {
                    return "Set failed, waypoint list insert index greater than waypoint list size";
                }
            }

            return "Waypoint set- vehicle index: " + vehicle.getIndex() + ", position (" + x + ", " + y + ")";
            // the vehicle.getIndex() method actually returns the numerical
            // designation of the vehicle from the GUI, not the actual index of
            // the vehicle in the vehicle list
        } else {
            return "Coordinates not in bounds";
        }
    }


    private boolean serverDoDeleteDelWP(HttpExchange exchange, int pathIndex, OutputStream os) throws IOException{
        //int x=vehicle.getPath().get(pathIndex)[0];
        //int y=vehicle.getPath().get(pathIndex)[1];
        
        //boolean suc = vehicle.delWaypoint(x, y);
        vehicle.removePathAt(pathIndex);
        String response="Waypoint successfully deleted";
        /*if (suc) {
            response= "Waypoint successfully deleted";
        } else {
            response= "Deletion failed, waypoint does not exist, this should not happen";//(x,y) is a waypoint that already exist, because in order to get here, it has to have been put there
        }*/

        exchange.sendResponseHeaders(200, response.length());
        os.write(response.getBytes());
        return true;


    }

    /*private String deleteWaypoint(int[] args) {

        if (args.length != 2) {
            return "arguments formatting error in deleteWaypoint)\n";
        }

        //int vehIndex = args[0];
        int x = args[0];
        int y = args[1];

        if (!(vehIndex < game.getVehicleList().size() && vehIndex >= 0)) {
            return "Vehicle index out of bounds\n";
        }
        
        //Vehicle veh = game.getVehicleList().getVehiclesAssigned(vehIndex);

        if (x < MySize.MAP_WIDTH_PXL && y < MySize.MAP_HEIGHT_PXL && x > 0  && y > 0) {

            boolean suc = vehicle.delWaypoint(x, y);
            if (suc) {
                return "Waypoint successfully deleted";
            } else {
                return "Deletion failed, waypoint does not exist, this should not happen";//(x,y) is a waypoint that already exist, because in order to get here, it has to have been put there
            }

        } else {
            return "Coordinates not in bounds, this should not happen";
        }
    }*/
    
    
}


class PathSerializer implements JsonSerializer<LinkedList<int []>> {

    @Override
    public JsonElement serialize(LinkedList<int []> src, Type typeOfSrc, JsonSerializationContext context) {

        Gson gson=new Gson();
        
        LinkedList<PositionObject> path=new LinkedList<PositionObject>();

        for(int i=0;i<src.size();i++){
            path.add(i, new PositionObject(src.get(i)));
        }
        
        
        return gson.toJsonTree(path);
    }
}