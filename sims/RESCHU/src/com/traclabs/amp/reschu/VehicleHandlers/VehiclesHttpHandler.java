package com.traclabs.amp.reschu.VehicleHandlers;

import java.io.IOException;
import java.io.OutputStream;

import reschu.game.controller.Reschu;
import reschu.game.model.VehicleList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import com.traclabs.amp.reschu.ReschuHttpHandler;

/**
 * File created by TRACLabs
 * User: oliverl3
 * Date: 6/7/13
 */
public class VehiclesHttpHandler implements HttpHandler {

    private VehicleList vehicleList;

    //the way it is now, this will break if a vehicle gets added.  
    // it will also break if a vehicle gets destroyed and gets removed from the list, since it relies on list indexes
    /**
     * This constructor creates the following contexts:
     * <ul>
     * <li>/Reschu/vehicles/n (Where 'n' represents the index of the individual Vehicle.)</li>
     * <li>/Reschu/vehicles/numberof</li>
     * </ul>
     * 
     * @param reschu - The <code>Reschu</code> instance associated with this instance.
     * @param server - The <code>HttpServer</code> instance associated with this instance.
     */
    public VehiclesHttpHandler(Reschu reschu, HttpServer server){

        
        vehicleList= reschu.game.getVehicleList();
        
        for(int i=0;i<vehicleList.size();i++){

            server.createContext("/Reschu/vehicles/"+i, new IndividualVehicleHttpHandler(reschu, server, i));
        }

        server.createContext("/Reschu/vehicles/numberof", new NumberOfVehiclesHttpHandler(vehicleList));
        
    }

    public void handle(HttpExchange exchange) throws IOException {


        if(exchange.getRequestMethod().equals("GET")){
            if (ReschuHttpHandler.checkInvalidPath(exchange)) return;

            OutputStream os = exchange.getResponseBody();

            GsonBuilder gsonBuilder=new GsonBuilder();
            gsonBuilder.registerTypeAdapter(VehicleList.class, new VehicleListSerializer());

            Gson gson=gsonBuilder.create();
            String JsonResponse=gson.toJson(vehicleList);

            exchange.sendResponseHeaders(200, JsonResponse.length());
            os.write(JsonResponse.getBytes());

            os.close();
        }/*
        else if(exchange.getRequestMethod().equals("POST")){
            
            Map<String, Object> parameters = ReschuHttpHandler.parsePostParameters(exchange);
            
            
            
        }*/
        else{

            OutputStream os = exchange.getResponseBody();
            String methNotSup="method not supported";
            exchange.sendResponseHeaders(400, methNotSup.length());
            os.write(methNotSup.getBytes());
            os.close();
        }
    
    }

}


