package com.traclabs.amp.reschu;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import reschu.game.model.Game;

import java.io.IOException;
import java.io.OutputStream;

/**
 * File created by TRACLabs
 * User: oliverl3
 * Date: 6/7/13
 */
public class TimeInReschuHttpHandler implements HttpHandler {

    private Game game;

    public TimeInReschuHttpHandler(Game g) {
        game=g;
    }
    
    //prints the amount of time since the start of the simulation (i think, at least some sort of time)
    @Override
    public void handle(HttpExchange exchange) throws IOException {

        if (ReschuHttpHandler.checkInvalidPath(exchange)) return;

        OutputStream os = exchange.getResponseBody();
        
        int elapsedTime=game.getElapsedTime();
        String result=String.valueOf(elapsedTime)+"\n";
        
        //System.out.println("Elapsed time: "+result);
        
        exchange.sendResponseHeaders(200, result.length());
        os.write(result.getBytes());

        os.close();
    }
}
