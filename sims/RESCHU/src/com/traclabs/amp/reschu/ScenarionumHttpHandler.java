package com.traclabs.amp.reschu;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import reschu.game.model.Game;

import java.io.IOException;
import java.io.OutputStream;

/**
 * File created by TRACLabs
 * User: oliverl3
 * Date: 6/7/13
 */
public class ScenarionumHttpHandler implements HttpHandler {
    
    Game game;
    
    public ScenarionumHttpHandler(Game g) {
        game =g;
    }

    //prints the gametype and scenario of the current simulation
    @Override
    public void handle(HttpExchange exchange) throws IOException {

        if (ReschuHttpHandler.checkInvalidPath(exchange)) return;

        OutputStream os = exchange.getResponseBody();

        int scenarioNum=game.getScenario();
        //String result="Scenario Number "+String.valueOf(scenarioNum)+"\n";

        Gson gson=new Gson();
        String JsonResponse=gson.toJson(scenarioNum);


        exchange.sendResponseHeaders(200, JsonResponse.length());
        os.write(JsonResponse.getBytes());

        os.close();


    }
}
