package com.traclabs.amp.reschu;

import com.google.gson.*;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import reschu.game.model.Game;
import reschu.game.model.Target;
import reschu.game.model.Vehicle;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

/**
 * File created by TRACLabs User: oliverl3 Date: 6/11/13
 */
public class TargetHttpHandler implements HttpHandler {
    


	// something to note, the target of the vehicle only gets assigned after the
	// simulation is up and running
	// and it also changes all the time

	private Game game;
	private Target target;

	// this handler will be created from two different contexts,
	// one from IndividualVehicleHttpHandler since each vehicle has a target
	// the other from probably MapHttpHandler or something, since the reschuMap
	// has a list of all targets in existence
	// of course, each context creates it's own handler, but handlers from both
	// contexts use the same code
	public TargetHttpHandler(Game g, Target t, HttpServer server, String context) {
		game = g;
		target = t;

		server.createContext(context + "/name", new TargetNAMEHttpHandler(
				target));
		server.createContext(context + "/intendedpayload",
				new TargetINTEDEDPAYLOADHttpHandler(target));
		server.createContext(context + "/terrain",
				new TargetTERRAINTYPEHttpHandler(target));
		server.createContext(context + "/pos", new TargetPOSITIONHttpHandler(
				target));

		server.createContext(context + "/done", new TargetDONEHttpHandler(
				target));
		server.createContext(context + "/destroyed",
				new TargetDESTROYEDHttpHandler(target));
		server.createContext(context + "/image", new TargetIMAGEHttpHandler(
				target));

	}

	@Override
	public void handle(HttpExchange exchange) throws IOException {

		// /Reschu/vehicles/1/target
		// /Reschu/map/assignedtargets/A

		if (ReschuHttpHandler.checkInvalidPath(exchange))
			return;

		System.out.println("In target http handler");

		if (exchange.getRequestMethod().equals("GET")) {

			OutputStream os = exchange.getResponseBody();

			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.registerTypeAdapter(Target.class,
					new TargetSerializer());
			Gson gson = gsonBuilder.create();
			String JsonResponse = gson.toJson(target);

			exchange.sendResponseHeaders(200, JsonResponse.length());
			os.write(JsonResponse.getBytes());

			os.close();

		} else if (exchange.getRequestMethod().equals("POST")) {

			String contextStr = exchange.getHttpContext().getPath();
			String[] contextPath = contextStr.split("/");

			OutputStream os = exchange.getResponseBody();

			if (!contextPath[2].equals("vehicles")) {

				String wrongContextResponse = "<h1>posting in wrong context</h1>";
				exchange.sendResponseHeaders(400, wrongContextResponse.length());
				os.write(wrongContextResponse.getBytes());

				os.close();
			}

			Map<String, Object> parameters = ReschuHttpHandler
					.parsePostParameters(exchange);

			if (!parameters.containsKey("target")) {

				String noParamResponse = "<h1>parameter absent or misformated</h1>";
				exchange.sendResponseHeaders(400, noParamResponse.length());
				os.write(noParamResponse.getBytes());
				os.close();

				return;

			}

			String targetName = (String) parameters.get("target");

			if (targetName == null) {
				String noParamResponse = "<h1>No target name was specified</h1>";
				exchange.sendResponseHeaders(400, noParamResponse.length());
				os.write(noParamResponse.getBytes());
				os.close();

				return;
			}

			Target newTarget = null;

			boolean found = false;

			for (int i = 0; i < game.reschuMap.getTargetList().size(); i++) {

				if (targetName.equals(game.reschuMap.getTargetList().get(i)
						.getName())) {

					found = true;
					newTarget = game.reschuMap.getTargetByName(targetName);
					break;
				}
			}

			if (!found) {
				String targetDNEResponse = "<h1>target specified does not exist</h1>";
				exchange.sendResponseHeaders(400, targetDNEResponse.length());
				os.write(targetDNEResponse.getBytes());
				os.close();
				return;
			}

			Vehicle vehicle = game.getVehicleList().getVehicle(
					Integer.parseInt(contextPath[3]));

			if (vehicle.isGrounded()) {
				String vehicleIsGroundedResponse = "<h1>vehicle is grounded, cannot be assigned a target</h1>";
				exchange.sendResponseHeaders(400,
						vehicleIsGroundedResponse.length());
				os.write(vehicleIsGroundedResponse.getBytes());
				os.close();
				return;
			}

			int targetX = newTarget.getX();
			// game.reschuMap.getTargetList().get(index).getPos()[0];
			int targetY = newTarget.getY();
			// game.reschuMap.getTargetList().get(index).getPos()[1];
			int targetZ = newTarget.getZ();
			// game.reschuMap.getTargetList().get(index).getPos()[2];

			if (vehicle.isAssignededTarget(targetX, targetY)) {
				String alreadyAssignedResponse = "<h1>Vehicle already assigned to this target</h1>";
				exchange.sendResponseHeaders(400,
						alreadyAssignedResponse.length());
				os.write(alreadyAssignedResponse.getBytes());
				os.close();
				return;
			} else if (vehicle.getPath().size() == 0) {
				// check to make sure clicked pos is actually a goal
				vehicle.addGoal(targetX, targetY, targetZ);
				vehicle.setTarget(newTarget);
			} else {
				vehicle.changeGoal(vehicle.getPath().getLast(), targetX,
						targetY, targetZ);
				vehicle.setTarget(newTarget);
			}

			String successResponse = "Vehicle " + vehicle.getIndex()
					+ "'s target set to " + targetName;
			exchange.sendResponseHeaders(200, successResponse.length());
			os.write(successResponse.getBytes());

			os.close();
		} else if (exchange.getRequestMethod().equals("DELETE")) {

			String contextStr = exchange.getHttpContext().getPath();
			String[] contextPath = contextStr.split("/");

			OutputStream os = exchange.getResponseBody();

			if (contextPath[2].equals("vehicles")) {

				Vehicle vehicle = game.getVehicleList().getVehicle(
						Integer.parseInt(contextPath[3]));

				if (vehicle.getTarget() == null) {

					String noTargetResponse = "<h1>vehicle has no designated target to delete</h1>";
					exchange.sendResponseHeaders(400, noTargetResponse.length());
					os.write(noTargetResponse.getBytes());

					os.close();
					return;
				}

				vehicle.nullifyOwnGoal();

				String successResponse = "Vehicle " + vehicle.getIndex()
						+ "'s target nullified";
				exchange.sendResponseHeaders(200, successResponse.length());
				os.write(successResponse.getBytes());

				os.close();
			} else if (contextPath[2].equals("map")
					&& contextPath[3].equals("targets")) {

				Target target;
				try {
					target = game.reschuMap.getTargetByName(contextPath[4]);

					//target.setDone();
					game.reschuMap.removeTarget(target.getName());
					
					String successResponse = "Target " + target.getName()
							+ " has been removed";
					exchange.sendResponseHeaders(200, successResponse.length());
					os.write(successResponse.getBytes());

					os.close();
				} catch (Exception e) {

					String nameDNEResponse = "<h1>no unassigned target of specified name exists</h1>";
					exchange.sendResponseHeaders(400, nameDNEResponse.length());
					os.write(nameDNEResponse.getBytes());

					os.close();
					return;

				}

			} else {

				String wrongContextResponse = "<h1>deleting in wrong context</h1>";
				exchange.sendResponseHeaders(400, wrongContextResponse.length());
				os.write(wrongContextResponse.getBytes());

				os.close();

			}

		} else {

			OutputStream os = exchange.getResponseBody();
			String methNotSup = "method not supported";
			exchange.sendResponseHeaders(400, methNotSup.length());
			os.write(methNotSup.getBytes());
			os.close();
		}
	}
}
