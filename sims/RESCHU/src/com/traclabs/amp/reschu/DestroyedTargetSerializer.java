package com.traclabs.amp.reschu;

import java.lang.reflect.Type;

import reschu.game.model.DestroyedTarget;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class DestroyedTargetSerializer implements JsonSerializer<DestroyedTarget> {

    @Override
    public JsonElement serialize(DestroyedTarget src, Type typeOfSrc, JsonSerializationContext context) {

        JsonObject targetJson=new JsonObject();
        Gson gson=new Gson();

        targetJson.addProperty("name",src.getName());
        targetJson.addProperty("availablePayloads",src.getAvailablePayloads().toString());
        targetJson.addProperty("terrainType",src.getMedium().toString());
        targetJson.add("pos",gson.toJsonTree(new PositionObject(src.getPos())));
        targetJson.addProperty("done",src.isDone());
        targetJson.addProperty("vehicleThatDestroyedMe", src.getVehicleThatDestroyedMe());
        targetJson.addProperty("targetThatReplacedMe", src.getObsTargetThatReplacedMe());
        return targetJson;
    }
}
