package com.traclabs.amp.reschu.MapHandlers;

import com.sun.net.httpserver.HttpServer;

import reschu.game.model.Game;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.LinkedList;

/**
 * File created by TRACLabs
 * User: oliverl3
 * Date: 6/19/13
 */
public class HazardsObserver implements PropertyChangeListener{
    
    HttpServer server;
    Game game;
    int numContext;
    
    
    public HazardsObserver(HttpServer s, Game g, int n){
        
        server=s;
        game=g;

        numContext=n;
        
        game.reschuMap.addHazardsObserver(this);
    }
    
    
    
    


    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        
        //strip all the contexts off /Reschu/map/hazards/
        
        for(int i=0;i< numContext;i++){
            
            server.removeContext("/Reschu/map/hazards/"+i);
            
        }
        

        @SuppressWarnings("unchecked")
		LinkedList<int[]> listHazards=(LinkedList<int[]>)propertyChangeEvent.getNewValue();
        
        //add all the contexts to /Reschu/map/hazards/ from the new list
        
        numContext=listHazards.size();
        
        for(int i=0;i<numContext;i++){
            
            server.createContext("/Reschu/map/hazards/"+i, new IndividualHazardHttpHandler(game, server,i));

        }
        
    }
        
        
    
}


