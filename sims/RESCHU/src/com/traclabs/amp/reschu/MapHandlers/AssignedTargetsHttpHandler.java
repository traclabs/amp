package com.traclabs.amp.reschu.MapHandlers;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;

import reschu.game.model.Game;
import reschu.game.model.Target;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import com.traclabs.amp.reschu.TargetSerializer;

/**
 * File created by TRACLabs
 * User: oliverl3
 * Date: 6/12/13
 */
public class AssignedTargetsHttpHandler implements HttpHandler {

    @SuppressWarnings("unused")
	private HttpServer server;

   ArrayList<Target> listAssignedTargets;



    public AssignedTargetsHttpHandler(Game game, HttpServer s) {

        server = s;

        listAssignedTargets=game.reschuMap.getTargetList();
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        
        // /Reschu/map/assignedtargets


        String requestStr=exchange.getRequestURI().getPath();
        String[] requestPath=requestStr.split("/");



        String contextStr=exchange.getHttpContext().getPath();
        String[] contextPath=contextStr.split("/");


        OutputStream os = exchange.getResponseBody();


        if(requestPath.length==contextPath.length){
            
            GsonBuilder gsonBuilder=new GsonBuilder();

            gsonBuilder.registerTypeAdapter(ArrayList.class, new TargetsInListSerializer("listAssignedTargets"));

            Gson gson=gsonBuilder.create();

            String JsonResponse=gson.toJson(listAssignedTargets);

            exchange.sendResponseHeaders(200, JsonResponse.length());
            os.write(JsonResponse.getBytes());

        }
        else if(requestPath[4].equals("numberofassignedtargets")){

            Gson gson=new Gson();

            String JsonResponse=gson.toJson(listAssignedTargets.size());
            exchange.sendResponseHeaders(200, JsonResponse.length());
            os.write(JsonResponse.getBytes());

        }
        else{

            String notRecogResponse="<h1>requestPath not recognized: assigned targets</h1>";
            exchange.sendResponseHeaders(400, notRecogResponse.length());
            os.write(notRecogResponse.getBytes());
        }

        os.close();
        
        
    }
}

class TargetsInListSerializer implements JsonSerializer<ArrayList<Target>> {
    
    String fieldName;

    @Override
    public JsonElement serialize(ArrayList<Target> src, Type typeOfSrc, JsonSerializationContext context) {

        JsonObject listJson=new JsonObject();
        
        Gson gson=new Gson();
        
        ArrayList<JsonElement> list=new ArrayList<JsonElement>();

        GsonBuilder targetGsonBuilder=new GsonBuilder();
        targetGsonBuilder.registerTypeAdapter(Target.class,new TargetSerializer());
        Gson targetGson=targetGsonBuilder.create();
        
        for(int i=0;i<src.size();i++){

            list.add(i, targetGson.toJsonTree(src.get(i)));

        }
        
        
        
        
        listJson.add(fieldName,gson.toJsonTree(list));



        
        
        return listJson;
    }
    
    
    public TargetsInListSerializer(String name){
        
        fieldName=name;
        
        
    }
    
}