package com.traclabs.amp.reschu.MapHandlers;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import reschu.game.model.Game;

import java.io.IOException;
import java.io.OutputStream;

/**
 * File created by TRACLabs
 * User: oliverl3
 * Date: 6/13/13
 */
public class MapArrayHttpHandler implements HttpHandler {

    int [][] map_array;

    public MapArrayHttpHandler(Game game) {

        map_array= game.reschuMap.getMap_array();
        
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {

        // /Reschu/reschuMap/maparray...
        
        // /Reschu/reschuMap/maparray/30,40


        
        String requestStr=exchange.getRequestURI().getPath();
        String[] requestPath=requestStr.split("/");


        String contextStr=exchange.getHttpContext().getPath();
        String[] contextPath=contextStr.split("/");


        OutputStream os = exchange.getResponseBody();

        


        if(requestPath.length==contextPath.length){
            Gson gson=new Gson();

            String JsonResponse=gson.toJson(map_array);
            exchange.sendResponseHeaders(200, JsonResponse.length());
            os.write(JsonResponse.getBytes());

        }
        else if(requestPath.length>contextPath.length){


            String coordStr=requestPath[4];
            String[] coord=coordStr.split(",");
            System.out.println("coordStr: " + coordStr);
            
            for(int i=0;i<coord.length;i++){

                System.out.println("coord [" +i+"]: " +coord[i]);
                
                
            }
            
            
            if(Integer.parseInt(coord[0])<map_array.length && Integer.parseInt(coord[1])<map_array[0].length){

                Gson gson=new Gson();

                System.out.println("ReschuMap width: " + map_array.length);
                System.out.println("ReschuMap height: " + map_array[0].length);

                String JsonResponse=gson.toJson(map_array[Integer.parseInt(coord[0])][Integer.parseInt(coord[1])]);
                exchange.sendResponseHeaders(200, JsonResponse.length());
                os.write(JsonResponse.getBytes());
                
            }
            else{

                String notRecogResponse="<h1>requestPath not recognized map array</h1>";
                exchange.sendResponseHeaders(400, notRecogResponse.length());
                os.write(notRecogResponse.getBytes());
            }
            
            
            

            
        }
        else{

            String notRecogResponse="<h1>requestPath not recognized map array</h1>";
            exchange.sendResponseHeaders(400, notRecogResponse.length());
            os.write(notRecogResponse.getBytes());
        }

        os.close();
        
        
        
    }
}
