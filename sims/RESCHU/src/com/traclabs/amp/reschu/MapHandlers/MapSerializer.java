package com.traclabs.amp.reschu.MapHandlers;

import com.google.gson.*;

import reschu.game.model.ReschuMap;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedList;

public class MapSerializer implements JsonSerializer<ReschuMap> {

    @Override
    public JsonElement serialize(ReschuMap src, Type typeOfSrc, JsonSerializationContext context) {

        JsonObject mapJson=new JsonObject();

       GsonBuilder targetsInListGsonBuilder=new GsonBuilder();
        targetsInListGsonBuilder.registerTypeAdapter(ArrayList.class,new TargetsInListSerializer("listAssignedTargets"));
        Gson targetsInListGson=targetsInListGsonBuilder.create();
        
        mapJson.add("targetList",targetsInListGson.toJsonTree(src.getTargetList()));

        GsonBuilder hazardsGsonBuilder=new GsonBuilder();
        hazardsGsonBuilder.registerTypeAdapter(LinkedList.class,new HazardsSerializer());
        Gson hazardsGson=hazardsGsonBuilder.create();
        mapJson.add("hazards",hazardsGson.toJsonTree(src.getListHazard()));
        
 

        return mapJson;
    }
}
