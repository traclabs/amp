package com.traclabs.amp.reschu.MapHandlers;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Map;

import reschu.constants.MyGame;
import reschu.game.model.Game;
import reschu.game.model.Target;
import reschu.game.model.Vehicle;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import com.traclabs.amp.reschu.ReschuHttpHandler;

/**
 * File created by TRACLabs User: oliverl3 Date: 6/12/13
 */
public class TargetsOnMapHttpHandler implements HttpHandler {

	Game game;
	@SuppressWarnings("unused")
	private HttpServer server;

	ArrayList<Target> targetList;

	public TargetsOnMapHttpHandler(Game g, HttpServer s) {

		game = g;
		server = s;
		targetList = game.reschuMap.getTargetList();

	}

	@Override
	public void handle(HttpExchange exchange) throws IOException {
		// /Reschu/map/targets

		if (exchange.getRequestMethod().equals("GET")) {
			String requestStr = exchange.getRequestURI().getPath();
			String[] requestPath = requestStr.split("/");

			String contextStr = exchange.getHttpContext().getPath();
			String[] contextPath = contextStr.split("/");

			OutputStream os = exchange.getResponseBody();

			if (requestPath.length == contextPath.length) {
				GsonBuilder gsonBuilder = new GsonBuilder();
				gsonBuilder.registerTypeAdapter(ArrayList.class,
						new TargetsInListSerializer("targetList"));

				Gson gson = gsonBuilder.create();
				String JsonResponse = gson.toJson(targetList);

				exchange.sendResponseHeaders(200, JsonResponse.length());
				os.write(JsonResponse.getBytes());
			} else if (requestPath[4].equals("numberofunassignedtargets")) {
				Gson gson = new Gson();

				String JsonResponse = gson.toJson(targetList.size());
				exchange.sendResponseHeaders(200, JsonResponse.length());
				os.write(JsonResponse.getBytes());

			} else {
				String notRecogResponse = "<h1>requestPath not recognized targets on map</h1>";
				exchange.sendResponseHeaders(400, notRecogResponse.length());
				os.write(notRecogResponse.getBytes());
			}

			os.close();
		} else if (exchange.getRequestMethod().equals("POST")) {

			OutputStream os = exchange.getResponseBody();

			Map<String, Object> parameters = ReschuHttpHandler
					.parsePostParameters(exchange);

			int x;
			int y;

			try {
				x = Integer.parseInt((String) parameters.get("x"));
			} catch (NumberFormatException e) {
				String errorResponse = "x misformated or not given";
				exchange.sendResponseHeaders(400, errorResponse.length());
				os.write(errorResponse.getBytes());
				return;
			}
			try {
				y = Integer.parseInt((String) parameters.get("y"));
			} catch (NumberFormatException e) {
				String errorResponse = "y misformated or not given";
				exchange.sendResponseHeaders(400, errorResponse.length());
				os.write(errorResponse.getBytes());
				return;
			}

			String intendedPayloadstr = (String) parameters
					.get("intendedpayload");

			Vehicle.Payload intendedPayload;

			try {
				intendedPayload = Vehicle.stringToPayload(intendedPayloadstr,
						true);

			} catch (Exception e) {

				String intendedPayloadUnrecognized = "<h1>intendedpayload argument not valid</h1>";
				exchange.sendResponseHeaders(400,
						intendedPayloadUnrecognized.length());
				os.write(intendedPayloadUnrecognized.getBytes());

				os.close();
				return;
			}
			try {
				if (game.reschuMap.getMap_array()[x][y] == MyGame.SEA) {
					if (intendedPayload == Vehicle.Payload.INSPECT_CAM
							|| intendedPayload == Vehicle.Payload.WEAPONS) {

						String targetTerrainWrong = "<h1>inspectcam and weapons targets can't be in water, only on land or shore</h1>";
						exchange.sendResponseHeaders(400,
								targetTerrainWrong.length());
						os.write(targetTerrainWrong.getBytes());

						os.close();
						return;
					}
				}
			} catch (ArrayIndexOutOfBoundsException ex) {
				String targetTerrainWrong = "<h1>Coordinates not in bounds.</h1>";
				exchange.sendResponseHeaders(400, targetTerrainWrong.length());
				os.write(targetTerrainWrong.getBytes());

				os.close();
				return;
			}

			try {
				String targetName;
				targetName = game.getEmptyTargetName();

				game.reschuMap.addTarget(new Target(targetName, targetName,
						intendedPayload, game.reschuMap.chkTargetOffset(x, y),
						game));

				game.setTargetUsed(targetName, true);

				String successResponse = "successfully added target";
				exchange.sendResponseHeaders(200, successResponse.length());
				os.write(successResponse.getBytes());
				os.close();
			} catch (IndexOutOfBoundsException e) {

				// No target names available.

				os = exchange.getResponseBody();
				String methNotSup = "No more Target names available";
				exchange.sendResponseHeaders(400, methNotSup.length());
				os.write(methNotSup.getBytes());
				os.close();

			}

		} else if (exchange.getRequestMethod().equals("DELETE")) {
			// NOTE: DELETE is only used to remove Targets from the Simulation
			// at this time. - ARS (Jan 2015)
			OutputStream os = exchange.getResponseBody();

			String requestStr = exchange.getRequestURI().getPath();
			String[] requestPath = requestStr.split("/");

			String targetName = requestPath[4];
			String successResponse = "";
			if (targetName.length() == 1) {

				if (game.reschuMap.removeTarget(targetName)) {
					game.setTargetUsed(targetName, false);
					successResponse = "Successfully deleted target: "
							+ targetName + " from RESCHU Simulation.";
				} else {
					successResponse = "Delete of Target was unsuccessful.  Target: "
							+ targetName + " was not found.";
				}
			} else {
				successResponse = "Target name was not a letter!";

			}

			exchange.sendResponseHeaders(200, successResponse.length());
			os.write(successResponse.getBytes());
			os.close();

		} else {

			OutputStream os = exchange.getResponseBody();
			String methNotSup = "Unsupported Request Method: "
					+ exchange.getRequestMethod();
			exchange.sendResponseHeaders(400, methNotSup.length());
			os.write(methNotSup.getBytes());
			os.close();
		}

	}
}
