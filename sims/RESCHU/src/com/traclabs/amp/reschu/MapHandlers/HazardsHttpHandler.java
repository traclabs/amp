package com.traclabs.amp.reschu.MapHandlers;

import com.google.gson.*;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import com.traclabs.amp.reschu.PositionObject;
import reschu.game.model.Game;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.util.LinkedList;

/**
 * File created by TRACLabs
 * User: oliverl3
 * Date: 6/12/13
 */
public class HazardsHttpHandler implements HttpHandler {

    private Game game;
	private HttpServer server;
    


    public HazardsHttpHandler(Game g, HttpServer s) {

        game = g;
        server = s;
        
        int numContext=0;
        for(int i=0;i<game.reschuMap.getListHazard().size();i++){

            server.createContext("/Reschu/map/hazards/"+i, new IndividualHazardHttpHandler(game, server,i));

            numContext++;
        }

        new HazardsObserver(server,game,numContext);
        

    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {

        // /Reschu/reschuMap/assignedtargets

        //if (ReschuHttpHandler.checkInvalidPath(exchange)) return;



        String requestStr=exchange.getRequestURI().getPath();
        String[] requestPath=requestStr.split("/");



        String contextStr=exchange.getHttpContext().getPath();
        String[] contextPath=contextStr.split("/");


        OutputStream os = exchange.getResponseBody();


        if(requestPath.length==contextPath.length){
            
            GsonBuilder gsonBuilder=new GsonBuilder();
            
            gsonBuilder.registerTypeAdapter(LinkedList.class,new HazardsSerializer());
            
            Gson gson=gsonBuilder.create();

            String JsonResponse=gson.toJson(game.reschuMap.getListHazard());

            exchange.sendResponseHeaders(200, JsonResponse.length());
            os.write(JsonResponse.getBytes());

        }
        else if(requestPath[4].equals("numberofhazards")){

            Gson gson=new Gson();

            String JsonResponse=gson.toJson(game.reschuMap.getListHazard().size());
            exchange.sendResponseHeaders(200, JsonResponse.length());
            os.write(JsonResponse.getBytes());

        }
        /*else if(requestPath.length>contextPath.length && Integer.parseInt(requestPath[4])<game.reschuMap.getListHazard().size() ){
        //hazards must be gotten with their index number, which can't be known without a call to the above if statement, and then counting the position objects
        // hazards areas are also always changing, so the hazard your looking for isn't even guaranteed to still be there by the time you call this one, there's a change that the index will have changed

            Gson gson=new Gson();

            String JsonResponse=gson.toJson(new PositionObject(game.reschuMap.getListHazard().get(Integer.parseInt(requestPath[4]))));


            exchange.sendResponseHeaders(200, JsonResponse.length());
            os.write(JsonResponse.getBytes());
        }*/
        else{

            String notRecogResponse="<h1>requestPath not recognized</h1>";
            exchange.sendResponseHeaders(400, notRecogResponse.length());
            os.write(notRecogResponse.getBytes());
        }

        os.close();


    }
}

class HazardsSerializer implements JsonSerializer<LinkedList<int []>> {

    @Override
    public JsonElement serialize(LinkedList<int []> src, Type typeOfSrc, JsonSerializationContext context) {

        JsonObject hazardsJson=new JsonObject();
        Gson gson=new Gson();

        LinkedList<PositionObject> hazards=new LinkedList<PositionObject>();

        for(int i=0;i<src.size();i++){

            hazards.add(i, new PositionObject(src.get(i)));

        }

        hazardsJson.add("hazards",gson.toJsonTree(hazards));

        return hazardsJson;
    }
}