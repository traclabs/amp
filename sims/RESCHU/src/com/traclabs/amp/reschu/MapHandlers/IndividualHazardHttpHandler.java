package com.traclabs.amp.reschu.MapHandlers;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import com.traclabs.amp.reschu.PositionObject;

import reschu.game.model.Game;

import java.io.IOException;
import java.io.OutputStream;

/**
 * File created by TRACLabs
 * User: oliverl3
 * Date: 6/20/13
 */
public class IndividualHazardHttpHandler implements HttpHandler {


    private Game game;
    @SuppressWarnings("unused")
	private HttpServer server;
    private int index;
    
    private int [] hazard;

    public IndividualHazardHttpHandler(Game g, HttpServer s, int i) {
        game = g;
        server = s;
        
        index=i;
        
        hazard=game.reschuMap.getListHazard().get(index);
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {

        OutputStream os = exchange.getResponseBody();
        
        Gson gson=new Gson();

        String JsonResponse=gson.toJson(new PositionObject(hazard[0],hazard[1]));


        exchange.sendResponseHeaders(200, JsonResponse.length());
        os.write(JsonResponse.getBytes());

        os.close();
        
    }
}
