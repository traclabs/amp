package com.traclabs.amp.reschu;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import reschu.game.model.Target;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;


public class TargetIMAGEHttpHandler implements HttpHandler {

	Target target;

	public TargetIMAGEHttpHandler(Target t) {
		target = t;
	}

	@Override
	public void handle(HttpExchange exchange) throws IOException {

		if (ReschuHttpHandler.checkInvalidPath(exchange))
			return;

		
		try {
			
		      // add the required response header for a PDF file
		      Headers h = exchange.getResponseHeaders();
		      h.add("Content-Type", "image/jpg");
		      
		      String imageFilePath = Target.class.getClassLoader().getResource(target.getImageFileName()).getPath();
		      
		      File file = new File (imageFilePath);
		      byte [] byteArray  = new byte [(int)file.length()];
		      FileInputStream fis = new FileInputStream(file);
		      
		      
		      BufferedInputStream bis = new BufferedInputStream(fis);
		      bis.read(byteArray, 0, byteArray.length);

		      // ok, we are ready to send the response.
		      exchange.sendResponseHeaders(200, file.length());
		     
		      OutputStream os = exchange.getResponseBody();
		     
		      
		      os.write(byteArray,0,byteArray.length);
		      os.close();
		      
		      bis.close();


		} catch (IOException e) {
			System.out.println("Target " + target.getName()
					+ "'s image " + target.getImageFileName() + " could not load. Exception: " + e.getLocalizedMessage());

		}

	}
}
