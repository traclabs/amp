package com.traclabs.amp.reschu;

import java.lang.reflect.Type;

import reschu.game.model.Target;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class TargetSerializer implements JsonSerializer<Target> {

	@Override
	public JsonElement serialize(Target src, Type typeOfSrc,
			JsonSerializationContext context) {

		JsonObject targetJson = new JsonObject();
		Gson gson = new Gson();

		targetJson.addProperty("name", src.getDescriptiveName()); 
		targetJson.addProperty("availablePayloads", src.getAvailablePayloads()
				.toString());
		targetJson.addProperty("terrainType", src.getMedium().toString());
		targetJson
				.add("pos", gson.toJsonTree(new PositionObject(src.getPos())));
		targetJson.addProperty("done", src.isDone());
		targetJson.addProperty("destroyed", src.isDestroyed());

		return targetJson;
	}
}
