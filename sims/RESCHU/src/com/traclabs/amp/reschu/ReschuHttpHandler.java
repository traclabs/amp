package com.traclabs.amp.reschu;

import com.google.gson.*;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import com.traclabs.amp.reschu.MapHandlers.MapHttpHandler;
import com.traclabs.amp.reschu.MapHandlers.MapSerializer;
import com.traclabs.amp.reschu.VehicleHandlers.VehicleListSerializer;
import com.traclabs.amp.reschu.VehicleHandlers.VehiclesHttpHandler;
import reschu.game.controller.Reschu;
import reschu.game.model.*;
import reschu.game.model.ReschuMap;

import java.io.*;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URLDecoder;
import java.util.*;


/**
 * File created by TRACLabs
 * User: oliverl3
 * Date: 6/7/13
 */
public class ReschuHttpHandler implements HttpHandler {

    private Reschu reschu;
    private Game game;



    private HttpServer server;

    /**
     * This constructor creates the following contexts:
     * <ul>
     * <li>/Reschu/scenario</li>
     * <li>/Reschu/time</li>
     * <li>/Reschu/vehicles</li>
     * <li>/Reschu/map</li>
     * </ul>
     * @param r - The <code>Reschu</code> instance associated with this instance.
     * @param s - The <code>HttpServer</code> instance associated with this instance.
     */
    public ReschuHttpHandler(Reschu r, HttpServer s){

        reschu=r;
        game= reschu.game;
        
        server=s;
        

        //these guys need to be passed the server object because the other handlers will neeed to create their own sub handlers
        // unless it's a "leaf" field, in which case it will not be creating any more sub handlers or sub contexts

        
        server.createContext("/Reschu/scenario", new ScenarionumHttpHandler(game));//no one really cares about this honestly
        server.createContext("/Reschu/time", new TimeInReschuHttpHandler(game));


        server.createContext("/Reschu/vehicles", new VehiclesHttpHandler(reschu, server));

        server.createContext("/Reschu/map", new MapHttpHandler(game, server));

    }

    //this method only handles "/Reschu".  
    // if there is anything after that, then that should go to other handlers
    // however, invalid additional paths will still come here, so those need to be handled.
    public void handle(HttpExchange exchange) throws IOException {


        if (checkInvalidPath(exchange)) return;

        if(exchange.getRequestMethod().equals("GET")){
            OutputStream os = exchange.getResponseBody();

            GsonBuilder gsonBuilder=new GsonBuilder();

            gsonBuilder.registerTypeAdapter(game.getClass(), new ReschuSerializer());


            Gson gson=gsonBuilder.create();
            String JsonResponse=gson.toJson(game);

            exchange.sendResponseHeaders(200, JsonResponse.length());
            os.write(JsonResponse.getBytes());

            os.close();
        }
        else{

            OutputStream os = exchange.getResponseBody();

            Gson gson=new Gson();
            String JsonResponse=gson.toJson("invalid method, no can do");


            exchange.sendResponseHeaders(405, JsonResponse.length());
            os.write(JsonResponse.getBytes());
        }
    }


    class ReschuSerializer implements JsonSerializer<Game> {

        @Override
        public JsonElement serialize(Game src, Type typeOfSrc, JsonSerializationContext context) {

            JsonObject reschuJson=new JsonObject();
            
            reschuJson.addProperty("scenarionum", src.getScenario());
            reschuJson.addProperty("elapsedTime",src.getElapsedTime());
            
            GsonBuilder vehicleListBuilder=new GsonBuilder();
            vehicleListBuilder.registerTypeAdapter(VehicleList.class,new VehicleListSerializer());
            
            Gson vehicleListGson=vehicleListBuilder.create();
            
            reschuJson.add("vehicles",vehicleListGson.toJsonTree(src.getVehicleList()));

            GsonBuilder mapBuilder=new GsonBuilder();
            mapBuilder.registerTypeAdapter(ReschuMap.class,new MapSerializer());

            Gson mapGson=mapBuilder.create();
            reschuJson.add("reschuMap",mapGson.toJsonTree(src.reschuMap));
            
            
            return reschuJson;
        }
    }




    public static boolean checkInvalidPath(HttpExchange exchange) throws IOException {
        String requestStr=exchange.getRequestURI().getPath();
        String[] requestPath=requestStr.split("/");
        //for "/Reschu/" requestPath[0] will be a null because of how split works
        //requestPath[1] will be "Reschu" otherwise this handler would not be executed, since we wouldn't be in this context
        //if there is a requestPath[2], then the requestPath is invalid.  any valid paths would have triggered other handlers with 
        // their more specific contexts, so anything in requestPath[2] that comes to here is junk and must be 
        // handled with a 400 or something

        //System.out.println("requestStr: "+requestStr);
        
        

        /*for(int i=0;i<requestPath.length;i++){
            System.out.println("requestPath["+i+"]: "+requestPath[i]);

        }*/

        String contextStr=exchange.getHttpContext().getPath();
        String[] contextPath=contextStr.split("/");
        //System.out.println("contextStr: "+contextStr);

        //handle the invalid requestPath case
        if(requestPath.length>contextPath.length){
            OutputStream os = exchange.getResponseBody();

            String notRecogResponse="<h1>requestPath not recognized RESCHU</h1>";
            exchange.sendResponseHeaders(400, notRecogResponse.length());
            os.write(notRecogResponse.getBytes());

            os.close();
            return true;

        }
        return false;
    }
    
    

    public static Map<String, Object> parseGetParameters(HttpExchange exchange) throws UnsupportedEncodingException {

        Map<String, Object> parameters = new HashMap<String, Object>();
        URI requestedUri = exchange.getRequestURI();
        String query = requestedUri.getRawQuery();
        parseQuery(query, parameters);

        //exchange.setAttribute("parameters", parameters);// I have no idea what setAttribute does

        return parameters;
    }

    public static Map<String, Object> parsePostParameters(HttpExchange exchange) throws IOException {
        InputStreamReader bodyReader = new InputStreamReader(exchange.getRequestBody(),"utf-8");
        BufferedReader bufferedBodyReader = new BufferedReader(bodyReader);
        StringBuilder queryBuilder = new StringBuilder();
        String bodyLine;
//        while (bufferedBodyReader.ready()){
//             bodyLine = bufferedBodyReader.readLine();
//            queryBuilder.append(bodyLine);
//        }
        //FIXME Alternative read method on buffer:
        while ((bodyLine = bufferedBodyReader.readLine()) != null) {
			queryBuilder.append(bodyLine);
		}
        
        

        Map<String, Object> parameters = new HashMap<String, Object>();
        
        System.out.println("query: "+queryBuilder.toString());

        parseQuery(queryBuilder.toString(), parameters);
        
        return parameters;
    }


    @SuppressWarnings("unchecked")
    private static void parseQuery(String query, Map<String, Object> parameters) throws UnsupportedEncodingException {

        if (query != null) {
            String pairs[] = query.split("[&]");

            for (String pair : pairs) {
                String param[] = pair.split("[=]");

                String key = null;
                String value = null;
                if (param.length > 0) {
                    key = URLDecoder.decode(param[0],
                            System.getProperty("file.encoding"));
                }

                if (param.length > 1) {
                    value = URLDecoder.decode(param[1],
                            System.getProperty("file.encoding"));
                }

                if (parameters.containsKey(key)) {
                    Object obj = parameters.get(key);
                    if(obj instanceof List<?>) {
                        List<String> values = (List<String>)obj;
                        values.add(value);
                    } else if(obj instanceof String) {
                        List<String> values = new ArrayList<String>();
                        values.add((String)obj);
                        values.add(value);
                        parameters.put(key, values);
                    }
                } else {
                    parameters.put(key, value);
                }
            }
        }
    }
}