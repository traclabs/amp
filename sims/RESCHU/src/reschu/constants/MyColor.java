package reschu.constants;

import java.awt.Color;

public class MyColor {
	final static public Color COLOR_HIGHLIGHT = new Color(255, 255, 255, 255);
	final static public Color COLOR_VEHICLE = new Color(128, 224, 255, 150);
	final static public Color COLOR_HIGHLIGHT_CONTROL = new Color(128, 224, 255, 255);
	final static public Color COLOR_HIGHLIGHT_TAB = new Color(128, 224, 255, 200);
	final static public Color COLOR_LINE = new Color(128, 224, 255, 150);
	final static public Color COLOR_VEHICLE_TIMELINE = new Color(130, 148, 202, 100);

	final static public Color COLOR_TARGET_DONE = new Color(50, 50, 50, 250);
	//final static public Color COLOR_TARGET_COMM_VACANT = new Color(128, 255, 128, 150);
	final static public Color COLOR_TARGET_COMM_OCCUPIED = new Color(128, 200, 128, 150);

    final static public Color COLOR_TARGET_WEAPONS_VACANT = new Color(255, 23, 34);
    final static public Color COLOR_TARGET_WEAPONS_OCCUPIED = new Color(200, 0, 0);
    final static public Color COLOR_TARGET_INSPECTCAM_VACANT = new Color(226, 0, 255);
    final static public Color COLOR_TARGET_INSPECTCAM_OCCUPIED = new Color(122, 0, 177);
    final static public Color COLOR_TARGET_SRSENSORS_VACANT= new Color(255, 142, 0);
    final static public Color COLOR_TARGET_SRSENSORS_OCCUPIED= new Color(172, 70, 0);
	final static public Color COLOR_TARGET_LRSENSORS_VACANT = new Color(130, 130, 130, 150);
	final static public Color COLOR_TARGET_LRSENSORS_OCCUPIED = new Color(50, 50, 50, 150);
    final static public Color COLOR_TARGET_LAND_BASE = new Color(0, 88, 43);
    final static public Color COLOR_TARGET_SEA_BASE = new Color(0, 6, 88);
    final static public Color COLOR_TARGET_DESTROYED = new Color(174, 174, 174);
    
	final static public Color COLOR_VEHICLE_COMM_BOUNDARY = new Color(50, 250, 50, 50);
	static public Color COLOR_VEHICLE_PENDING = new Color(128, 224, 255, 150);
}
