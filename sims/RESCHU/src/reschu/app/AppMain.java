package reschu.app;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import reschu.constants.MyGameMode;
import reschu.game.controller.Reschu;

import com.traclabs.amp.reschu.ReschuServer;

@SuppressWarnings("unused")
public class AppMain {

	private int _gamemode = MyGameMode.ADMINISTRATOR_MODE;
	private int _scenario=1;
    private boolean autoassign=false;
    
	private JFrame _frmLogin;
	private JButton _btnStart;
	@SuppressWarnings("rawtypes")
	private JComboBox _cmbBoxGameMode, _cmbBoxScenario;
	private JCheckBox checkautoassign;
    
	private Reschu reschu;

	/**
	 * When tutorial is finished, RESCHU automatically restarts in the training
	 * mode.
	 */
	public void Restart_Reschu() {
		if (_gamemode == MyGameMode.TUTORIAL_MODE) {
			_gamemode = MyGameMode.TRAIN_MODE;
		}
		reschu.Game_End();
		reschu.dispose();
		reschu = null;

		initRESCHU(false);
	}

	/**
	 * This should NEVER be called if you are NOT in the tutorial mode.
	 */
	public void TutorialFinished() {
	}

	private void initRESCHU(boolean autoassign) {
		// Setting _scenario again seems counter-intuitive here.
		// Since we are differentiating between administrators and subjects,
		// we need to update the scenario number here again.

		// Create an instance of Reschu (JFrame)
		reschu = new Reschu(_gamemode, _scenario, this, autoassign);
		reschu.pack();
		reschu.setVisible(true);

        //traclabs additions below
        
        ReschuServer RServer=new ReschuServer(reschu);
        Thread serverThr=new Thread(RServer);
        serverThr.start();

        System.out.println("server initialized");
        reschu.Game_Start();
      
    }


	private void setFrmLogin() {
        autoassign=false;//doubt it even works if true
        initRESCHU(autoassign);

	}

	static public void main(String argv[]) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				AppMain app = new AppMain();
				app.setFrmLogin();
			}
		});
	}
}
