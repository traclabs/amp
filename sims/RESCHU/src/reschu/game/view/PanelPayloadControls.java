package reschu.game.view;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import reschu.game.controller.GUI_Listener;

public class PanelPayloadControls extends JPanel implements ActionListener {
	private static final long serialVersionUID = -3327951582205927390L;
	private GUI_Listener listener;
	JButton btnUp, btnDown, btnCW, btnCCW, btnZoomIn, btnZoomOut;
	JButton btnSubmit;
	//private URL urlImgZoomIn, urlImgZoomOut; // ,urlImgUp, urlImgDown, urlImgCW,
												// urlImgCCW;
	double origin_time;

	/** Creates a new instance of PanelPayloadControls */
	public PanelPayloadControls(GUI_Listener e, String strTitle, double o) {
		origin_time = o;
		//String url_prefix = MyURL.URL_PREFIX;

		// try { urlImgUp = new URL(url_prefix+"pan_up.gif");
		// btnUp = new JButton(new ImageIcon(urlImgUp));
		// } catch (MalformedURLException urle) {
		// btnUp = new JButton("UP");}
		// try {
		// urlImgDown = new URL(url_prefix+"pan_down.gif");
		// btnDown = new JButton(new ImageIcon(urlImgDown));
		// } catch (MalformedURLException urle) {
		// btnDown = new JButton("DOWN");}
		// try {
		// urlImgCW = new URL(url_prefix+"pan_right.gif");
		// btnCW = new JButton(new ImageIcon(urlImgCW));
		// } catch (MalformedURLException urle) {
		// btnCW = new JButton("CW");}
		// try {
		// urlImgCCW = new URL(url_prefix+"pan_left.gif");
		// btnCCW = new JButton(new ImageIcon(urlImgCCW));
		// } catch (MalformedURLException urle) {
		// btnCCW = new JButton("CCW");}
        try {

            URL urlzoomin=FrameEnd.class.getClassLoader().getResource("zoom_in.gif");
            btnZoomIn = new JButton(new ImageIcon(urlzoomin));

        } catch (Exception exc) {
            btnZoomIn = new JButton("+");
        }
        try {
            URL urlzoomout=FrameEnd.class.getClassLoader().getResource("zoom_out.gif");
            btnZoomOut = new JButton(new ImageIcon(urlzoomout));
        } catch (Exception exc) {
            btnZoomOut = new JButton("-");
        }

		btnSubmit = new JButton("OK");
		btnSubmit.setEnabled(false);
		/*
		 * setLayout(new GridLayout(6,1)); add(btnUp);
		 * btnUp.addActionListener(this); add(btnDown);
		 * btnDown.addActionListener(this); add(btnCW);
		 * btnCW.addActionListener(this); add(btnCCW);
		 * btnCCW.addActionListener(this); add(btnZoomIn);
		 * btnZoomIn.addActionListener(this); add(btnZoomOut);
		 * btnZoomOut.addActionListener(this);
		 */
		setLayout(new GridLayout(2, 1));
		add(btnZoomIn);
		btnZoomIn.addActionListener(this);
		// add(btnSubmit); btnSubmit.addActionListener(this);
		add(btnZoomOut);
		btnZoomOut.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnUp)
			listener.Pan_Up_Selected();
		if (e.getSource() == btnDown)
			listener.Pan_Down_Selected();
		if (e.getSource() == btnCW)
			listener.Rotate_Clockwise_Selected();
		if (e.getSource() == btnCCW)
			listener.Rotate_Counter_Selected();
		if (e.getSource() == btnZoomIn)
			listener.Zoom_In();
		if (e.getSource() == btnZoomOut)
			listener.Zoom_Out();
		if (e.getSource() == btnSubmit)
			listener.Submit_Payload();
	}

	public void enableSubmit(boolean enable) {
		btnSubmit.setEnabled(enable);
	}

	public void setListener(GUI_Listener l) {
		listener = l;
	}

}
