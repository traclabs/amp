package reschu.game.view;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.net.URL;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.Timer;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import reschu.constants.MyColor;
import reschu.constants.MyFont;
import reschu.constants.MyGame;
import reschu.constants.MySize;
import reschu.constants.MySpeed;
import reschu.game.controller.Reschu;
import reschu.game.model.EngageScenario;
import reschu.game.model.Game;
import reschu.game.model.Vehicle;

public class PanelControl extends JPanel implements ChangeListener,
		ActionListener {
	private static final long serialVersionUID = 6768850931538883107L;
	private JTabbedPane tabbedPane;
	private VehiclePanel[] pnlVehicle;
	private VehicleGeneralPanel pnlGeneral;
	private Reschu lsnr;
	private JScrollPane scrollPane;
	private Game game;
	private Timer tmr;
	private boolean[] colorFlag;
	private boolean eventFromMap;

	public PanelControl(Reschu l, Game g, String strTitle) {
		Vehicle v;

		game = g;
		lsnr = l;
		colorFlag = new boolean[game.getVehicleList().size()];
		tmr = new Timer(MySpeed.SPEED_CLOCK, this);
		tmr.start();
		eventFromMap = false;
		tabbedPane = new JTabbedPane();
		tabbedPane.addChangeListener(this);
		pnlVehicle = new VehiclePanel[game.getVehicleList().size()];

		pnlGeneral = new VehicleGeneralPanel(lsnr, game);
		pnlGeneral.setPreferredSize(new Dimension(getWidth(), 250));
		scrollPane = new JScrollPane(pnlGeneral);
		// scrollPane.remove(scrollPane.getHorizontalScrollBar());

		tabbedPane.addTab("ALL", scrollPane);
		tabbedPane.setMnemonicAt(0, KeyEvent.VK_A);

		for (int i = 0; i < game.getVehicleList().size(); i++) {
			v = game.getVehicleList().getVehicle(i);
			pnlVehicle[i] = new VehiclePanel(lsnr, v);
			tabbedPane.addTab(v.getIndex() + "(" + v.getName() + ")", pnlVehicle[i]);
			tabbedPane.setMnemonicAt(i+1, getVK(v));
		}
		setLayout(new GridLayout(1, 1));
		add(tabbedPane);
	}

	public void setEnabled(boolean enabled) {
		tabbedPane.setEnabled(enabled);
	}

	private int getVK(Vehicle v) {
		switch (v.getIndex()) {
		case 0:
			return KeyEvent.VK_0;
		case 1:
			return KeyEvent.VK_1;
		case 2:
			return KeyEvent.VK_2;
		case 3:
			return KeyEvent.VK_3;
		case 4:
			return KeyEvent.VK_4;
		case 5:
			return KeyEvent.VK_5;
		case 6:
			return KeyEvent.VK_6;
		case 7:
			return KeyEvent.VK_7;
		case 8:
			return KeyEvent.VK_8;
		case 9:
			return KeyEvent.VK_9;
		}
		return 0;
	}

	public void stateChanged(ChangeEvent e) {
		int i = tabbedPane.getSelectedIndex();
		if (i == 0) {
			lsnr.Vehicle_Unselected_From_pnlControl();
			if (!eventFromMap && game.isRunning()) {
				lsnr.EVT_VSelect_Tab_All();
			}
		} else {
			lsnr.Vehicle_Selected_From_pnlControl(i-1);
			if (!eventFromMap) {
				lsnr.EVT_VSelect_Tab(i-1);
			}
		}
		eventFromMap = false;
	}

	public void Show_Vehicle_Status(int vehicleidx) {
		eventFromMap = true;
		tabbedPane.setSelectedIndex(vehicleidx+1);
	}

	public void chkEngageEnabled() {
		pnlGeneral.chkEngageEnabled();
		for (int i = 0; i < pnlVehicle.length; i++)
			pnlVehicle[i].chkEngageEnabled();
	}

	/**
	 * Updates the damage info of this vehicle
	 */
	public void Update_Vehicle_Damage(Vehicle v) {
		pnlGeneral.Update_Damage(v);
		pnlVehicle[v.getIndex() ].Update_Damage();
	}

	/**
	 * Update the mission info of this vehicle to the mission of this payload
	 */
	public void Update_Vehicle_Payload(Vehicle v, EngageScenario p) {
		pnlVehicle[v.getIndex()].Update_Payload(p);
	}

	/**
	 * Clears the mission info of this vehicle
	 */
	public void Update_Vehicle_Payload_Clear(Vehicle v) {
		pnlVehicle[v.getIndex()].Payload_Clear();
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == tmr) {
			for (int i = 0; i < game.getVehicleList().size(); i++) {
                colorFlag[i] = game.getVehicleList().getVehicle(i).getStatus() == MyGame.STATUS_VEHICLE_PENDING && !colorFlag[i];
				// below: i+1 because 0-th tab is for "all"
				if (colorFlag[i])
					tabbedPane.setForegroundAt(i+1,
							MyColor.COLOR_HIGHLIGHT_TAB);
				else
					tabbedPane.setForegroundAt(i+1, Color.BLACK);
			}
		}
	}
}

class VehicleGeneralPanel extends JPanel {
	private static final long serialVersionUID = -8910858184513488565L;
	private VehicleCompactInfo[] infoList;

    public VehicleGeneralPanel(Reschu l, Game g) {
		double size[][] = { { TableLayout.FILL }, { 52, 52, 52, 52, 52 } };
		setLayout(new TableLayout(size));
        infoList = new VehicleCompactInfo[g.getVehicleList().size()];
		for (int i = 0; i < g.getVehicleList().size(); i++) {
			infoList[i] = new VehicleCompactInfo(l, g.getVehicleList()
					.getVehicle(i));
			add(infoList[i], "0," + i);
		}
	}

	public void chkEngageEnabled() {
		for (int i = 0; i < infoList.length; i++)
			infoList[i].chkEngageEnabled();
	}

	public void Update_Damage(Vehicle v) {
		infoList[v.getIndex()].Update_Damage();
	}
}

class VehicleCompactInfo extends JPanel implements ActionListener {
	private static final long serialVersionUID = 3608652661736826513L;
	private GridBagLayout grid_bag_layout = new GridBagLayout();
    private JPanel pnlVehicle, pnlInfo;
	private JLabel lblHealth;
    private JButton btnEngage;
	private Reschu lsnr;
	private Vehicle v;
	private VehicleIcon2 iconV;
    private int intDamage;

	public VehicleCompactInfo(Reschu l, Vehicle v) {
		this.v = v;
		lsnr = l;

        TitledBorder bdrTitle = BorderFactory.createTitledBorder("");
		setBorder(bdrTitle);
		intDamage = (int) Math.round(v.getDamage());

		// FIRST
		pnlVehicle = new JPanel();
		iconV = new VehicleIcon2(v);
		pnlVehicle.setBorder(bdrTitle);
		pnlVehicle.setLayout(new GridLayout(0, 1));
		pnlVehicle.add(iconV);

		// SECOND
		lblHealth = new JLabel("Damage : " + intDamage); // COLOR =
															// lblHealth.setForeground
        String concat;
        if(v.getVehicleClass()== Vehicle.VehicleClass.FIRESCOUT){
            concat="inspect cam";
        }
        else{
            concat="scan sensors";
        }
        JLabel lblTask = new JLabel("Current Task : " + concat);
		pnlInfo = new JPanel();
		pnlInfo.setLayout(new GridLayout(0, 1));
		pnlInfo.setBorder(bdrTitle);
        Color COLOR_BACK = new Color(178, 178, 178, 255);
        pnlInfo.setBackground(COLOR_BACK);
		pnlInfo.add(lblHealth);
		pnlInfo.add(lblTask);

		// THIRD
		btnEngage = new JButton("ENGAGE");
		btnEngage.addActionListener(this);
		btnEngage.setEnabled(false);

		// SETTING LAYOUT
		GridBagConstraints gbc = new GridBagConstraints();
		setLayout(grid_bag_layout);
		insert_grid(gbc, pnlVehicle, 0, 0, 1, 1, 0.1, 1.0, 0);
		add(pnlVehicle);
		insert_grid(gbc, pnlInfo, 1, 0, 1, 1, 0.85, 1.0, 0);
		add(pnlInfo);
		insert_grid(gbc, btnEngage, 2, 0, 1, 1, 0.05, 1.0, 0);
		add(btnEngage);

	}

	public void chkEngageEnabled() {
		repaint();

		iconV.chkEngageEnabled();

		if (v.getStatus() == MyGame.STATUS_VEHICLE_PENDING)
			btnEngage.setEnabled(true);
		else
			btnEngage.setEnabled(false);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnEngage) {
			lsnr.Engage(v);
		}

	}

	public void Update_Damage() {
		if (intDamage != Math.round(v.getDamage()))
			lblHealth.setForeground(Color.red);
		else
			lblHealth.setForeground(Color.black);
		intDamage = (int) Math.round(v.getDamage());
		lblHealth.setText("Damage : " + Math.round(intDamage));
	}

	private void insert_grid(GridBagConstraints gbc, Component cmpt, int x,
			int y, int width, int height, double percent_x, double percent_y,
			int ins) {
		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridx = x;
		gbc.gridy = y;
		gbc.gridwidth = width;
		gbc.gridheight = height;
		gbc.weightx = percent_x;
		gbc.weighty = percent_y;
		gbc.insets = new Insets(ins, ins, ins, ins);
		grid_bag_layout.setConstraints(cmpt, gbc);
	}
}

class VehiclePanel extends JPanel implements ActionListener {
	private static final long serialVersionUID = 4435452374430336399L;
	private GridBagLayout grid_bag_layout = new GridBagLayout();
	private Vehicle selectedVehicle;
    private JPanel pnlVehicle, pnlStatus;// , pnlControl;
    private JTextArea txtMission = new JTextArea(5, 5);
	private JScrollPane scrollPane;
	private JProgressBar progressBar;
	// private JButton btnGoal, btnAddWP, btnDelWP;
	private JButton btnEngage;
    private Reschu lsnr;
	private String name;

	VehiclePanel(Reschu e, Vehicle v) {
		double size[][] = { { 150, TableLayout.FILL }, { TableLayout.FILL } };

		GridBagConstraints gbc = new GridBagConstraints();
		lsnr = e;
		selectedVehicle = v;
		name = v.getName();
		// setLayout(grid_bag_layout);
		setLayout(new TableLayout(size));
		setPreferredSize(new Dimension(180, 230));

		//
		// TOP-LEFT
		pnlVehicle = new JPanel();
        TitledBorder bdrTitle = BorderFactory.createTitledBorder("Vehicle Info");
		pnlVehicle.setBorder(bdrTitle);
        ImageIcon imgIcon = new ImageIcon("Temporarily Unavailable");

    

        try {
            if(v.getVehicleClass()== Vehicle.VehicleClass.FIRESCOUT){

                URL url=FrameEnd.class.getClassLoader().getResource("firescout.jpg");
                imgIcon = new ImageIcon(url);
           
            }
            else if(v.getVehicleClass()== Vehicle.VehicleClass.GLOBALHAWK){
                URL url=FrameEnd.class.getClassLoader().getResource("globalhawk.jpg");
                imgIcon = new ImageIcon(url);

            }
            else if(v.getVehicleClass()== Vehicle.VehicleClass.TALISMAN){
                URL url=FrameEnd.class.getClassLoader().getResource("talisman.jpg");
                imgIcon = new ImageIcon(url);
            }
            else if(v.getVehicleClass()== Vehicle.VehicleClass.REAPER){
                URL url=FrameEnd.class.getClassLoader().getResource("reaper.jpg");
                imgIcon = new ImageIcon(url);
            }
            else if(v.getVehicleClass()== Vehicle.VehicleClass.SPARTANSCOUT){
                URL url=FrameEnd.class.getClassLoader().getResource("spartanscout.jpg");
                imgIcon = new ImageIcon(url);
            }
            
            else if(v.getVehicleClass()== Vehicle.VehicleClass.JDAM){
                URL url=FrameEnd.class.getClassLoader().getResource("jdam.jpg");
                imgIcon = new ImageIcon(url);
            }
            else if(v.getVehicleClass()== Vehicle.VehicleClass.KNIFEFISH){
                URL url=FrameEnd.class.getClassLoader().getResource("knifefish.jpg");
                imgIcon = new ImageIcon(url);
            }
            else if(v.getVehicleClass()== Vehicle.VehicleClass.GENERIC_AV){
                URL url=FrameEnd.class.getClassLoader().getResource("genericAV.jpg");
                imgIcon = new ImageIcon(url);
            }
            else if(v.getVehicleClass()== Vehicle.VehicleClass.GENERIC_SV){
                URL url=FrameEnd.class.getClassLoader().getResource("genericSV.jpg");
                imgIcon = new ImageIcon(url);
            }
            else if(v.getVehicleClass()== Vehicle.VehicleClass.GENERIC_UV){
                URL url=FrameEnd.class.getClassLoader().getResource("genericUV.jpg");
                imgIcon = new ImageIcon(url);
            }
            else if(v.getVehicleClass()== Vehicle.VehicleClass.RMMV){
                URL url=FrameEnd.class.getClassLoader().getResource("rmmv.jpg");
                imgIcon = new ImageIcon(url);
            }
            else if(v.getVehicleClass()== Vehicle.VehicleClass.BLACKJACK){
                URL url=FrameEnd.class.getClassLoader().getResource("blackjack.jpg");
                imgIcon = new ImageIcon(url);
            }
            else if(v.getVehicleClass()== Vehicle.VehicleClass.TRITON){
                URL url=FrameEnd.class.getClassLoader().getResource("triton.jpg");
                imgIcon = new ImageIcon(url);
            }
            else if(v.getVehicleClass()== Vehicle.VehicleClass.PREDATOR){
                URL url=FrameEnd.class.getClassLoader().getResource("predator.jpg");
                imgIcon = new ImageIcon(url);
            }
            else{
                throw new Exception();
            }
            //imgIcon = new ImageIcon(new URL(MyURL.URL_VEHICLE + v.getType() + "_" + v.getPayload() + ".jpg"));
        }
        catch (Exception ex) {
            System.out.println("image failed to load for: " + v.getName());

        }
        
        
		// lblVehicle = new JLabel(v.getName(), imgIcon, JLabel.CENTER);
        JLabel lblVehicle = new JLabel("", imgIcon, JLabel.CENTER);
		lblVehicle.setVerticalTextPosition(JLabel.CENTER);
		lblVehicle.setHorizontalTextPosition(JLabel.CENTER);
		lblVehicle.setFont(MyFont.fontBold);
		pnlVehicle.add(lblVehicle);

		//
		// TOP-RIGHT
		bdrTitle = BorderFactory
				.createTitledBorder("Vehicle Health And Status");
		pnlStatus = new JPanel();
		pnlStatus.setLayout(grid_bag_layout); // pnlStatus.setLayout(new
												// GridLayout(5,1));
		pnlStatus.setBorder(bdrTitle);
        JLabel lblDamage = new JLabel("Damage Level");
        JLabel lblMission = new JLabel("Current Mission");
		txtMission.setEditable(false);
		txtMission.setLineWrap(true);
		scrollPane = new JScrollPane(txtMission);
		scrollPane.setAutoscrolls(true);
		scrollPane.remove(scrollPane.getHorizontalScrollBar());
		btnEngage = new JButton("Engage");
		btnEngage.addActionListener(this);
		progressBar = new JProgressBar(0, 100);
		progressBar.setValue(0);
		progressBar.setStringPainted(true);
		insert_grid(gbc, lblDamage, 0, 0, 1, 1, 1, 0.1, 0);
		pnlStatus.add(lblDamage);
		insert_grid(gbc, progressBar, 0, 1, 1, 1, 1, 0.1, 0);
		pnlStatus.add(progressBar);
		insert_grid(gbc, lblMission, 0, 2, 1, 1, 1, 0.1, 0);
		pnlStatus.add(lblMission);
		insert_grid(gbc, scrollPane, 0, 3, 1, 1, 1, 0.5, 0);
		pnlStatus.add(scrollPane);
		insert_grid(gbc, btnEngage, 0, 4, 1, 1, 1, 0.1, 0);
		pnlStatus.add(btnEngage);

		add(pnlVehicle, "0,0");
		add(pnlStatus, "1,0");
		// insert_grid(gbc, pnlVehicle, 0, 0, 1, 1, 0, 0, 0); add(pnlVehicle);
		// insert_grid(gbc, pnlStatus, 1, 0, 1, 1, 0.9, 0, 0); add(pnlStatus);
	}

	public String getName() {
		return name;
	}

	public void chkEngageEnabled() {
		if (selectedVehicle.getStatus() == MyGame.STATUS_VEHICLE_PENDING)
			btnEngage.setEnabled(true);
		else
			btnEngage.setEnabled(false);
	}

	public void Update_Damage() {
		progressBar.setValue((int) Math.round(selectedVehicle.getDamage()));
	}

	public void Update_Payload(EngageScenario p) {
		txtMission.setText(p.getStatement());
	}

	public void Payload_Clear() {
		txtMission.setText("");
	}

	private void insert_grid(GridBagConstraints gbc, Component cmpt, int x,
			int y, int width, int height, double percent_x, double percent_y,
			int ins) {
		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridx = x;
		gbc.gridy = y;
		gbc.gridwidth = width;
		gbc.gridheight = height;
		gbc.weightx = percent_x;
		gbc.weighty = percent_y;
		gbc.insets = new Insets(ins, ins, ins, ins);
		grid_bag_layout.setConstraints(cmpt, gbc);
	}

	public void actionPerformed(ActionEvent e) {
		// if( e.getSource() == btnGoal ){
		// lsnr.Vehicle_Goal_From_pnlControl(selectedVehicle);}
		// if( e.getSource() == btnAddWP){ if( selectedVehicle.hasGoal() )
		// lsnr.Vehicle_WP_Add_From_pnlControl(selectedVehicle);}
		// if( e.getSource() == btnDelWP){ if( selectedVehicle.hasWaypoint() )
		// lsnr.Vehicle_WP_Del_From_pnlControl(selectedVehicle);}
		if (e.getSource() == btnEngage) {
   			lsnr.Engage(selectedVehicle);
		}
	}
}

// MAKE THIS SEPERATE
class VehicleIcon2 extends JPanel {
	private static final long serialVersionUID = -5961320475456746793L;
	private Vehicle v;
	private Color vColor;
	private boolean colorFlag;

	public VehicleIcon2(Vehicle v) {
		this.v = v;
		vColor = MyColor.COLOR_VEHICLE;
	}

	public void paint(Graphics g) {
		Graphics2D g2 = (Graphics2D) g.create();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		if (v.getMedium() == Vehicle.TerrainMedium.AIR) {
			g.setColor(Color.BLACK);
			g.drawArc(1, 3, MySize.SIZE_VEHICLE_WIDTH_PXL,
					MySize.SIZE_VEHICLE_HEIGHT_PXL, 0, 180);
			g.drawLine(1, MySize.SIZE_VEHICLE_HEIGHT_PXL / 2 + 3,
					MySize.SIZE_VEHICLE_WIDTH_PXL + 1,
					MySize.SIZE_VEHICLE_HEIGHT_PXL / 2 + 3);
			g.setColor(vColor);
			g.fillArc(1, 3, MySize.SIZE_VEHICLE_WIDTH_PXL,
					MySize.SIZE_VEHICLE_HEIGHT_PXL, 0, 180);
			paintString((Graphics2D) g, MySize.SIZE_VEHICLE_WIDTH_PXL / 2 - 1,
					MySize.SIZE_VEHICLE_HEIGHT_PXL / 2 - 2, Color.black,
					MyFont.fontSmallBold, Integer.toString(v.getIndex()));
		} else if (v.getMedium() == Vehicle.TerrainMedium.WATER) {
			g.setColor(Color.BLACK);
			g.drawArc(1, -MySize.SIZE_VEHICLE_HEIGHT_PXL / 2 + 3,
					MySize.SIZE_VEHICLE_WIDTH_PXL,
					MySize.SIZE_VEHICLE_HEIGHT_PXL, 180, 180);
			g.drawLine(1, 3, MySize.SIZE_VEHICLE_WIDTH_PXL + 1, 3);
			g.setColor(vColor);
			g.fillArc(1, -MySize.SIZE_VEHICLE_HEIGHT_PXL / 2 + 3,
					MySize.SIZE_VEHICLE_WIDTH_PXL,
					MySize.SIZE_VEHICLE_HEIGHT_PXL, 180, 180);
			paintString((Graphics2D) g, MySize.SIZE_VEHICLE_WIDTH_PXL / 2, 18,
					Color.black, MyFont.fontSmallBold,
					Integer.toString(v.getIndex()));
		}
	}

	public void paintString(Graphics2D g, int x, int y, Color color, Font font,
			String str) {
		g.setColor(color);
		g.setFont(font);
		g.drawString(str, x, y);
	}

	public void chkEngageEnabled() {
		if (v.getStatus() == MyGame.STATUS_VEHICLE_PENDING) {
			colorFlag = !colorFlag;
			if (colorFlag)
				vColor = MyColor.COLOR_VEHICLE;
			else
				vColor = MyColor.COLOR_VEHICLE_PENDING;
		} else {
			vColor = MyColor.COLOR_VEHICLE;
		}
		repaint();
	}
}
