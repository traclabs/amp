package reschu.game.model;

import reschu.game.model.Vehicle.Payload;

public class DestroyedTarget extends Target {
	private String vehicleThatDestroyedMe;
	private String obsTargetThatReplacedMe;

	public String getVehicleThatDestroyedMe() {
		return vehicleThatDestroyedMe;
	}

	public void setVehicleThatDestroyedMe(String vehicleThatDestroyedMe) {
		this.vehicleThatDestroyedMe = vehicleThatDestroyedMe;
	}

	public String getObsTargetThatReplacedMe() {
		return obsTargetThatReplacedMe;
	}

	public void setObsTargetThatReplacedMe(String obsTargetThatReplacedMe) {
		this.obsTargetThatReplacedMe = obsTargetThatReplacedMe;
	}

	public DestroyedTarget(String n, Payload payload, int[] p, Game game,
			String vehicleThatDestroyedMe, String obsTargetThatReplacedMe) {
		super(n, n, payload, p, game);
		this.vehicleThatDestroyedMe = vehicleThatDestroyedMe;
		this.obsTargetThatReplacedMe = obsTargetThatReplacedMe;
	}

	public DestroyedTarget(Target targ, String obsTarg,
			String vehicleName, Game game, Payload pay) {
		super(targ.getName(), targ.getName(), pay, targ.getPos(), game);
		
		this.obsTargetThatReplacedMe = obsTarg;
		this.vehicleThatDestroyedMe = vehicleName;
	}

}
