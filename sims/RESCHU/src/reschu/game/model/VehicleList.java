package reschu.game.model;

import java.util.ArrayList;
import java.util.LinkedList;

import reschu.constants.MyGame;
import reschu.game.controller.GUI_Listener;

public class VehicleList {
	
	private LinkedList<Vehicle> v_list;
	public VehicleList(Game g) {
		v_list = new LinkedList<Vehicle>();
	}

	/**
	 * Checks whether a vehicle list has a vehicle with a given name
	 */
	public boolean hasVehicle(String v_name) {
		for (int i = 0; i < v_list.size(); i++)
			if (v_list.get(i).getName().equals(v_name))
				return true;
		return false;
	}

	/**
	 * Add a vehicle
	 * 
	 *
	 * @param g
	 *            game
	 * @param m
	 *            reschuMap
	 * @param idx
	 *            index number
	 * @param l
	 *            listener
	 * @param v_name
	 *            vehicle name
	 * @param v_class
	 *            vehicle's class
	 * @param v_payload
	 *            vehicle's payload
	 * @param velocity
	 *            vehicle's velocity
	 */
	public void addVehicle(Game g, ReschuMap m, int idx, GUI_Listener l,
			String v_name, Vehicle.VehicleClass v_class,
			Vehicle.Payload[] v_payload, double velocity, int x, int y,
			boolean ground) throws UserDefinedException {
		if (this.hasVehicle(v_name))
			throw new UserDefinedException(v_name + " already exists.");
		
		// Create Payload set for Vehicle:

	ArrayList<Vehicle.Payload> vPayloads = new ArrayList<Vehicle.Payload>();
	for(int i = 0; i < v_payload.length; i++){
		vPayloads.add(v_payload[i]);
	}

		
		if (v_class == Vehicle.VehicleClass.TALISMAN
				|| v_class == Vehicle.VehicleClass.SPARTANSCOUT) {

			if (m.getCellType(x, y) != MyGame.SEA) {

				System.err
						.println("ERROR: a water vehicle's initial position must be in the sea.  program exiting");
				System.exit(-1);
				return;
			}
			/*
			 * while (m.getCellType(x, y) == MyGame.LAND) { x =
			 * rnd.nextInt(MySize.width); y = rnd.nextInt(MySize.height); }
			 */
		}

		Vehicle vehicle = new Vehicle(g, m, l, idx, v_name, v_class, vPayloads,
				ground);
		vehicle.setVelocityNominal(velocity);
		vehicle.setPos(x, y);

		v_list.addLast(vehicle);
	}

	public void addVehicle(Game g, ReschuMap m, int idx, GUI_Listener l,
			String v_name, Vehicle.VehicleClass v_class,
			Vehicle.Payload[] v_payload, double velocity, boolean ground,
			int[] initialpos) throws UserDefinedException {
		if (this.hasVehicle(v_name))
			throw new UserDefinedException(v_name + " already exists.");
		
		// Create Payload set for Vehicle:

	ArrayList<Vehicle.Payload> vPayloads = new ArrayList<Vehicle.Payload>();
	for(int i = 0; i < v_payload.length; i++){
		vPayloads.add(v_payload[i]);
	}

		int x = initialpos[0];
		int y = initialpos[1];

		Vehicle vehicle = new Vehicle(g, m, l, idx, v_name, v_class, vPayloads,
				ground);
		vehicle.setVelocityNominal(velocity);
		vehicle.setPos(x, y);

		v_list.addLast(vehicle);
	}

	/**
	 * Returns the size of the vehicle list
	 */
	public int size() {
		return v_list.size();
	}

	public Vehicle getVehicle(int i) {
		return v_list.get(i);
	}

	/**
	 * Get a vehicle with a given name
	 * 
	 * @param v_name
	 *            Vehicle name
	 * @throws UserDefinedException
	 */
	public Vehicle getVehicle(String v_name) throws UserDefinedException {
		if (!hasVehicle(v_name))
			throw new UserDefinedException("No such vehicle(" + v_name
					+ ") in Vehicle List.");

		for (int i = 0; i < v_list.size(); i++)
			if (v_list.get(i).getName().equals(v_name))
				return v_list.get(i);

		throw new UserDefinedException("No such vehicle(" + v_name
				+ ") in Vehicle List.");// Never reaches
	}

	/**
	 * Get a vehicle at a given position.
	 * 
	 * @param x
	 *            x-coordinate
	 * @param y
	 *            y-coordinate
	 */
	public Vehicle getVehicle(int x, int y) {
		for (int i = 0; i < v_list.size(); i++) {
			if (v_list.get(i).getX() == x && v_list.get(i).getY() == y)
				return v_list.get(i);
		}

		return null;
	}

	/**
	 * Returns total damage of all vehicles
	 */
	public int getTotalDamage() {
		int total_damage = 0;
		for (int i = 0; i < size(); i++) {
			total_damage += getVehicle(i).getDamage();
		}
		return total_damage;
	}
}