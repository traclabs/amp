package reschu.game.model;


public class EngageScenario {
	private int idx;
	private String statement;
	private String filename;
	private String vehicleType;
	private String targetType;
	private int[] location;
	private boolean done;

	public EngageScenario(int i, int[] loc, String vType, String tType, String stmt) {
		idx = i;
        
        String filenameVType;
        String filenameTType;
        if(vType.equals("air")){
            filenameVType="UAV";
        }
        else{
            filenameVType="UUV";
        }
        
        if(tType.equals("land")){
            filenameTType="LAND";
        }
        else{
            filenameTType="SHORE";
        }
        
		filename = idx + "_" + filenameVType + "_" + filenameTType + ".jpg";
		vehicleType = vType;
		targetType = tType;
		location = loc;
		statement = stmt;
		done = false;
	}

	public String getStatement() {
		return statement;
	}

	public String getFilename() {
        return "engageScenarios/"+ filename;
	}

	public Vehicle.TerrainMedium getVehicleType() {
        
        if(vehicleType.equals("air")){
            
            return Vehicle.TerrainMedium.AIR;
        }
        else if(vehicleType.equals("water")){
            return Vehicle.TerrainMedium.WATER;
        }
        
		return null;//shouldn't happen
	}

	public Vehicle.TerrainMedium getTargetType() {

        if(targetType.equals("land")){

            return Vehicle.TerrainMedium.LAND;
        }
        else if(targetType.equals("water")){
            return Vehicle.TerrainMedium.WATER;
        }

        return null;//shouldn't happen
        
	}

	public int[] getLocation() {
		return location;
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean b) {
		done = b;
	}
}
