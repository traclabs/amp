package reschu.game.model;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.EnumSet;
import java.util.Random;

import javax.swing.Timer;

import reschu.constants.MyColor;
import reschu.constants.MyGame;
import reschu.constants.MySize;
import reschu.constants.MySpeed;
import reschu.database.DBWriter;
import reschu.game.controller.GUI_Listener;
import reschu.game.controller.Reschu;
import reschu.game.model.Vehicle.VehicleClass;
import reschu.game.utils.FileReader;
import reschu.game.view.FrameEnd;
import reschu.game.view.PanelMsgBoard;

public class Game implements Runnable, ActionListener {
	static public int TIME_TOTAL_GAME = 10 * 60 * MySpeed.SPEED_CLOCK * 901;// it's
																			// over
																			// 9000!!

	// private double PROBABILITY_TARGET_VISIBILITY; // The higher, the more
	// visible target

	// private final int nTargetAreaTotal =
	// MyGame.nTOTAL_TARGETS;//(Reschu.tutorial()) ?
	// MyGame.nTARGET_AREA_TOTAL_TUTORIAL: MyGame.nTARGET_AREA_TOTAL;
	final private int[] DB_BY_PIXEL = new int[] { 480, 480, 470, 470, 470, 470,
			460, 460, 450, 450, 450, 440, 440, 430, 430, 430, 420, 410, 410,
			410, 410, 410, 410, 410, 410, 410, 410, 420, 420, 420, 430, 430,
			430, 430, 430, 430, 430, 420, 420, 420, 420, 420, 420, 420, 420,
			420, 430, 430, 440, 440, 450, 450, 460, 460, 460, 460, 470, 470,
			480, 480, 480, 490, 490, 490, 490, 500, 500, 500, 510, 510, 510,
			510, 510, 520, 520, 520, 520, 520, 520, 530, 530, 540, 550, 550,
			560, 560, 570, 570, 580, 580, 590, 590, 590, 600 };

	final private EnumSet<VehicleClass> airVehicles = EnumSet.of(
			VehicleClass.GLOBALHAWK, VehicleClass.FIRESCOUT,
			VehicleClass.REAPER, VehicleClass.JDAM, VehicleClass.GENERIC_AV,
			VehicleClass.PREDATOR);

	final private EnumSet<VehicleClass> waterVehicles = EnumSet.of(
			VehicleClass.TALISMAN, VehicleClass.SPARTANSCOUT,
			VehicleClass.KNIFEFISH, VehicleClass.RMMV, VehicleClass.GENERIC_SV,
			VehicleClass.GENERIC_UV);

	private int[] DB = new int[MySize.height];

	static public Calendar cal = Calendar.getInstance();

	private VehicleList vehicle_list;
	private EngageScenarioList engageScenarioList;

	public ReschuMap reschuMap;

	public ReschuMap getReschuMap() {
		return reschuMap;
	}

	public int ex_pos_x, ex_pos_y;
	private GUI_Listener lsnr;
	private Timer tmr_clock;
	private int elapsedTime;
	private Vehicle currentPayloadVehicle;
	private boolean vehicleColorFlashFlag = true;
	private DBWriter dbWriter;
	private StructTargetNamePool[] targetNamePool;

	private int score;

	private Random rnd = new Random();

	private int scenario;
	private boolean autoassign;

	public synchronized int getElapsedTime() {
		return elapsedTime;
	}

	public Game(GUI_Listener l, int scenario, boolean autoassign) {
		this.scenario = scenario;
		this.autoassign = autoassign;
		if (Reschu.train())
			Game.TIME_TOTAL_GAME *= 10;

		if (Reschu._database) {
			new Thread(new Runnable() {
				public void run() {
					dbWriter = new DBWriter();
					dbWriter.CreateTable(Reschu._username);
				}
			}).start();
		}

		lsnr = l;

		rnd.setSeed(getSeedNum(scenario));
		// setProbability_Target_Visibility(scenario);

		PanelMsgBoard.Msg("Game Started");
		tmr_clock = new Timer((int) MySpeed.SPEED_TIMER, this);
		for (int i = 0; i < DB.length; i++)
			DB[i] = DB_BY_PIXEL[Math.round(i / 5)] / MySize.SIZE_CELL;

		vehicle_list = new VehicleList(this);
		// @change-removed passing random object to PayloatList() 2008-04-01
		engageScenarioList = new EngageScenarioList();

		reschuMap = new ReschuMap(MySize.width, MySize.height, this, lsnr);
		elapsedTime = 0;

		targetNamePool = new StructTargetNamePool[22];
		for (int i = 0; i < 22; i++)
			targetNamePool[i] = new StructTargetNamePool("" + (char) (65 + i));

		setMap();

		// reschuMap.setTargetAreaRandomly(rnd);
		reschuMap.setTargetAreaFromFile();

		// setVehiclesBasedOnScenario(scenario);
		setVehiclesFromConfigFile();

		prepareEngageScenarioList();
	}

	public void setListener(GUI_Listener l) {
		lsnr = l;
	}

	public DBWriter getDBWriter() {
		return dbWriter;
	}

	private int getSeedNum(int scenario) {
		if (Reschu.tutorial() || Reschu.train()) {
			switch (scenario) {
			case 1:
			case 2:
			case 3:
			case 4:
				return 50;
			default:
				return 0;
			}
		} else {
			switch (scenario) {
			/*
			 * @changed 2008-06-29 Carl
			 * 
			 * case 1: return 10; case 2: return 20; case 3: return 30; case 4:
			 * return 40; default: return 0;
			 */
			case 1:
			case 2:
			case 3:
			case 4:
				return 10;
			default:
				return 0;
			}
		}
	}

	/*
	 * private void setProbability_Target_Visibility(int scenario) { switch
	 * (scenario) { case 1: PROBABILITY_TARGET_VISIBILITY = 1; break; case 2:
	 * PROBABILITY_TARGET_VISIBILITY = 1; break; case 3:
	 * PROBABILITY_TARGET_VISIBILITY = 0.5; break; case 4:
	 * PROBABILITY_TARGET_VISIBILITY = 0.7; break; default:
	 * PROBABILITY_TARGET_VISIBILITY = 1; break; } }
	 */

	public void setVehiclesFromConfigFile() {

		FileReader fr = null;
		try {
			URL url = Game.class.getClassLoader().getResource(
					"config_vehicles_in_existence.dat");
			System.out.println("loading " + url);
			fr = new FileReader(url);
			fr.openFile();
		} catch (Exception e) {
			System.out.println("load vehicles data error");
			e.printStackTrace();
			System.exit(-1);
		}

		String aLine;
		@SuppressWarnings("unused")
		int lineNum = -1, idx;
		while ((aLine = fr.readLineByLine()) != null) {

			lineNum++;
			String[] line = aLine.split(" ");

			if (!(line.length == 6 || line.length == 7)) {
				System.err
						.println("ERROR: Failed to load vehicle at line ("
								+ lineNum
								+ "), check to make sure there are a correct number of args.  program exiting");
				System.exit(0);
			}

			String name = line[0];
			Vehicle.VehicleClass vehicleClass;
			Vehicle.Payload payloadList[];
			int velocity;
			int x_pos;
			int y_pos;

			boolean grounded;

			// ///////////////
			/*
			 * Determine the vehicle class
			 */
			if (line[1].equals("firescout")) {
				vehicleClass = Vehicle.VehicleClass.FIRESCOUT;
			} else if (line[1].equals("reaper")) {
				vehicleClass = Vehicle.VehicleClass.REAPER;
			} else if (line[1].equals("talisman")) {
				vehicleClass = Vehicle.VehicleClass.TALISMAN;
			} else if (line[1].equals("globalhawk")) {
				vehicleClass = Vehicle.VehicleClass.GLOBALHAWK;
			} else if (line[1].equals("spartanscout")) {
				vehicleClass = Vehicle.VehicleClass.SPARTANSCOUT;
			} else if (line[1].equals("jdam")) {
				vehicleClass = Vehicle.VehicleClass.JDAM;
			} else if (line[1].equals("knifefish")) {
				vehicleClass = Vehicle.VehicleClass.KNIFEFISH;
			} else if (line[1].equals("rmmv")) {
				vehicleClass = Vehicle.VehicleClass.RMMV;
			} else if (line[1].equals("predator")) {
				vehicleClass = Vehicle.VehicleClass.PREDATOR;
			} else if (line[1].equals("generic_av")) {
				vehicleClass = Vehicle.VehicleClass.GENERIC_AV;
			} else if (line[1].equals("generic_sv")) {
				vehicleClass = Vehicle.VehicleClass.GENERIC_SV;
			} else if (line[1].equals("generic_uv")) {
				vehicleClass = Vehicle.VehicleClass.GENERIC_UV;
			} else if (line[1].equals("blackjack")) {
				vehicleClass = Vehicle.VehicleClass.BLACKJACK;
			} else if (line[1].equals("triton")) {
				vehicleClass = Vehicle.VehicleClass.TRITON;
			}

			else {
				System.out.println("load vehicles class not recognized");
				System.exit(-1);
				return;
			}

			// ////////////////
			String paylloadListSTRING[] = line[2].split("&");
			payloadList = new Vehicle.Payload[paylloadListSTRING.length];

			for (int i = 0; i < paylloadListSTRING.length; i++) {
				try {
					payloadList[i] = Vehicle.stringToPayload(
							paylloadListSTRING[i], false);
				} catch (Exception e) {
					System.err
							.println("ERROR: Failed to load vehicle's payload list at line ("
									+ lineNum
									+ "), argument misformated or non-existent.  program exiting");
					System.exit(0);
					return;
				}
			}

			// /////////////////
			try {
				velocity = Integer.parseInt(line[3]);
			} catch (NumberFormatException e) {
				System.err
						.println("ERROR: Failed to load target at line ("
								+ lineNum
								+ "), velocity argument misformated or non-existent.  program exiting");
				System.exit(0);
				return;
			}

			// //////////////////////////////////////////////////////

			if (line[4].equals("notgrounded")) {

				try {
					x_pos = Integer.parseInt(line[5]);
				} catch (NumberFormatException e) {
					System.err
							.println("ERROR: Failed to load target at line ("
									+ lineNum
									+ "), x position argument misformated or non-existent.  program exiting");
					System.exit(0);
					return;
				}

				// //////////////
				try {
					y_pos = Integer.parseInt(line[6]);
				} catch (NumberFormatException e) {
					System.err
							.println("ERROR: Failed to load target at line ("
									+ lineNum
									+ "), y position argument misformated or non-existent.  program exiting");
					System.exit(0);
					return;
				}

				grounded = false;

				try {
					vehicle_list.addVehicle(this, reschuMap, lineNum, lsnr,
							name, vehicleClass, payloadList, velocity, x_pos,
							y_pos, grounded);
				} catch (Exception e) {
					e.printStackTrace();
				}

			} else if (line[4].equals("grounded")) {

				grounded = true;

				if (line[5].length() == 1) {
					ArrayList<Target> unassignedTargets = reschuMap
							.getTargetList();

					Target tempTarget = null;
					for (int i = 0; i < unassignedTargets.size(); i++) {
						tempTarget = unassignedTargets.get(i);
						if (tempTarget.getName().equals(line[5])) {

							if ((this.waterVehicles.contains(vehicleClass))
									&& (!tempTarget.getAvailablePayloads()
											.contains(Vehicle.Payload.SEABASE))) {
								System.out
										.println("a water vehicle can't start on a land base, program exiting");
								System.exit(-1);
								return;
							}

							if ((this.airVehicles.contains(vehicleClass))
									&& (!(tempTarget.getAvailablePayloads()
											.contains(Vehicle.Payload.AIRBASE)))) {

								if (!vehicleClass
										.equals(VehicleClass.FIRESCOUT)) {
									System.out
											.println("an air vehicle can't start on a sea base, program exiting");
									System.exit(-1);
									return;
								}
							}

							int[] initialpos = unassignedTargets.get(i)
									.getPos().clone();

							try {
								vehicle_list.addVehicle(this, reschuMap,
										lineNum, lsnr, name, vehicleClass,
										payloadList, velocity, grounded,
										initialpos);
								break;
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				} else {
					System.out
							.println("failed to load, base name must be a single character");
					System.exit(-1);
					return;
				}

			} else {
				System.out
						.println("failed to load, vehicle must be grounded or notgrounded, if grounded, base must be specified");
				System.exit(-1);
				return;
			}

		}

		fr.closeFile();

	}

	/*
	 * public void setVehiclesBasedOnScenario(int scenario) { try { switch
	 * (scenario) { case 1:
	 * vehicle_list.addVehicle(this,reschuMap,0,lsnr,"Fire Scout A"
	 * ,Vehicle.VehicleClass.firescout,new
	 * Vehicle.Payload[]{Vehicle.Payload.inspectcam,
	 * Vehicle.Payload.SRsensors},2,false); //vehicle_list.addVehicle(0,
	 * Vehicle.VehicleClass.firescout, "Fire Scout A",Vehicle.PAYLOAD_ISR, 500 /
	 * MySpeed.SPEED_CONTROL, rnd,reschuMap, lsnr, this); if
	 * (!Reschu.tutorial()){
	 * vehicle_list.addVehicle(this,reschuMap,1,lsnr,"Fire Scout B"
	 * ,Vehicle.VehicleClass.firescout,new
	 * Vehicle.Payload[]{Vehicle.Payload.inspectcam,
	 * Vehicle.Payload.SRsensors},2,false);
	 * vehicle_list.addVehicle(this,reschuMap
	 * ,2,lsnr,"Fire Scout C",Vehicle.VehicleClass.firescout,new
	 * Vehicle.Payload[]{Vehicle.Payload.inspectcam,
	 * Vehicle.Payload.SRsensors},2,false);
	 * vehicle_list.addVehicle(this,reschuMap
	 * ,3,lsnr,"Fire Scout D",Vehicle.VehicleClass.firescout,new
	 * Vehicle.Payload[]{Vehicle.Payload.inspectcam,
	 * Vehicle.Payload.SRsensors},2,false);
	 * vehicle_list.addVehicle(this,reschuMap
	 * ,4,lsnr,"Fire Scout E",Vehicle.VehicleClass.firescout,new
	 * Vehicle.Payload[]{Vehicle.Payload.inspectcam,
	 * Vehicle.Payload.SRsensors},2,false); } break; case 2:
	 * vehicle_list.addVehicle
	 * (this,reschuMap,0,lsnr,"Fire Scout A",Vehicle.VehicleClass.firescout,new
	 * Vehicle.Payload[]{Vehicle.Payload.inspectcam,
	 * Vehicle.Payload.SRsensors},2,false);
	 * vehicle_list.addVehicle(this,reschuMap
	 * ,1,lsnr,"Talisman A",Vehicle.VehicleClass.talisman,new
	 * Vehicle.Payload[]{Vehicle.Payload.inspectcam,
	 * Vehicle.Payload.SRsensors},2,false); if (!Reschu.tutorial()){
	 * vehicle_list
	 * .addVehicle(this,reschuMap,2,lsnr,"Fire Scout B",Vehicle.VehicleClass
	 * .firescout,new Vehicle.Payload[]{Vehicle.Payload.inspectcam,
	 * Vehicle.Payload.SRsensors},2,false);
	 * vehicle_list.addVehicle(this,reschuMap
	 * ,3,lsnr,"Talisman B",Vehicle.VehicleClass.talisman,new
	 * Vehicle.Payload[]{Vehicle.Payload.inspectcam,
	 * Vehicle.Payload.SRsensors},2,false);
	 * vehicle_list.addVehicle(this,reschuMap
	 * ,4,lsnr,"Fire Scout C",Vehicle.VehicleClass.firescout,new
	 * Vehicle.Payload[]{Vehicle.Payload.inspectcam,
	 * Vehicle.Payload.SRsensors},2,false); } break; case 3:
	 * vehicle_list.addVehicle
	 * (this,reschuMap,0,lsnr,"Fire Scout A",Vehicle.VehicleClass.firescout,new
	 * Vehicle.Payload[]{Vehicle.Payload.inspectcam,
	 * Vehicle.Payload.SRsensors},2,false);
	 * vehicle_list.addVehicle(this,reschuMap
	 * ,1,lsnr,"Global Hawk A",Vehicle.VehicleClass.globalhawk,new
	 * Vehicle.Payload[]{Vehicle.Payload.LRsensors},2,false); if
	 * (!Reschu.tutorial()){
	 * vehicle_list.addVehicle(this,reschuMap,2,lsnr,"Fire Scout B"
	 * ,Vehicle.VehicleClass.firescout,new
	 * Vehicle.Payload[]{Vehicle.Payload.inspectcam,
	 * Vehicle.Payload.SRsensors},2,false);
	 * vehicle_list.addVehicle(this,reschuMap
	 * ,3,lsnr,"Global Hawk B",Vehicle.VehicleClass.globalhawk,new
	 * Vehicle.Payload[]{Vehicle.Payload.LRsensors},2,false);
	 * vehicle_list.addVehicle
	 * (this,reschuMap,4,lsnr,"Fire Scout C",Vehicle.VehicleClass.firescout,new
	 * Vehicle.Payload[]{Vehicle.Payload.inspectcam,
	 * Vehicle.Payload.SRsensors},2,false); } break; case 4:
	 * vehicle_list.addVehicle
	 * (this,reschuMap,0,lsnr,"Fire Scout A",Vehicle.VehicleClass.firescout,new
	 * Vehicle.Payload[]{Vehicle.Payload.inspectcam,
	 * Vehicle.Payload.SRsensors},2,false);
	 * vehicle_list.addVehicle(this,reschuMap
	 * ,1,lsnr,"Talisman",Vehicle.VehicleClass.talisman,new
	 * Vehicle.Payload[]{Vehicle.Payload.inspectcam,
	 * Vehicle.Payload.SRsensors},2,false);
	 * vehicle_list.addVehicle(this,reschuMap
	 * ,2,lsnr,"Global Hawk",Vehicle.VehicleClass.globalhawk,new
	 * Vehicle.Payload[]{Vehicle.Payload.LRsensors},2,false); if
	 * (!Reschu.tutorial()){
	 * vehicle_list.addVehicle(this,reschuMap,3,lsnr,"Fire Scout B"
	 * ,Vehicle.VehicleClass.firescout,new
	 * Vehicle.Payload[]{Vehicle.Payload.inspectcam,
	 * Vehicle.Payload.SRsensors},2,false);
	 * vehicle_list.addVehicle(this,reschuMap
	 * ,4,lsnr,"Reaper",Vehicle.VehicleClass.reaper,new
	 * Vehicle.Payload[]{Vehicle.Payload.weapons,
	 * Vehicle.Payload.SRsensors},2,false); } break; default:
	 * vehicle_list.addVehicle
	 * (this,reschuMap,0,lsnr,"Fire Scout A",Vehicle.VehicleClass.firescout,new
	 * Vehicle.Payload[]{Vehicle.Payload.inspectcam,
	 * Vehicle.Payload.SRsensors},2,false); break;
	 * 
	 * //add a case that starts out with a blank scenario, reads from a config
	 * file
	 * 
	 * }
	 * 
	 * } catch (UserDefinedException e) { e.printStackTrace(); } }
	 */
	public void prepareEngageScenarioList() {

		FileReader fr = null;
		try {
			URL url = Game.class.getClassLoader().getResource(
					"engageScenarios/info.dat");
			fr = new FileReader(url);

			fr.openFile();

		} catch (Exception e) {
			System.out.println("load scenarios data error");
			e.printStackTrace();
			System.exit(-1);
		}

		String vec_medium, target_medium, stmt;
		int[] loc;

		String aLine;
		int lineNum = -1, idx;
		while ((aLine = fr.readLineByLine()) != null) {
			lineNum++;
			String[] a = aLine.split("#");
			if (a.length == 6) {
				if (!FileReader.isNumber(a[0].trim())
						|| !FileReader.isNumber(a[3].trim())
						|| !FileReader.isNumber(a[4].trim())) {
					System.err
							.println("ERROR: Failed to load payload at line ("
									+ lineNum + "), check the numbers.");
					continue;
				}
				if (!Vehicle.isAProperVehMedium(a[1].trim())) {
					System.err
							.println("ERROR: Failed to load payload at line ("
									+ lineNum + "), check the vehicle type ("
									+ a[1].trim() + ").");
					continue;
				}
				if (!Target.isProperTargetMedium(a[2].trim())) {
					System.err
							.println("ERROR: Failed to load payload at line ("
									+ lineNum + "), check the mission type ("
									+ a[2].trim() + ").");
					continue;
				}

				idx = Integer.parseInt(a[0].trim());
				vec_medium = a[1].trim();
				target_medium = a[2].trim();
				loc = new int[] { Integer.parseInt(a[3].trim()),
						Integer.parseInt(a[4].trim()) };
				stmt = a[5].trim();

				engageScenarioList.addEngageScenario(idx, vec_medium,
						target_medium, loc, stmt);
				// System.out.println(idx +", " + vec_medium +", " +mis_type
				// +", "
				// +loc[0] +", " +loc[1] + ", " +stmt);
			} else {
				System.err.println("ERROR: Failed to load payload at line ("
						+ lineNum + "), check the delimeter.");
			}
		}
		fr.closeFile();
	}

	private void setMap() {
		for (int i = 0; i < MySize.height; i++) {
			for (int j = 0; j < DB[i]; j++)
				reschuMap.setCellType(j, i, MyGame.LAND);
			reschuMap.setCellType(DB[i], i, MyGame.SEASHORE);
			for (int j = DB[i] + 1; j < MySize.width; j++)
				reschuMap.setCellType(j, i, MyGame.SEA);
		}
	}

	public void run() {

		// normalize randomizer
		if (Reschu.tutorial()) {
			for (int i = 0; i < 21; i++)
				rnd.nextInt(1);
			reschuMap.setHazardArea(rnd);
			// reschuMap.setTargetArea_TEMPORARY_FOR_TUTORIAL_BY_CARL(rnd);
		} else {
			reschuMap.setHazardArea(rnd);
		}

		if (autoassign) {
			AutoTargetAssignAll();
		}

		tmr_clock.start();

		if (!Reschu._database)
			return;

		if (Reschu.expermient()) {
			lsnr.EVT_System_GameStart();

			// below DBwrites are different from writing to the user's table.
			// we record the login info to the USER table,
			// which contains infos of all the users
			getDBWriter().UserTable_SetGameStart(Reschu._username);
			getDBWriter().UserTable_SetTime(Reschu._username);
		}
	}

	public boolean isRunning() {
		return tmr_clock.isRunning();
	}

	public void stop() {
		tmr_clock.stop();
	}

	public VehicleList getVehicleList() {
		return vehicle_list;
	}

	public EngageScenarioList getEngageScenarioList() {
		return engageScenarioList;
	}

	public void vehicle_location_change() {
		lsnr.vehicle_location_changed();
	}

	public Vehicle Vechicle_Location_Check(int x, int y) {
		for (int i = 0; i < vehicle_list.size(); i++) {
			if (!vehicle_list.getVehicle(i).isGrounded()) {
				int v_x = vehicle_list.getVehicle(i).getX();
				int v_y = vehicle_list.getVehicle(i).getY();
				int w = Math.round(MySize.SIZE_VEHICLE_WIDTH_PXL
						/ MySize.SIZE_CELL);
				int h = Math.round(MySize.SIZE_VEHICLE_HEIGHT_PXL
						/ MySize.SIZE_CELL);
				for (int j = -w; j < w; j++)
					for (int k = -h; k < h; k++)
						if (v_x == x + j && v_y == y + k)
							return vehicle_list.getVehicle(i);
			}
		}
		return null;
	}

	public StructSelectedPoint Vehicle_Goal_Check(int x, int y) {
		Vehicle v;
		int w = Math.round(MySize.SIZE_WAYPOINT_PXL / MySize.SIZE_CELL);
		for (int i = 0; i < vehicle_list.size(); i++) {
			v = vehicle_list.getVehicle(i);
			if (v.hasGoal()) {
				int w_x = v.getPath().get(v.getPathSize() - 1)[0];
				int w_y = v.getPath().get(v.getPathSize() - 1)[1];
				for (int m = -w; m < w; m++)
					for (int n = -w; n < w; n++)
						if (w_x == x + m && w_y == y + n)
							return new StructSelectedPoint(v, w_x, w_y, 0); // 0
																			// =
																			// no
																			// meaning
			}
		}
		return null;
	}

	public StructSelectedPoint Vehicle_Waypoint_Check(int x, int y) {
		Vehicle v;
		int w = Math.round(MySize.SIZE_WAYPOINT_PXL / MySize.SIZE_CELL);
		for (int i = 0; i < vehicle_list.size(); i++) {
			v = vehicle_list.getVehicle(i);
			if (v.getPath().size() > 1) {
				for (int j = 0; j < v.getPath().size() - 1; j++) {
					int w_x = v.getPath().get(j)[0];
					int w_y = v.getPath().get(j)[1];
					for (int m = -w; m < w; m++)
						for (int n = -w; n < w; n++)
							if (w_x == x + m && w_y == y + n)
								return new StructSelectedPoint(v, w_x, w_y, j);
				}
			}
		}
		return null;
	}

	public void addScore(int i) {
		score += i;
	}

	public int getScore() {
		return score;
	}

	public Vehicle getCurrentPayloadVehicle() {
		return currentPayloadVehicle;
	}

	public void setCurrentPayloadVehicle(Vehicle v) {
		currentPayloadVehicle = v;
	}

	public void clearCurrentPayloadVehicle() {
		currentPayloadVehicle = null;
	}

	private void AutoTargetAssignAll() {
		Vehicle v;
		for (int i = 0; i < vehicle_list.size(); i++) {
			v = vehicle_list.getVehicle(i);
			AutoTargetAssign(v);
		}
	}

	public void AutoTargetAssign(Vehicle v) {
		//
		// NOTE: FIXME - (ARS FEB 2015) - This function was commented out as it
		// is not needed at this time. This will need to be revisited to
		// accommodate for multiple Payloads for Targets and Vehicles.
		//
		//
		//
		// if (v.getPath().size() == 0 && reschuMap.getAvailableTarget() > 0) {
		// Target target;
		//
		// if (v.getVehicleClass() == Vehicle.VehicleClass.TALISMAN) {
		// for (int i = 0; i < reschuMap.getListUnassignedTarget().size(); i++)
		// {
		// if (reschuMap.getListUnassignedTarget().get(i).getMedium() == v
		// .getMedium()
		// && v.vHasPayloadForTarget(reschuMap
		// .getListUnassignedTarget().get(i)
		// .getIntendedPayload())) {
		// target = reschuMap.getListUnassignedTarget().get(i);
		// v.addGoal(target.getPos()[0], target.getPos()[1]);
		// if (elapsedTime != 0)
		// lsnr.EVT_GP_SetGP_by_System(v.getIndex(),
		// target.getName());
		// break;
		// }
		// }
		// } else if (v.getVehicleClass() == Vehicle.VehicleClass.FIRESCOUT) {
		// for (int i = 0; i < reschuMap.getListUnassignedTarget().size(); i++)
		// {
		// if (v.vHasPayloadForTarget(reschuMap
		// .getListUnassignedTarget().get(i)
		// .getIntendedPayload())) {
		// target = reschuMap.getListUnassignedTarget().get(i);
		// v.addGoal(target.getPos()[0], target.getPos()[1]);
		// if (elapsedTime != 0)
		// lsnr.EVT_GP_SetGP_by_System(v.getIndex(),
		// target.getName());
		// break;
		// }
		// }
		// } else if (v.getVehicleClass() == Vehicle.VehicleClass.GLOBALHAWK) {
		// for (int i = 0; i < reschuMap.getListUnassignedTarget().size(); i++)
		// {
		// if (v.vHasPayloadForTarget(reschuMap
		// .getListUnassignedTarget().get(i)
		// .getIntendedPayload())) {
		// target = reschuMap.getListUnassignedTarget().get(i);
		// v.addGoal(target.getPos()[0], target.getPos()[1]);
		// if (elapsedTime != 0)
		// lsnr.EVT_GP_SetGP_by_System(v.getIndex(),
		// target.getName());
		// break;
		// }
		// }
		// }
		// }
	}

	public void actionPerformed(ActionEvent e) {
		elapsedTime += MySpeed.SPEED_TIMER;
		if (elapsedTime >= Game.TIME_TOTAL_GAME) {
			stop(); // stops a timer

			if (Reschu._database) {
				getDBWriter().ScoreTable_SetScore(Reschu._username, getScore());
				getDBWriter().UserTable_SetGameFinish(Reschu._username);
			}

			lsnr.Game_End();

			// TEMPORARY SOLUTION
			FrameEnd frmEnd = new FrameEnd(lsnr);
			frmEnd.setSize(400, 300);
			frmEnd.setLocation(300, 300);
			frmEnd.setVisible(true);
		}

		vehicleColorFlashFlag = !vehicleColorFlashFlag;

		for (int i = 0; i < vehicle_list.size(); i++) {
			Vehicle v = vehicle_list.getVehicle(i);
			if (v.getPath().size() != 0
					&& (elapsedTime % v.getVelocityActual() == 0)
					&& !v.isGrounded()) {
				v.moveHillClimbing();
			}
		}
		vehicle_location_change();

		// Update pnlControl's "ENGAGE" button
		if (elapsedTime % MySpeed.SPEED_CLOCK == 0)
			lsnr.Clock_Tick(elapsedTime);

		// Pending Vehicle's Flashing Color
		if (vehicleColorFlashFlag)
			MyColor.COLOR_VEHICLE_PENDING = new Color(128, 224, 255, 255);
		else
			MyColor.COLOR_VEHICLE_PENDING = new Color(228, 124, 155, 255);

		// Update Hazard Area
		int haUpdateSpeed = (Reschu.tutorial()) ? MySpeed.SPEED_CLOCK_HAZARD_AREA_UPDATE_TUTORIAL
				: MySpeed.SPEED_CLOCK_TARGET_AREA_UPDATE;
		if (elapsedTime % haUpdateSpeed == 0) {
			reschuMap.delHazardArea(rnd, 1);
			reschuMap.setHazardArea(rnd);
		}

		// Update Targets

		if (elapsedTime % MySpeed.SPEED_CLOCK_TARGET_AREA_UPDATE == 0) {
			reschuMap.garbageTargetCollect();
			// reschuMap.setTargetAreaRandomly(rnd);
		}

		// Auto Target Assign
		// Problem - Should avoid when a vehicle's status is set to PENDING
		// if( elapsedTime % MySpeed.SPEED_CLOCK_AUTO_TARGET_ASSIGN_UPDATE == 0)
		// { AutoTargetAssignAll(); }

		// Check Vehicle - Hazard Area
		if (elapsedTime % MySpeed.SPEED_CLOCK_DAMAGE_CHECK == 0)
			for (int i = 0; i < vehicle_list.size(); i++)
				vehicle_list.getVehicle(i).chkHazardArea();
	}

	public void setTargetUsed(String name, boolean isUsed) {
		for (int i = 0; i < targetNamePool.length; i++)
			if (targetNamePool[i].getName().equals(name))
				targetNamePool[i].setUsed(isUsed);
	}

	public String getEmptyTargetName() throws IndexOutOfBoundsException {
		for (int i = 0; i < targetNamePool.length; i++) {
			// System.out.println("targetNamePool "+i+": "+targetNamePool[i].getName());
			if (!targetNamePool[i].isUsed()) {
				targetNamePool[i].setUsed(true);
				return targetNamePool[i].getName();
			}
		}
		throw new IndexOutOfBoundsException(
				"ERROR, no empty Target names available."); // FIXME - Really!?!
															// - MAKE SURE THIS
															// NEVER HAPPENS!!
															// IT SHOULDN'T BE
															// HAPPEN! //-
															// fuckin relax,
															// whoever wrote
															// that shit, it
															// won't cost you ur
															// phd
	}

	public boolean checkTargetNameOKThenMarkAsUsed(String name) {
		if (name.length() == 1) {

			int nameAscii = (int) (name.charAt(0));
			if (nameAscii > 64 && nameAscii < 88) {

				for (int i = 0; i < targetNamePool.length; i++) {
					if (targetNamePool[i].getName().equals(name)
							&& !targetNamePool[i].isUsed()) {
						targetNamePool[i].setUsed(true);
						return true;
					}
				}

				// targetNamePool[nameAscii-64].setUsed(true);
				// return true;
			}
		}

		return false;

	}

	public static double getDistance(double x1, double y1, double x2, double y2) {
		return Math.sqrt(Math.pow((double) (x2 - x1), 2.0)
				+ Math.pow((double) (y2 - y1), 2.0));
	}

	public static double getDistance(int x1, int y1, int x2, int y2) {
		return Math.sqrt(Math.pow((double) (x2 - x1), 2.0)
				+ Math.pow((double) (y2 - y1), 2.0));
	}

	public int getScenario() {
		return scenario;
	}
}