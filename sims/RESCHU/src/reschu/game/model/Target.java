package reschu.game.model;


import java.util.ArrayList;

import reschu.constants.MyGame;

public class Target {

	final private String name;

	
	private String imageFileName;

	private boolean destroyed;

	private ArrayList<Vehicle.Payload> availablePayloads;

	final private Vehicle.TerrainMedium medium;
	// it should be noted that the terrain medium must be totally dependent on
	// the position.
	// if it's in water, then the medium is water. if it's on the shore, then we
	// say the medium is also water. land for land.
	// it does not make sense for the target to have a medium of land and be
	// located in the middle of the ocean on the map

	// final private int[] pos;
	private Position pos;

	// private String type;
	private boolean done;

	// private boolean visible;

	private Vehicle vehicleAssigned;

	// private LinkedList<Vehicle> vehiclesAssigned = new LinkedList<Vehicle>();

	public Target(String n, String imageFileName, Vehicle.Payload payload,
			int[] p, Game game) {
		availablePayloads =new ArrayList<Vehicle.Payload>();
		availablePayloads.add(payload);

		name = n;

		if (p.length <= 2) {
			pos = new Position(p[0], p[1], 0);
		} else {
			pos = new Position(p[0], p[1], p[2]);
		}

		if (game.reschuMap.getMap_array()[pos.getX()][pos.getY()] == MyGame.LAND) {
			medium = Vehicle.TerrainMedium.LAND;
		} else {// if(game.reschuMap.getMap_array()[pos[0]][pos[1]]==
				// MyGame.SEASHORE ||
				// game.reschuMap.getMap_array()[pos[0]][pos[1]]== MyGame.SEA){
			medium = Vehicle.TerrainMedium.WATER;
		}

		done = false;

		this.imageFileName = imageFileName;

	}

	// public Target(String n, Vehicle.Payload payload, int[] p, Vehicle veh,
	// Game g) {
	//
	// this(n, payload, p, g);
	//
	// vehicleAssigned = (veh);
	//
	// }
	//
	// public Target(String n, Set<Vehicle.Payload> payload,
	// Vehicle.Payload defaultPayload, int[] p, Vehicle veh, Game game) {
	// availablePayloads = EnumSet.copyOf(payload);
	//
	// name = n;
	//
	// if (p.length <= 2) {
	// pos = new Position(p[0], p[1], 0);
	// } else {
	// pos = new Position(p[0], p[1], p[2]);
	// }
	//
	// if (game.reschuMap.getMap_array()[pos.getX()][pos.getY()] == MyGame.LAND)
	// {
	// medium = Vehicle.TerrainMedium.LAND;
	// } else {
	// medium = Vehicle.TerrainMedium.WATER;
	// }
	//
	// done = false;
	//
	// vehicleAssigned = (veh);
	//
	//
	// try {
	// // Grab the InputStream for the image.
	// InputStream in =
	// Target.class.getClassLoader().getResourceAsStream("rmmv.jpg");
	//
	// // Then read it in.
	// normalImage = ImageIO.read(in);
	//
	// in = Target.class.getClassLoader().getResourceAsStream("rmmv.jpg");
	//
	// damagedImage = ImageIO.read(in);
	// } catch (IOException e) {
	// System.out.println("The image was not loaded.");
	// normalImage = null;
	// damagedImage = null;
	// }
	// }

	public int[] getPos() {
		int[] returnArray = { this.pos.getX(), this.pos.getY(), this.pos.getZ() };

		return returnArray;
	}

	public synchronized int getX() {
		return pos.getX();
	}

	public synchronized int getY() {
		return pos.getY();
	}

	public synchronized int getZ() {
		return pos.getZ();
	}

	/**
	 * @return True if this instance has been destroyed, false otherwise.
	 */
	public boolean isDestroyed() {
		return destroyed;
	}

	public Vehicle.TerrainMedium getMedium() {
		return medium;
	}

	public String getName() {
		return name;
	}

	public ArrayList<Vehicle.Payload> getAvailablePayloads() {
		return availablePayloads;
	}

	public Vehicle getVehicleAssigned() {
		return vehicleAssigned;
	}

	public void assignVehicleToTarget(Vehicle vehicle) {
		this.vehicleAssigned = vehicle;
		// vehiclesAssigned.add(vehicle);
	}

	public void unassignVehicleFromTarget(Vehicle vehicle) {
		if (this.vehicleAssigned.getName() == vehicle.getName()) {
			vehicleAssigned = null;
		}
	}

	public void setDone() {
		done = true;
	}

	public void setAvailablePayloads(ArrayList<Vehicle.Payload> availablePayloads) {
		this.availablePayloads = availablePayloads;
	}

	public boolean isDone() {
		return done;
	}

	static public boolean isProperTargetMedium(String s) {
		return s.equals("land") || s.equals("water");
	}

	public void destroyTarget() {

		this.destroyed = true;
		// TODO Add in switch to Destroyed Image here - ARS - JULY 2015
	}

	/**
	 * @return the imageFileName
	 */
	public String getImageFileName() {

		String filename = (this.isDestroyed()) ? this.imageFileName + "Destroyed.jpg"
				: this.imageFileName + ".jpg";
		
		return filename;

	}
	
	/**
	 * @return the descriptive name for the target.
	 */
	public String getDescriptiveName() {

		
		return this.imageFileName;

	}
}
