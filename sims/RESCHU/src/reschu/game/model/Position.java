package reschu.game.model;


public class Position {
	
	private int mX;
	private int mY;
	private int mZ;
	
	
	
	
	/**
	 * @param X
	 * @param Y
	 * @param Z
	 */
	public Position(int X, int Y, int Z) {
		super();
		this.mX = X;
		this.mY = Y;
		this.mZ = Z;
	}
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Position [mX=" + mX + ", mY=" + mY + ", mZ=" + mZ + "]";
	}
	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + mX;
		result = prime * result + mY;
		result = prime * result + mZ;
		return result;
	}
	/** 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Position other = (Position) obj;
		if (mX != other.mX)
			return false;
		if (mY != other.mY)
			return false;
		if (mZ != other.mZ)
			return false;
		return true;
	}
	/**
	 * @return the X-coordinate
	 */
	public int getX() {
		return mX;
	}
	/**
	 * @param X the X-coordinate to set
	 */
	public void setX(int X) {
		this.mX = X;
	}
	/**
	 * @return the Y-coordinate
	 */
	public int getY() {
		return mY;
	}
	/**
	 * @param Y the Y-coordinate to set
	 */
	public void setY(int Y) {
		this.mY = Y;
	}
	/**
	 * @return the Z-coordinate
	 */
	public int getZ() {
		return mZ;
	}
	/**
	 * @param Z the Z-coordinate to set
	 */
	public void setZ(int Z) {
		this.mZ = Z;
	}
	
	public int[] toArray(){
		int[] positionArray = new int[3];
		positionArray[0] = this.mX;
		positionArray[1] = this.mY;
		positionArray[2] = this.mZ;
		
		return  positionArray;
		
	}
	

}
