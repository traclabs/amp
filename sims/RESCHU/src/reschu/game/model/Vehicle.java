package reschu.game.model;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.LinkedList;
import java.util.Random;

import reschu.constants.MyGame;
import reschu.constants.MySize;
import reschu.constants.MySpeed;
import reschu.game.controller.GUI_Listener;
import reschu.game.view.PanelMsgBoard;

public class Vehicle {
	/*
	 * final static public String TYPE_UAV = "UAV"; final static public String
	 * TYPE_UUV = "UUV"; final static public String PAYLOAD_ISR = "ISR"; final
	 * static public String PAYLOAD_COM = "COM";
	 */

	public enum VehicleClass {
		FIRESCOUT, GLOBALHAWK, TALISMAN, REAPER, SPARTANSCOUT, JDAM, KNIFEFISH, RMMV, PREDATOR, GENERIC_AV, GENERIC_SV, GENERIC_UV, TRITON, BLACKJACK
	}

	public enum TerrainMedium {
		AIR, WATER, LAND
	}

	public enum Payload {
		WEAPONS, INSPECT_CAM, SR_SENSOR, MR_SENSOR, LR_SENSOR, UISS, AIRBASE, SEABASE, SONAR, IR_SENSOR, SA_SENSOR, OPTICAL_SENSOR
	}

	private Game game;
	private ReschuMap reschuMap;

	final private String name;

	final private VehicleClass vehicleClass;

	public VehicleClass getVehicleClass() {

		return vehicleClass;
	}

	final private ArrayList<Payload> availablePayloads;
	final private TerrainMedium medium;

	private Payload activePayload;

	private Position currentPosition;
	private Target target;
	private LinkedList<int[]> path = new LinkedList<int[]>();

	private int index;
	private double velocityActual;
	private double velocityNominal;
	private GUI_Listener lsnr;
	private int status;
	private double vDamage;

	private int UUV_stuck_count;
	private boolean UUV_stuck;
	private boolean intersect;

	private boolean grounded;
	private int lastCommandStatus;

	public Vehicle(Game g, ReschuMap m, GUI_Listener l, int idx, String n,
			VehicleClass vclass, ArrayList<Payload> p, boolean ground) {

		game = g;
		reschuMap = m;
		lsnr = l;

		index = idx;

		name = n;

		/*
		 * Set Vehicle medium parameter based on vehicle class:
		 */
		vehicleClass = vclass;
		switch (vclass) {
		case FIRESCOUT:
		case GLOBALHAWK:
		case REAPER:
		case PREDATOR:
		case JDAM:
		case GENERIC_AV:
		case TRITON:
		case BLACKJACK:
			medium = TerrainMedium.AIR;
			break;
		case TALISMAN:
		case SPARTANSCOUT:
		case RMMV:
		case GENERIC_SV:
		case GENERIC_UV:
		case KNIFEFISH:
			medium = TerrainMedium.WATER;
			break;
		default:
			medium = TerrainMedium.WATER;
			System.out
					.println("error, vehicle class not recognized, should not be here");
			System.exit(1);
			break;
		}

		availablePayloads = p;
		activePayload = p.get(0);

		grounded = ground;

		velocityActual = 1;
		velocityNominal = 1;
		setStatus(MyGame.STATUS_VEHICLE_STASIS);
		vDamage = 0;
		UUV_stuck_count = 0;
		UUV_stuck = false;
		intersect = false;

		lastCommandStatus = MyGame.STATUS_VEHICLE_LAST_COMMAND_PENDING;

		currentPosition = new Position(0, 0, 0);

	}

	/**
	 * Set the position of this vehicle (synchronized)
	 */
	public synchronized void setPos(int x, int y) {
		setX(x);
		setY(y);
	}

	/**
	 * Get a path of this vehicle (synchronized)
	 */
	public synchronized LinkedList<int[]> getPath() {
		return path;
	}

	/**
	 * Add a waypoint to the path of this vehicle (synchronized)
	 */
	public synchronized void addPath(int idx, int[] e) {
		path.add(idx, e);
		setStatus(MyGame.STATUS_VEHICLE_MOVING);
	}

	/**
	 * Add a waypoint to the last path of this vehicle (synchronized)
	 */
	public synchronized void addPathLast(int[] e) {
		path.addLast(e);
		setStatus(MyGame.STATUS_VEHICLE_MOVING);
	}

	/**
	 * Set a path of this vehicle (synchronized)
	 */
	public synchronized void setPath(int idx, int[] e) {
		path.set(idx, e);
		setStatus(MyGame.STATUS_VEHICLE_MOVING);
	}

	/**
	 * Get the size of a path of this vehicle (synchronized)
	 */
	public synchronized int getPathSize() {
		return path.size();
	}

	/**
	 * Get a coordinate of a waypoint of this vehicle (synchronized)
	 */
	public synchronized int[] getPathAt(int idx) {
		return path.get(idx);
	}

	/**
	 * Remove a waypoint in the path of this vehicle (synchronized)
	 */
	public synchronized void removePathAt(int idx) {
		path.remove(idx);

		if (getStatus() == MyGame.STATUS_VEHICLE_MOVING && path.size() == 0) {

			setStatus(MyGame.STATUS_VEHICLE_STASIS);
		}
	}

	/**
	 * Get a coordinate at the first path of this vehicle (synchronized)
	 */
	public synchronized int[] getFirstPath() {
		return path.getFirst();
	}

	/**
	 * Get a coordinate at the last path of this vehicle (synchronized)
	 */
	public synchronized int[] getLastPath() {
		return path.getLast();
	}

	/**
	 * Remove the first waypoint of path of this vehicle (synchronized)
	 */
	public synchronized void removeFirstPath() {
		path.removeFirst();
	}

	/**
	 * Returns a reschuMap that this vehicle is assigned to
	 */
	public synchronized ReschuMap getReschuMap() {
		return reschuMap;
	}

	public synchronized void setX(int x) {
		this.currentPosition.setX(x);
	}

	public synchronized int getX() {
		return this.currentPosition.getX();
	}

	public synchronized void setY(int y) {
		this.currentPosition.setY(y);
	}

	public synchronized int getY() {
		return this.currentPosition.getY();
	}

	public synchronized void setZ(int z) {
		this.currentPosition.setZ(z);
	}

	public synchronized int getZ() {
		return this.currentPosition.getZ();
	}

	public String getName() {
		return name;
	}

	public TerrainMedium getMedium() {
		return medium;
	}

	public void setIndex(int idx) {
		index = idx;
	}

	public int getIndex() {
		return index;
	}

	public Payload getActivePayload() {
		return activePayload;
	}

	public ArrayList<Payload> getAvailablePayloads() {
		return this.availablePayloads;
	}

	public boolean setActivePayload(Payload candidatePayload) {
		boolean success = false;
		if (this.availablePayloads.contains(candidatePayload)) {
			this.activePayload = candidatePayload;
			success = true;
			System.out
					.println("Successfully switched Vehicle: " + this.getName()
							+ " Active Payload to " + candidatePayload);

		} else if ((this.medium == TerrainMedium.AIR && (candidatePayload == Payload.AIRBASE))) {
			this.availablePayloads.add(Payload.AIRBASE);
			this.activePayload = Payload.AIRBASE;
			System.out
					.println("Successfully added AIRBASE Payload to Vehicle: "
							+ this.getName());

		} else if ((this.medium == TerrainMedium.WATER && (candidatePayload == Payload.SEABASE))) {
			this.availablePayloads.add(Payload.SEABASE);
			this.activePayload = Payload.SEABASE;
			System.out
					.println("Successfully added SEABASE Payload to Vehicle: "
							+ this.getName());
		}

		else {
			System.out.println("Could not switch Vehicle: " + this.getName()
					+ " Active Payload to " + candidatePayload);
		}

		return success;
	}

	public void setVelocityNominal(double vel) {

		if (velocityActual == velocityNominal) {
			velocityActual = vel;
		}

		velocityNominal = vel;
	}

	public double getVelocityNominal() {
		return velocityNominal;
	}

	public double getVelocityActual() {
		return velocityActual;
	}

	public void setTarget(Target t) {
		target = t;
	}

	public Target getTarget() {
		return target;
	}

	public void setStatus(int i) {// STATUS_VEHICLE_STASIS = 0,
									// STATUS_VEHICLE_MOVING = 1,
									// STATUS_VEHICLE_PENDING = 2,
									// STATUS_VEHICLE_PAYLOAD = 3
		status = i;

		if (status == MyGame.STATUS_VEHICLE_STASIS) {
			velocityActual = 0;
		} else if (status == MyGame.STATUS_VEHICLE_MOVING) {
			velocityActual = velocityNominal;
		}
	}

	public int getStatus() {
		return status;
	}

	public void setIntersect(boolean b) {
		intersect = b;
	}

	public boolean getIntersect() {
		return intersect;
	}

	public boolean isGrounded() {

		return grounded;
	}

	public void ground() {
		setStatus(MyGame.STATUS_VEHICLE_STASIS);
		nullifyOwnGoal();
		path.clear();
		velocityActual = 0;
		grounded = true;
	}

	public void unground() {

		velocityActual = velocityNominal;

		grounded = false;
	}

	static public boolean isAProperVehMedium(String s) {

		return s.equals("air") || s.equals("water");
	}

	public double getDamage() {
		return vDamage;
	}

	@SuppressWarnings("unused")
	private boolean boundaryCheck(int x, int y, int[] target_pos) {

		int w = Math.round(MySize.SIZE_TARGET_PXL / MySize.SIZE_CELL / 2);
		return (x <= target_pos[0] + w) && (x >= target_pos[0] - w)
				&& (y <= target_pos[1] + w) && (y >= target_pos[1] - w);
	}

	private double calculateDistanceBetween(int x1, int y1, int z1, int x2,
			int y2, int z2) {

		double distanceBetween = 0.0f;

		distanceBetween = Math.sqrt(Math.pow((Math.abs(x1 - x2)), 2)
				+ (Math.pow((Math.abs(y1 - y2)), 2))
				+ (Math.pow((Math.abs(z1 - z2)), 2)));

		return distanceBetween;

	}

	/**
	 * @return This function returns true if a vehicle has an assigned Target,
	 *         false otherwise
	 */
	public boolean isAssignededTarget(int x, int y) {

		if (this.target != null) {
			if ((target.getX() == x) && (target.getY() == y)) {
				return true;
			}
			return false;
		}


		return false;
	}

	public boolean isAssignedATarget() {
		return (this.target == null);
	}

	public void addGoal(int x, int y, int z) {
		int commandStatus = (setGoal(x, y, z, null)) ? MyGame.STATUS_VEHICLE_LAST_COMMAND_SUCCESSFUL
				: MyGame.STATUS_VEHICLE_LAST_COMMAND_FAILED;
		setLastCommandStatus(commandStatus);
		setStatus(MyGame.STATUS_VEHICLE_MOVING);
	}

	public void changeGoal(int[] ex_goal, int x, int y, int z) {

		int commandStatus = (setGoal(x, y, z, ex_goal)) ? MyGame.STATUS_VEHICLE_LAST_COMMAND_SUCCESSFUL
				: MyGame.STATUS_VEHICLE_LAST_COMMAND_FAILED;
		setLastCommandStatus(commandStatus);
		setStatus(MyGame.STATUS_VEHICLE_MOVING);
	}

	public boolean setGoal(int x, int y, int z, int[] ex_goal) {

		
		@SuppressWarnings("unused")
		int[] target_pos;
		Target t;
		boolean assigned = false;

		Target formerGoal = target;

		ArrayList<Target> unassignedTargets = getReschuMap().getTargetList();
		boolean found = false;
		int i = 0;

		double distance = 999999999;
		double currentDistance = 0;
		Target closestTarget = null;

		// Go through Targets in existance and find the Target located closest
		// to the argument x, y, and z coordinates:

		while ((!found) && (i < unassignedTargets.size())) {
			setTarget(null);
			t = unassignedTargets.get(i);
			target_pos = t.getPos();

			// Calculate distance between the passed in coordinates and the
			// current target's position:
			// NOTE: The Z coordinate is HARDCODED to 0 for now. This needs to
			// be fixed up the call stack.

			currentDistance = this.calculateDistanceBetween(x, y, z, t.getX(),
					t.getY(), t.getZ());

			if (currentDistance < distance) {
				distance = currentDistance;
				closestTarget = t;
			}
			i++;
		}

		if (closestTarget != null) {
			// if (boundaryCheck(x, y, target_pos)) {

			// Check that Vehicle's Active Payload is one of the Target's
			// available Payloads:
			if (closestTarget.getAvailablePayloads().contains(
					this.activePayload)) {
				// Check that the Vehicle Medium and Target Medium "match":
				if (!(this.medium.equals(closestTarget.getMedium()))) {
					// If this is a water Vehicle:
					if (this.medium.equals(TerrainMedium.WATER)) {
						lsnr.showMessageOnTopOfMap(
								"A water-based Vehicle cannot be assigned to a land-based or air-based Target",
								5);
						return false;
					} // If this is a land Vehicle
					else if (this.medium.equals(TerrainMedium.LAND)) {
						if (closestTarget.getMedium().equals(
								TerrainMedium.WATER)) {
							lsnr.showMessageOnTopOfMap(
									"A Land-based Vehicle cannot be assigned to a water-based Target",
									5);
							return false;
						}
					}
				}

				x = closestTarget.getX();
				// target_pos[0];
				y = closestTarget.getY();
				// target_pos[1];
				if (ex_goal != null) {
					nullifyGoal(ex_goal);
				}
				setTarget(closestTarget);
				// PanelMsgBoard.Msg("Vehicle ["+index+"] is assigned to a target medium ["+target.getMedium()+"]");
				PanelMsgBoard.Msg("Vehicle [" + index
						+ "] has been assigned to a target.");
				getReschuMap().assignTarget(x, y, this, closestTarget);

				assigned = true;
				found = true;

			} else {

				// Check if Vehicle has any payloads that match the
				// Target's:
				ArrayList<Payload> usablePayloads = new ArrayList<Payload>();

				for (Payload pay : this.availablePayloads) {
					if (closestTarget.getAvailablePayloads().contains(pay)) {
						usablePayloads.add(pay);
					}
				}
				// if any usable payloads were found on this Vehicle for the
				// given Target t:
				if (usablePayloads.size() > 0) {
					lsnr.showMessageOnTopOfMap(
							"This Vehicle's active payload, "
									+ this.activePayload
									+ " cannot be used on the Target, "
									+ closestTarget.getName()
									+ ". This Vehicle can use any of the following payloads on this target: "
									+ this.availablePayloads.toString(), 5);
				} else {
					setTarget(closestTarget);
					// PanelMsgBoard.Msg("Vehicle ["+index+"] is assigned to a target medium ["+target.getMedium()+"]");
					PanelMsgBoard.Msg("Vehicle [" + index
							+ "] has been assigned to a target.");
					getReschuMap().assignTarget(x, y, this, closestTarget);

					assigned = true;
					found = true;
				}
			}

			if (ex_goal != null) {// change goal
				if (assigned) {
					setPath(getPathSize() - 1, new int[] { x, y });
				} else {
					setTarget(formerGoal);
				}
				if (game.getElapsedTime() != 0) {
					if (assigned)
						lsnr.EVT_GP_ChangeGP_End_Assigned(index, x, y,
								getTarget().getName());
					else
						lsnr.EVT_GP_ChangeGP_End_Unassigned(index, x, y);
				}
			} else {// add goal
				if (assigned) {
					addPathLast(new int[] { x, y });
				}
				if (game.getElapsedTime() != 0) {
					if (assigned)
						lsnr.EVT_GP_SetGP_End_Assigned(index, x, y, getTarget()
								.getName());
					else
						lsnr.EVT_GP_SetGP_End_Unassigned(index, x, y);
				}
			}
			return assigned;
		} else {// no closest Target found:
			return false;

		}
	}

	private void nullifyGoal(int[] ex_goal) {
		int[] ex_target_pos;
		ArrayList<Target> assignedTargets = getReschuMap().getTargetList();
		for (int i = 0; i < assignedTargets.size(); i++) {
			ex_target_pos = assignedTargets.get(i).getPos();

			if (ex_goal[0] == ex_target_pos[0]
					&& ex_goal[1] == ex_target_pos[1]) {

				getReschuMap().unassignTarget(ex_target_pos[0],
						ex_target_pos[1], this);
				setTarget(null);
				break;
			}
		}
	}

	public void nullifyOwnGoal() {
		int[] position;

		if (this.getTarget() == null) {
			position = this.currentPosition.toArray();
		} else {
			position = this.getTarget().getPos();
		}
		nullifyGoal(position);

		if (this.getStatus() == 2) {
			this.setStatus(0);
		}
	}

	public synchronized int addWaypoint(int x, int y) {

		int idx = 0;

		if (getPathSize() != 0) {
			double distance = 9999999; // infinite
			double d = Game.getDistance(getX(), getY(), x, y)
					+ Game.getDistance(getPathAt(0)[0], getPathAt(0)[1], x, y);
			if (d < distance)
				distance = d;

			for (int i = 0; i < getPathSize() - 1; i++) {
				d = Game.getDistance(getPathAt(i)[0], getPathAt(i)[1], x, y)
						+ Game.getDistance(getPathAt(i + 1)[0],
								getPathAt(i + 1)[1], x, y);
				if (d < distance) {
					distance = d;
					idx = i + 1;
				}
			}

		}

		if (x > 0 && x < MySize.width && y > 0 && y < MySize.height)
			addPath(idx, new int[] { x, y });
		return idx;
	}

	public synchronized int addWaypoint(int x, int y, int idx) {
		addPath(idx, new int[] { x, y });

		return idx;
	}

	public boolean delWaypoint(int x, int y) {
		for (int i = 0; i < getPathSize(); i++) {
			if (getPathAt(i)[0] == x && getPathAt(i)[1] == y) {
				removePathAt(i);

				// new addition by traclabs, changed this method to return true
				// if success, false otherwise
				return true;
			}
		}
		return false;
	}

	public void delWaypoint(int[] coordinate) {
		for (int i = 0; i < getPathSize() - 1; i++)
			if (getPathAt(i) == coordinate) {
				removePathAt(i);
			}
	}

	public void changeWaypoint(int ex_x, int ex_y, int new_x, int new_y) {
		if (new_x < 0 || new_x > MySize.width || new_y < 0
				|| new_y > MySize.height)
			return;

		for (int i = 0; i < getPathSize() - 1; i++)
			if (getPathAt(i)[0] == ex_x && getPathAt(i)[1] == ex_y) {
				getPathAt(i)[0] = new_x;
				getPathAt(i)[1] = new_y;
			}
	}

	public boolean hasGoal() {
		return getPathSize() != 0;
	}

	public boolean hasWaypoint() {
		return getPathSize() - 1 != 0;
	}

	// Moving Algorithms
	public double getDistanceToFirst(double pos_x, double pos_y) {
		if (getPathSize() == 0)
			return 0;
		return Math.sqrt(Math.pow(pos_x - getFirstPath()[0], 2.0)
				+ Math.pow(pos_y - getFirstPath()[1], 2.0));
	}

	public void moveHillClimbing() {
		if (UUV_stuck) {
			moveTo(6, 1);
			if (--UUV_stuck_count <= 0)
				UUV_stuck = false;
			return;
		}

		double presentDistance = getDistanceToFirst(getX(), getY());
		double distance = 999999999;
		Random rnd = new Random();
		int direction = 8;
		boolean stuck = true;

		double vel = velocityActual;

		for (int i = 0; i < 8; i++) {
			direction = rnd.nextInt(8);

			switch (direction) {
			case 0:
				distance = getDistanceToFirst(getX() - vel, getY() - vel);
				break;
			case 1:
				distance = getDistanceToFirst(getX() - vel, getY());
				break;
			case 2:
				distance = getDistanceToFirst(getX() - vel, getY() + vel);
				break;
			case 3:
				distance = getDistanceToFirst(getX(), getY() - vel);
				break;
			case 4:
				distance = getDistanceToFirst(getX(), getY() + vel);
				break;
			case 5:
				distance = getDistanceToFirst(getX() + vel, getY() - vel);
				break;
			case 6:
				distance = getDistanceToFirst(getX() + vel, getY());
				break;
			case 7:
				distance = getDistanceToFirst(getX() + vel, getY() + vel);
				break;
			}
			if (distance < presentDistance && chkValidMove(direction, vel)) {
				stuck = false;
				break;
			}
		}
		if (stuck) {
			UUV_stuck_count++;
			if (UUV_stuck_count >= 5)
				UUV_stuck = true;
		} else
			moveTo(direction, vel);
	}

	/*
	 * public void moveTo(int direction){ moveTo(direction,1); }
	 */

	public void moveTo(int direction, double doublevelocity) {

		if (isGrounded()) {
			return;
		}

		// int x=getX();
		// int y=getY();
		int velocityint = (int) Math.round(doublevelocity);
		switch (direction) {
		case 0: // up-left
			setX(getX() - velocityint);
			setY(getY() - velocityint);
			break;
		case 1: // up
			setX(getX() - velocityint);
			break;
		case 2: // up-right
			setX(getX() - velocityint);
			setY(getY() + velocityint);
			break;
		case 3: // left
			setY(getY() - velocityint);
			break;
		case 4: // right
			setY(getY() + velocityint);
			break;
		case 5: // down-left
			setX(getX() + velocityint);
			setY(getY() - velocityint);
			break;
		case 6: // down
			setX(getX() + velocityint);
			break;
		case 7: // down-right
			setX(getX() + velocityint);
			setY(getY() + velocityint);
			break;
		default:
			break;
		}

		if (getDistanceToFirst(getX(), getY()) <= 3) {
			if (velocityActual != 1) {
				velocityActual = 1;
				String msg = "Vehicle ["
						+ index
						+ "] is slowing down to velocityActual=1 in order to engage.";
				PanelMsgBoard.Msg(msg);
			}
		} else {
			if (velocityActual != velocityNominal) {
				velocityActual = velocityNominal;
			}
		}

		payloadCheck(getX(), getY());
	}

	private void payloadCheck(int pos_x, int pos_y) {
		if (getPathSize() != 0
				&& (pos_x == getFirstPath()[0] && pos_y == getFirstPath()[1])) {
			if (getPathSize() == 1 && target != null) {
				// VEHICLE ARRIVED TO ITS GOAL WHERE THE PLACE IS THE ONE OF
				// UNASSIGNED_TARGETS

				setStatus(MyGame.STATUS_VEHICLE_PENDING);
				this.lastCommandStatus = MyGame.STATUS_VEHICLE_LAST_COMMAND_PENDING;
				String msg = "Vehicle [" + index + "] has reached its target.";
				PanelMsgBoard.Msg(msg);

				lsnr.EVT_Vehicle_ArrivesToTarget(index, getTarget().getName(),
						getTarget().getPos()[0], getTarget().getPos()[1]);
			}
			lsnr.Hide_Popup(this);
			removeFirstPath();
		}
	}

	public void setLastCommandStatus(int lastCommandStatus) {
		this.lastCommandStatus = lastCommandStatus;
	}

	public boolean vPayloadContainsScanSensors() {

		return (this.availablePayloads.contains(Payload.LR_SENSOR));

	}

	public boolean vPayloadContainsInspectCam() {
		return (this.availablePayloads.contains(Payload.INSPECT_CAM));
	}

	public boolean vHasPayloadForTarget(Payload payload) {

		return (this.availablePayloads.contains(payload));

	}

	public EnumSet<Payload> vHasPayloadForTarget(EnumSet<Payload> payloads) {
		EnumSet<Payload> matchedPayloads = EnumSet.noneOf(Payload.class);

		for (Payload pay : payloads) {
			if (this.availablePayloads.contains(pay)) {
				matchedPayloads.add(pay);
			}
		}

		return matchedPayloads;

	}

	public synchronized boolean chkValidMove(int direction, double vel) {
		switch (direction) {
		case 0: // up-left
			return (getX() > 0 && getY() > 0)
					&& (chkValidPosition(getX() - (int) vel, getY() - (int) vel));
		case 1: // up
			return (getX() > 0)
					&& (chkValidPosition(getX() - (int) vel, getY()));
		case 2: // up-right
			return (getX() > 0 && getY() < MySize.height - 1)
					&& (chkValidPosition(getX() - (int) vel, getY() + (int) vel));
		case 3: // left
			return (getY() > 0)
					&& (chkValidPosition(getX(), getY() - (int) vel));
		case 4: // right
			return (getY() < MySize.height - 1)
					&& (chkValidPosition(getX(), getY() + (int) vel));
		case 5: // down-left
			return (getX() < MySize.width - 1 && getY() > 0)
					&& (chkValidPosition(getX() + (int) vel, getY() - (int) vel));
		case 6: // down
			return (getX() < MySize.width - 1)
					&& (chkValidPosition(getX() + (int) vel, getY()));
		case 7: // down-right
			return (getX() < MySize.width - 1 && getY() < MySize.height - 1)
					&& (chkValidPosition(getX() + (int) vel, getY() + (int) vel));
		default:
			return false;
		}
	}

	public synchronized boolean chkValidPosition(int width, int height) {
		if (medium == TerrainMedium.WATER) {
			if (getReschuMap().getCellType(width, height) == MyGame.LAND)
				return false;
		}
		return true;
	}

	public void chkHazardArea() {
		int damage = 0;
		double d;
		int[] hazard_pos;
		for (int i = 0; i < reschuMap.getListHazard().size(); i++) {
			hazard_pos = reschuMap.getListHazard().get(i);
			d = Math.sqrt(Math.pow(
					(double) (this.currentPosition.getX() - hazard_pos[0]), 2.0)
					+ Math.pow(
							(double) (this.currentPosition.getY() - hazard_pos[1]),
							2.0))
					* MySize.SIZE_CELL;
			if (d <= MySize.SIZE_HAZARD_1_PXL)
				damage += 50;
			else if (d < MySize.SIZE_HAZARD_2_PXL
					&& d > MySize.SIZE_HAZARD_1_PXL)
				damage += 30;
			else
				damage += 0;

			if (d < 50d && d > 45d)
				lsnr.EVT_Vehicle_Damaged(getIndex(), hazard_pos[0],
						hazard_pos[1]);

		}
		// We don't decrease the speed of a vehicle anymore
		// setBuffer(damage);
		vDamage += (double) (damage) / 100;
		lsnr.Vehicle_Damaged_By_Hazard_Area_From_Vehicle(this);
	}

	// the following methods put in by traclabs
	public int calcTimeToTarget() {
		// ttt is not a field, but is calculated. the GUI does it with
		// distance/velocityActual in the VehicleTime class.
		// the code actually calculates time to target with
		// "distance*velocityActual". this is because in the
		// code, velocityActual is not distance/time like what reasonable human
		// beings hold to, but rather the code's
		// definition of velocityActual is time/distance. the velocityActual
		// field variable is an int which is a certain amount
		// of "milliseconds". Idk what the standard distance unit being used
		// here is.

		// above comment is outdated, i changed it so it's better now

		int time = 0;
		for (int j = 0; j < this.getPathSize(); j++) {

			int temp = (int) Math
					.round(getDist(this.getPath().get(j)[0], this.getPath()
							.get(j)[1], this.getX(), this.getY())
							/ (getVelocityActual() / ((double) MySpeed.SPEED_TIMER / 1000)));

			time = time + temp;
		}

		return time;
	}

	// this method is taken straight from PanelTimeLine.java, over there it is
	// called simply "getD"
	private double getDist(int x1, int y1, int x2, int y2) {
		return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
	}

	public int getUUV_stuck_count() {
		return UUV_stuck_count;
	}

	public boolean isUUV_stuck() {
		return UUV_stuck;
	}

	public static Payload stringToPayload(String payload, boolean allowBases)
			throws Exception {

		System.out.println(payload);

		if (payload.equals("weapons")) {
			return Payload.WEAPONS;
		} else if (payload.equals("inspectcam")) {
			return Payload.INSPECT_CAM;
		} else if (payload.equals("SRsensor")) {
			return Payload.SR_SENSOR;
		} else if (payload.equals("MRsensor")) {
			return Payload.MR_SENSOR;
		} else if (payload.equals("LRsensor")) {
			return Payload.LR_SENSOR;
		} else if (payload.equals("uiss")) {
			return Payload.UISS;
		} else if (payload.equals("IRsensor")) {
			return Payload.IR_SENSOR;
		} else if (payload.equals("SAsensor")) {
			return Payload.SA_SENSOR;
		} else if (payload.equals("Opticalsensor")) {
			return Payload.OPTICAL_SENSOR;
		} else if (payload.equals("sonar")) {
			return Payload.SONAR;
		} else if (allowBases) {
			if (payload.equals("airbase")) {
				return Payload.AIRBASE;
			} else if (payload.equals("seabase")) {
				return Payload.SEABASE;
			}
		}

		throw new Exception();
	}

	public int getLastCommandStatus() {
		return this.lastCommandStatus;
	}
}
