package reschu.game.model;

import java.beans.PropertyChangeEvent;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Random;

import reschu.constants.MyGame;
import reschu.constants.MySize;
import reschu.game.controller.GUI_Listener;
import reschu.game.controller.Reschu;
import reschu.game.model.Vehicle.Payload;
import reschu.game.utils.FileReader;

import com.traclabs.amp.reschu.TargetsObserver;
import com.traclabs.amp.reschu.MapHandlers.HazardsObserver;

public class ReschuMap {
	private Game g;
	private GUI_Listener lsnr;

	private int[][] map_array;
	private Map<String, Target> targetsMap = new HashMap<String, Target>();
	// private Map<String, Target> assignedTargetMap = new HashMap<String,
	// Target>();
	// private Map<String, Target> unassignedTargetMap = new HashMap<String,
	// Target>();
	private LinkedList<int[]> listHazard = new LinkedList<int[]>();
	private TargetsObserver targetsObserver;
	private Map<String, DestroyedTarget> destroyedTargetMap = new HashMap<String, DestroyedTarget>();

	// private final LinkedList<Target> listTargets=new LinkedList<Target>();

	ReschuMap() {
	}

	ReschuMap(int width, int height, Game g, GUI_Listener l) {
		map_array = new int[width][height];
		this.g = g;
		lsnr = l;

	}

	// following method added by traclabs
	public int[][] getMap_array() {
		return map_array;
	}

	// public synchronized ArrayList<Target> getListAssignedTarget() {
	// return new ArrayList<Target>(assignedTargetMap.values());
	// }
	//
	// public synchronized ArrayList<Target> getListUnassignedTarget() {
	// return new ArrayList<Target>(unassignedTargetMap.values());
	// }

	public synchronized ArrayList<Target> getTargetList() {
		return new ArrayList<Target>(targetsMap.values());
	}

	public synchronized ArrayList<DestroyedTarget> getListDestroyedTarget() {
		return new ArrayList<DestroyedTarget>(destroyedTargetMap.values());
	}

	// public synchronized Map<String, Target> getMapAssignedTarget() {
	// return assignedTargetMap;
	// }
	//
	// public synchronized Map<String, Target> getMapUnassignedTarget() {
	// return unassignedTargetMap;
	// }

	public synchronized Map<String, Target> getTargetMap() {
		return targetsMap;
	}

	public synchronized Target getTargetByName(String name) {

		if (targetsMap.containsKey(name)) {
			return targetsMap.get(name);
		}

		return null;

	}

	public LinkedList<int[]> getListHazard() {
		return listHazard;
	}

	public int getTargetTypeCount(Vehicle.TerrainMedium terrain,
			Vehicle.Payload payload) {
		int cnt = 0;
		ArrayList<Target> currentTargetList = this.getTargetList();
		Target tempTarget = null;

		for (int i = 0; i < currentTargetList.size(); i++) {
			tempTarget = currentTargetList.get(i);
			if (tempTarget.getMedium() == terrain
					&& tempTarget.getAvailablePayloads().contains(payload)) {
				cnt++;
			}
		}
		return cnt;
	}

	public int getTargetTypeCount(Vehicle.Payload payload) {
		int cnt = 0;
		ArrayList<Target> currentTargetList = this.getTargetList();
		Target tempTarget = null;

		for (int i = 0; i < currentTargetList.size(); i++) {
			tempTarget = currentTargetList.get(i);

			if (tempTarget.getAvailablePayloads().contains(payload)) {
				cnt++;
			}
		}

		return cnt;
	}

	public int getTotalTargets() {

		return getTargetList().size();

	}

	public synchronized void setCellType(int x, int y, int type) {
		map_array[x][y] = type;
	}

	public synchronized int getCellType(int x, int y) {
		return map_array[x][y];
	}

	public synchronized void addHazard(int x, int y, int size) {
		if (g.isRunning())
			lsnr.EVT_HazardArea_Generated(new int[] { x, y });
		listHazard.addLast(new int[] { x, y, size });
	}

	public synchronized void assignTarget(int x, int y, Vehicle vehicle,
			Target t) {

		t.assignVehicleToTarget(vehicle);

		synchronized (targetsMap) {

			targetsObserver.propertyChange(new PropertyChangeEvent(this,
					"targets", null, targetsMap));
		}
	}

	public synchronized void unassignTarget(int x, int y, Vehicle vehicle) {
		Target currentTarget = vehicle.getTarget();

		vehicle.setTarget(null);

		if (currentTarget != null) {
			currentTarget.assignVehicleToTarget(vehicle);
		}

		synchronized (targetsMap) {
			targetsObserver.propertyChange(new PropertyChangeEvent(this,
					"targets", null, targetsMap));

		}

	}

	public synchronized boolean isTarget(int x, int y) {
		ArrayList<Target> targets = getTargetList();
		for (Target currentTarget : targets) {
			if ((currentTarget.getX() == x) && (currentTarget.getY() == y)) {
				return true;
			}
		}

		return false;
	}

	public synchronized boolean isHazard(int x, int y) {
		for (int i = 0; i < listHazard.size(); i++) {
			if (listHazard.get(i)[0] == x && listHazard.get(i)[1] == y)
				return true;
		}
		return false;
	}

	@SuppressWarnings("unused")
	private boolean boundaryCheck(int x, int y, int[] target_pos) {
		int w = Math.round(MySize.SIZE_TARGET_PXL / MySize.SIZE_CELL / 2);

		for (int i = -w; i < w; i++)
			for (int j = -w; j < w; j++)
				if (x + i == target_pos[0] && y + j == target_pos[1])
					return true;
		return false;
	}

	public void setHazardArea(Random rnd) {
		int nHazard = (Reschu.tutorial()) ? MyGame.nHAZARD_AREA_TUTORIAL
				: MyGame.nHAZARD_AREA;
		int nHazardNeed = nHazard - getListHazard().size();
		int x, y;

		for (int i = 0; i < nHazardNeed; i++) {
			while (!chkOkayToAdd((x = rnd.nextInt(MySize.width)),
					(y = rnd.nextInt(MySize.height)))) {
			}

			addHazard(x, y, rnd.nextInt(5));

		}

		// notifyListeners(this, "",null, listHazard);
		hazardsObserver.propertyChange(new PropertyChangeEvent(this, "", null,
				listHazard));
	}

	private HazardsObserver hazardsObserver;

	public void addHazardsObserver(HazardsObserver newListener) {
		// listener.add(newListener);
		hazardsObserver = newListener;
	}

	private boolean chkOkayToAdd(int x, int y) {
		final int LIMIT_DISTANCE = 40;
		final boolean DEBUG = false;

		ArrayList<Target> targets = getTargetList();

		for (int i = 0; i < targets.size(); i++) {
			Target t = targets.get(i);
			if (Game.getDistance(t.getPos()[0], t.getPos()[1], x, y) < LIMIT_DISTANCE) {
				int x1 = t.getPos()[0], y1 = t.getPos()[1], x2 = x, y2 = y;
				double d = Game.getDistance(t.getPos()[0], t.getPos()[1], x, y);
				if (DEBUG)
					System.err.println("LAT (" + x1 + "," + y1
							+ ") with randomly generated (" + x2 + "," + y2
							+ "), d=(" + d + ")");
				return false;
			}
		}

		for (int i = 0; i < listHazard.size(); i++) {
			if (Game.getDistance(x, y, listHazard.get(i)[0],
					listHazard.get(i)[1]) < LIMIT_DISTANCE) {
				int x1 = listHazard.get(i)[0], y1 = listHazard.get(i)[1], x2 = x, y2 = y;
				double d = Game.getDistance(x, y, listHazard.get(i)[0],
						listHazard.get(i)[1]);
				if (DEBUG)
					System.err.println("LHA(" + x1 + "," + y1
							+ ") with randomly generated (" + x2 + "," + y2
							+ "), d=(" + d + ")");
				return false;
			}
		}
		for (int i = 0; i < g.getVehicleList().size(); i++) {
			Vehicle v = g.getVehicleList().getVehicle(i);
			if (Game.getDistance(v.getX(), v.getY(), x, y) < LIMIT_DISTANCE) {
				int x1 = v.getX(), y1 = v.getY(), x2 = x, y2 = y;
				double d = Game.getDistance(v.getX(), v.getY(), x, y);
				if (DEBUG)
					System.err.println("LVH(" + x1 + "," + y1
							+ ") with randomly generated (" + x2 + "," + y2
							+ "), d=(" + d + ")");
				return false;
			}
		}
		return true;
	}

	public void delHazardArea(Random rnd, int n) {
		for (int i = 0; i < n; i++) {
			int idx = rnd.nextInt(getListHazard().size());
			synchronized (this) {
				lsnr.EVT_HazardArea_Disappeared(new int[] {
						listHazard.get(idx)[0], listHazard.get(idx)[1] });
				listHazard.remove(idx);
			}
		}

		hazardsObserver.propertyChange(new PropertyChangeEvent(this, "", null,
				listHazard));
	}

	public synchronized void addTarget(Target t) {
		if (g.isRunning())
			lsnr.EVT_Target_Generated(t.getName(), t.getPos(), t
					.getAvailablePayloads().contains(Vehicle.Payload.LR_SENSOR));

		synchronized (targetsMap) {
			targetsMap.put(t.getName(), t);
		}
	}

	public synchronized void addTarget(Target t, boolean visible) {
		if (g.isRunning())
			lsnr.EVT_Target_Generated(t.getName(), t.getPos(), visible);

		synchronized (targetsMap) {
			targetsMap.put(t.getName(), t);
		}
	}

	public synchronized boolean removeTarget(String targetName) {
		// find target in list:

		Target tempTarget = null;

		// check unassigned list:
		if (targetsMap.containsKey(targetName)) {
			tempTarget = targetsMap.get(targetName);
		}

		if (tempTarget != null) {
			// remove target from list:
			if (g.isRunning())
				lsnr.EVT_Target_Disappeared(tempTarget.getName(),
						tempTarget.getPos());

			synchronized (targetsMap) {
				targetsMap.remove(tempTarget.getName());
			}
		} else {
			return false;
		}
		this.g.setTargetUsed(tempTarget.getName(), false);
		return true;
	}

	public void setTargetAreaFromFile() {

		FileReader fr = null;
		try {
			URL url = Game.class.getClassLoader().getResource(
					"config_initial_targets.dat");
			fr = new FileReader(url);
			fr.openFile();
		} catch (Exception e) {
			System.out.println("load scenarios data error");
			e.printStackTrace();
			System.exit(-1);
		}

		String aLine;
		@SuppressWarnings("unused")
		int lineNum = -1, idx;
		while ((aLine = fr.readLineByLine()) != null) {

			lineNum++;
			String[] line = aLine.split(" ");

			if (line.length == 5) {

				Vehicle.Payload intendedPayload;
				int x;
				int y;
				String name, imageName;

				try {
					intendedPayload = Vehicle.stringToPayload(line[0], true);
				} catch (Exception e) {
					System.err
							.println("ERROR: Failed to load target at line ("
									+ lineNum
									+ "), intended payload argument misformated or non-existent.  program exiting");
					System.exit(0);
					return;
				}
				try {
					x = Integer.parseInt(line[1]);
				} catch (NumberFormatException e) {
					System.err
							.println("ERROR: Failed to load target at line ("
									+ lineNum
									+ "), x position argument misformated or non-existent.  program exiting");
					System.exit(0);
					return;
				}
				try {
					y = Integer.parseInt(line[2]);
				} catch (NumberFormatException e) {
					System.err
							.println("ERROR: Failed to load target at line ("
									+ lineNum
									+ "), y position argument misformated or non-existent.  program exiting");
					System.exit(0);
					return;
				}
				try {
					name = line[3];
					if (!g.checkTargetNameOKThenMarkAsUsed(name)) {
						throw new Exception();

					}
				} catch (Exception e) {
					System.err
							.println("ERROR: Failed to load target at line ("
									+ lineNum
									+ "), name is bad or already used.  program exiting");
					System.exit(0);
					return;
				}

				imageName = line[4];

				addTarget(new Target(name, imageName, intendedPayload,
						chkTargetOffset(x, y), g));

			} else {
				System.err
						.println("ERROR: Failed to load target at line ("
								+ lineNum
								+ "), check to make sure there are a correct number of args.  program exiting");
				System.exit(0);
			}
		}

		fr.closeFile();

	}

	public void setTargetAreaRandomly(Random rnd) {

		for (int i = 0; i < MyGame.nTOTAL_TARGETS/*-getTotalTargets()*/; i++) {

			int roll = (int) (Math.random() * ((9) + 1));
			String name = g.getEmptyTargetName();
			switch (roll) {
			case 0:
			// weapons
			{
				int x, y;
				do {
					x = rnd.nextInt(MySize.width);
					y = rnd.nextInt(MySize.height);
				} while (!chkOkayToAdd(x, y));

				addTarget(new Target(name, name, Vehicle.Payload.WEAPONS,
						chkTargetOffset(x, y), g));

				break;
			}
			case 1:
			case 2: {
				// close inspect land
				int x, y;
				do {
					x = rnd.nextInt(MySize.width);
					y = rnd.nextInt(MySize.height);
				} while (!(getCellType(x, y) == MyGame.LAND)
						|| !chkOkayToAdd(x, y));

				addTarget(new Target(name, name, Vehicle.Payload.INSPECT_CAM,
						chkTargetOffset(x, y), g));
				break;
			}
			case 3: {// close inspect seashore
				int x, y;
				do {
					x = rnd.nextInt(MySize.width);
					y = rnd.nextInt(MySize.height);
				} while (!(getCellType(x, y) == MyGame.SEASHORE)
						|| !chkOkayToAdd(x, y)); // @change yale 2008-06-29 ||
													// !chkOkayToAdd(x,y) );

				addTarget(new Target(name, name, Vehicle.Payload.INSPECT_CAM,
						chkTargetOffset(x, y), g));

				break;
			}
			case 4:

			case 5:
			// monitor land
			{
				int x, y;
				do {
					x = rnd.nextInt(MySize.width);
					y = rnd.nextInt(MySize.height);
				} while (!(getCellType(x, y) == MyGame.LAND)
						|| !chkOkayToAdd(x, y));

				addTarget(new Target(name, name, Vehicle.Payload.SR_SENSOR,
						chkTargetOffset(x, y), g));
				break;
			}
			case 6:
			// monitor seashore
			{
				int x, y;
				do {
					x = rnd.nextInt(MySize.width);
					y = rnd.nextInt(MySize.height);
				} while (!(getCellType(x, y) == MyGame.SEASHORE)
						|| !chkOkayToAdd(x, y));

				addTarget(new Target(name, name, Vehicle.Payload.SR_SENSOR,
						chkTargetOffset(x, y), g));
				break;
			}
			case 7:
			// monitor sea
			{
				int x, y;
				do {
					x = rnd.nextInt(MySize.width);
					y = rnd.nextInt(MySize.height);
				} while (!(getCellType(x, y) == MyGame.SEA)
						|| !chkOkayToAdd(x, y));

				addTarget(new Target(name, name, Vehicle.Payload.SR_SENSOR,
						chkTargetOffset(x, y), g));
				break;
			}
			case 8:

			case 9: {// surveil
				int x, y;
				do {
					x = rnd.nextInt(MySize.width);
					y = rnd.nextInt(MySize.height);
				} while (!(getCellType(x, y) != MyGame.SEASHORE && chkOkayToAdd(
						x, y)));

				addTarget(new Target(name, name, Vehicle.Payload.LR_SENSOR,
						chkTargetOffset(x, y), g));

				break;
			}

			default:
				System.out
						.println("switch case in reschumap hit the default case, should not happen");
				break;

			}

		}
		targetsObserver.propertyChange(new PropertyChangeEvent(this, "targets",
				"adding", targetsMap));

	}

	public void addTargetsObserver(TargetsObserver newListener) {
		targetsObserver = newListener;
	}

	public int[] chkTargetOffset(int x, int y) { // this method just makes sure
													// that part of the target
													// isn't hanging off the map
		int offset = 10;
		if (x < offset)
			x += offset;
		if (y < offset)
			y += offset;
		if (x > MySize.width - offset)
			x -= offset;
		if (y > MySize.height - offset)

			y -= offset;
		return new int[] { x, y };
	}

	// FIXME ARS NOW!!!
	public void garbageTargetCollectForWeapons(String destroyedTargetName) {
		int x, y;
		Vehicle.Payload intendedPayload = Vehicle.Payload.INSPECT_CAM;
		String observationTargetName = g.getEmptyTargetName();

		synchronized (getTargetList()) {
			// Remove 'done' target:
			Target t = this.targetsMap.get(destroyedTargetName);

			// store off destroyed Target's position:
			x = t.getPos()[0];
			y = t.getPos()[1];

			if (t.isDone()) {

				lsnr.EVT_Target_Disappeared(t.getName(), t.getPos());
				g.setTargetUsed(t.getName(), false);
				this.targetsMap.remove(destroyedTargetName);
				DestroyedTarget desTarg = new DestroyedTarget(t,
						observationTargetName,
						t.getVehicleAssigned().getName(), g, Payload.WEAPONS);
				this.destroyedTargetMap.put(t.getName(), desTarg);

			}
		}

		// add new "observation target" where destroyed target was:

		// Add a new, visible Target in place of the destroyed target:
		g.reschuMap.addTarget(
				new Target(observationTargetName, observationTargetName,
						intendedPayload, g.reschuMap.chkTargetOffset(x, y), g),
				true);

	}

	public void garbageTargetCollect() {
		synchronized (getTargetList()) {
			for (int i = 0; i < getTargetList().size(); i++) {
				if (getTargetList().get(i).isDone()) {
					Target t = getTargetList().get(i);
					lsnr.EVT_Target_Disappeared(t.getName(), t.getPos());
					g.setTargetUsed(t.getName(), false);
					getTargetList().remove(i);
				}
			}

		}

	}

	// @TEMPORARY SOLUTION JUST FOR TUTORIAL BY CARL
	//
	// MANUAL SETTARGETAREA CODE I WAS ADDING
	// public void manual_setTargetArea(int x, int y, String Mission_Type,
	// String Vehicle_Type) {
	// Target t = new Target(g.getEmptyTargetName(), chkTargetOffset(x, y),
	// Mission_Type, Vehicle_Type, g.getTargetVisibility());
	// addTarget(t);
	// }

	// THIS IS TEMPORARY
	@SuppressWarnings("unused")
	private boolean chkOkayToAdd_TEMPORARY_FOR_TUTORIAL_BY_CARL(int x, int y) {
		for (int j = 0; j < getTargetList().size(); j++) {
			Target t = getTargetList().get(j);
			if (Game.getDistance(t.getPos()[0], t.getPos()[1], x, y) < MySize.SIZE_HAZARD_3_PXL)
				// System.err.println("Target[" + t.getName() +"] d=" +
				// Game.getDistanceToFirst(t.getPos()[0], t.getPos()[1], x, y
				// ));
				return false;
		}

		for (int i = 0; i < listHazard.size(); i++) {
			if (Game.getDistance(x, y, listHazard.get(i)[0],
					listHazard.get(i)[1]) < MySize.SIZE_HAZARD_3_PXL)
				return false;
		}
		for (int i = 0; i < g.getVehicleList().size(); i++) {
			Vehicle v = g.getVehicleList().getVehicle(i);
			if (Game.getDistance(v.getX(), v.getY(), x, y) < MySize.SIZE_HAZARD_3_PXL)
				return false;
		}
		return true;
	}

	// THIS IS TEMPORARY
	public void setTargetArea_TEMPORARY_FOR_TUTORIAL_BY_CARL(Random rnd) {/*
																		 * int
																		 * nTargetAreaLand
																		 * =
																		 * (Reschu
																		 * .
																		 * tutorial
																		 * ()) ?
																		 * MyGame
																		 * .
																		 * nTARGET_AREA_LAND_TUTORIAL
																		 * :
																		 * MyGame
																		 * .
																		 * nTARGET_AREA_LAND
																		 * ; int
																		 * nLandTargetNeed
																		 * =
																		 * nTargetAreaLand
																		 * -
																		 * getTargetTypeCount
																		 * (
																		 * "LAND"
																		 * );
																		 * int
																		 * nShoreTargetNeed
																		 * =
																		 * MyGame
																		 * .
																		 * nTARGET_AREA_SHORE
																		 * -
																		 * getTargetTypeCount
																		 * (
																		 * "SHORE"
																		 * );
																		 * int
																		 * nCommTargetNeed
																		 * =
																		 * MyGame
																		 * .
																		 * nTARGET_AREA_COMM
																		 * -
																		 * getTargetTypeCount
																		 * (
																		 * "COMM"
																		 * );
																		 * 
																		 * //
																		 * System
																		 * .out.
																		 * println
																		 * (
																		 * "Shore "
																		 * +
																		 * nShoreTargetNeed
																		 * );
																		 * 
																		 * for
																		 * (int
																		 * i =
																		 * 0; i
																		 * <
																		 * nLandTargetNeed
																		 * ;
																		 * i++)
																		 * { int
																		 * x, y;
																		 * do {
																		 * //
																		 * System
																		 * .out.
																		 * println
																		 * (
																		 * "LAND"
																		 * ); x
																		 * =
																		 * rnd.
																		 * nextInt
																		 * (
																		 * MySize
																		 * .
																		 * width
																		 * ); y
																		 * =
																		 * rnd.
																		 * nextInt
																		 * (
																		 * MySize
																		 * .
																		 * height
																		 * ); }
																		 * while
																		 * (
																		 * getCellType
																		 * (x,
																		 * y) ==
																		 * MyGame
																		 * .SEA
																		 * || !
																		 * chkOkayToAdd_TEMPORARY_FOR_TUTORIAL_BY_CARL
																		 * (x,
																		 * y));
																		 * Target
																		 * t =
																		 * new
																		 * Target
																		 * (g.
																		 * getEmptyTargetName
																		 * (), ,
																		 * "LAND"
																		 * ,
																		 * chkTargetOffset
																		 * (x,
																		 * y),
																		 * "UAV"
																		 * , g.
																		 * getTargetVisibility
																		 * ());
																		 * addTarget
																		 * (t);
																		 * } for
																		 * (int
																		 * i =
																		 * 0; i
																		 * <
																		 * nShoreTargetNeed
																		 * ;
																		 * i++)
																		 * { int
																		 * x, y;
																		 * do {
																		 * //
																		 * System
																		 * .out.
																		 * println
																		 * (
																		 * "SHORE"
																		 * ); x
																		 * =
																		 * rnd.
																		 * nextInt
																		 * (
																		 * MySize
																		 * .
																		 * width
																		 * ); y
																		 * =
																		 * rnd.
																		 * nextInt
																		 * (
																		 * MySize
																		 * .
																		 * height
																		 * ); //
																		 * if (
																		 * getCellType
																		 * (
																		 * y,x)
																		 * !=
																		 * MyGame
																		 * .
																		 * SEASHORE
																		 * ) //
																		 * System
																		 * .out.
																		 * println
																		 * (
																		 * "LAND TYPE PROB"
																		 * ); //
																		 * if (!
																		 * chkOkayToAdd
																		 * (
																		 * x,y))
																		 * System
																		 * .out.
																		 * println
																		 * (
																		 * "GROUND TAKEN"
																		 * ); }
																		 * while
																		 * (
																		 * getCellType
																		 * (x,
																		 * y) !=
																		 * MyGame
																		 * .
																		 * SEASHORE
																		 * || !
																		 * chkOkayToAdd_TEMPORARY_FOR_TUTORIAL_BY_CARL
																		 * (x,
																		 * y));
																		 * Target
																		 * t =
																		 * new
																		 * Target
																		 * (g.
																		 * getEmptyTargetName
																		 * (), ,
																		 * "SHORE"
																		 * ,
																		 * chkTargetOffset
																		 * (x,
																		 * y),
																		 * "UUV"
																		 * , g.
																		 * getTargetVisibility
																		 * ());
																		 * addTarget
																		 * (t);
																		 * } for
																		 * (int
																		 * i =
																		 * 0; i
																		 * <
																		 * nCommTargetNeed
																		 * ;
																		 * i++)
																		 * { int
																		 * x, y;
																		 * do {
																		 * x =
																		 * rnd
																		 * .nextInt
																		 * (
																		 * MySize
																		 * .
																		 * width
																		 * ); y
																		 * =
																		 * rnd.
																		 * nextInt
																		 * (
																		 * MySize
																		 * .
																		 * height
																		 * ); }
																		 * while
																		 * (!
																		 * chkOkayToAdd_TEMPORARY_FOR_TUTORIAL_BY_CARL
																		 * (x,
																		 * y));
																		 * Target
																		 * t =
																		 * new
																		 * Target
																		 * (g.
																		 * getEmptyTargetName
																		 * (), ,
																		 * "COMM"
																		 * ,
																		 * chkTargetOffset
																		 * (x,
																		 * y),
																		 * "UAV"
																		 * , g.
																		 * getTargetVisibility
																		 * ());
																		 * addTarget
																		 * (t);
																		 * }
																		 */
	}

	public synchronized Map<String, DestroyedTarget> getMapDestroyedTargets() {
		// TODO Auto-generated method stub
		return this.destroyedTargetMap;
	}

}
