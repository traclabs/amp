package reschu.game.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class FileReader {
    private BufferedReader br;
    private URL source;

    public FileReader(String url) {
        try{
            source=new URL(url);
        }
        catch(MalformedURLException e){

            e.printStackTrace();
        }
    }

    public FileReader(URL url){
        source=url;

    }

    public void openFile() {
        try {
            br = new BufferedReader(new InputStreamReader(source.openStream()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void closeFile() {
        try {
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String readLineByLine() {
        String ret = "";

        try {
            ret = br.readLine();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return ret;
    }

    static public boolean isNumber(String s) {
        char[] testSet = s.replace("-", "").replace("+", "").toCharArray();
        for (int i = 0; i < testSet.length; i++) {
            if ((int) testSet[i] < 48 || (int) testSet[i] > 57)
                return false;
        }
        return true;
    }

}
